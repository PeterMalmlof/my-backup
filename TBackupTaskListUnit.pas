unit TBackupTaskListUnit;

interface

uses
  Windows, Contnrs, ComCtrls, SysUtils, StrUtils, Forms,

  TBackupPairUnit,  // Backup Pair Object
  TGenTreeViewUnit, // Tree View
  TBackupTaskUnit;  // Backup Task Object

//------------------------------------------------------------------------------
//  My Open Folder Object
//------------------------------------------------------------------------------
type TBackupTaskList = Class(TObject)
  protected
    objTaskList : TObjectList;

    function GetTask(var Iter : integer; out pTask : TBackupTask):boolean;

    // Load all Stored Tasks from Files

    procedure LoadTasks;

    // Save all Dirty Tasks to File

    procedure SaveTasks;

    // Return TreeNode of a specified Task Object

    function FindTaskNode(
          const TaskView : TGenTreeView;
          const pTask    : TBackupTask): TGenTreeNode;

  public
    constructor Create;
    destructor  Destroy; override;

    function GetUniqueTaskName(const sName : string): string;

    //  Add a Backup Task to Task List

    procedure AddTask    (const pTask : TBackupTask);

    //  Remove a Task from Task List and Delete Task file

    procedure RemoveTask (const pTask : TBackupTask);

    // Get Task From its Name

    function FindTask(const sName : string): TBackupTask;

    // Get Pair From its Name

    function FindPair(const sName : string): TBackupPair;

    // Return True if a specified Task exists

    function IsTask(const pTask : TObject): boolean;

    // Return True if a specified Folder Pair exists

    function IsPair(const pPair : TBackupPair): boolean;

    // Return the Parent Task of a Folder Pair

    Function GetTaskFromPair(const pPair : TBackupPair): TBackupTask;

    // Refresh the Task View Control

    procedure RefreshTaskView(const TaskView : TGenTreeView);
end;

implementation

uses
  TPmaClassesUnit; // Classes

//------------------------------------------------------------------------------
//  Create Task Object
//------------------------------------------------------------------------------
constructor TBackupTaskList.Create;
begin
  inherited Create;

  // Create Task List that own its objects

  objTaskList := TObjectList.Create(true);

  // Read Stored Tasks from Files

  LoadTasks;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupTaskList.Destroy;
begin

  // Save Dirty Tasks to Files

  SaveTasks;

  // Free Tasklist

  objTaskList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Iterator
//------------------------------------------------------------------------------
function TBackupTaskList.GetTask(
            var Iter : integer; out pTask : TBackupTask):boolean;
begin
  result := false;

  if (Iter >= 0) and (Iter < objTaskList.Count) and
     (objTaskList[Iter] is TBackupTask) then
    begin
      pTask  := objTaskList[Iter] as TBackupTask;
      result := true;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//
//                                LOAD & SAVE TASKS
//
//------------------------------------------------------------------------------
//  Internal: Load all Tasks
//------------------------------------------------------------------------------
procedure TBackupTaskList.LoadTasks;
var
  fa    : Integer;
  sr    : TSearchRec;
  sPath : string;
  pTask : TBackupTask;
begin
  // Look for files in Application directory with the Task extension

  fa    := SysUtils.faAnyFile;
  sPath := SysUtils.IncludeTrailingPathDelimiter(
            ExtractFileDir(Application.ExeName));

  if (SysUtils.FindFirst(sPath + '*' + TBackupTask.GetTaskExt, fa, sr) = 0) then
    begin
      repeat

        // Got a Task File, Open it and add to Tasklist

        pTask := TBackupTask.Create(AnsiLeftStr(sr.Name,length(sr.Name) -
                                            length(TBackupTask.GetTaskExt)));
        if (pTask <> nil) then
          AddTask(pTask);

      until SysUtils.FindNext(sr) <> 0;

      SysUtils.FindClose(sr);
    end;
end;
//------------------------------------------------------------------------------
//  Internal: Save all Tasks
//------------------------------------------------------------------------------
procedure TBackupTaskList.SaveTasks;
var
  Iter  : integer;
  pTask : TBackupTask;
begin
  Iter := 0;
  while self.GetTask(Iter, pTask) do
    pTask.SaveTask;
end;
//------------------------------------------------------------------------------
//
//                                INTERNAL FUNCTIONS
//
//------------------------------------------------------------------------------
//  Internal: Return an unique Task Name
//------------------------------------------------------------------------------
function TBackupTaskList.GetUniqueTaskName(const sName : string): string;
var
  Iter  : integer;
  pTask : TBackupTask;
  sTest  : string;
  bFound : boolean;
  nLoop  : integer;
begin
  sTest := sName;
  nLoop := 0;

  repeat
    bFound := false;

    Iter := 0;
    while self.GetTask(Iter, pTask) do
      if AnsiSameText(sTest, pTask.pName) then
        begin
          bFound := true;
          break;
        end;

    if (not bFound) then
      begin
        result := sTest;
        exit;
      end;

    Inc(nLoop);
    sTest := sName + ' ' + IntToStr(nLoop);
  until nLoop > 1000;

end;
//------------------------------------------------------------------------------
//  Internal: Return TreeNode of a specified Task Object
//------------------------------------------------------------------------------
function TBackupTaskList.FindTaskNode(
          const TaskView : TGenTreeView;
          const pTask    : TBackupTask): TGenTreeNode;
begin
  result := TaskView.NodeByData(pTask);
end;
//------------------------------------------------------------------------------
//
//                                EXTERNAL METHODS
//
//------------------------------------------------------------------------------
// Get Task From its Name
//------------------------------------------------------------------------------
function TBackupTaskList.FindTask(const sName : string): TBackupTask;
var
  Iter  : integer;
  pTask : TBackupTask;
begin
  result := nil;

  Iter := 0;
  while self.GetTask(Iter, pTask) do
    if AnsiSameText(trim(pTask.pName), trim(sName)) then
      begin
        result := pTask;
        break;
      end;
end;
//------------------------------------------------------------------------------
// Get Pair From its Name
//------------------------------------------------------------------------------
function TBackupTaskList.FindPair(const sName : string): TBackupPair;
var
  Iter  : integer;
  pTask : TBackupTask;
  pPair : TBackupPair;
begin
  result := nil;

  Iter := 0;
  while self.GetTask(Iter, pTask) do
    begin
      pPair := pTask.FindPair(sName);
      if (pPair <> nil) then
        begin
          result := pPair;
          break;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Add a Backup Task to Task List
//------------------------------------------------------------------------------
procedure TBackupTaskList.AddTask(const pTask : TBackupTask);
begin
  if (pTask <> nil) and (pTask is TBackupTask) then
    begin
      objTaskList.Add(pTask);
    end;
end;
//------------------------------------------------------------------------------
//  Remove a Task from Task List and Delete Task file
//------------------------------------------------------------------------------
procedure TBackupTaskList.RemoveTask(const pTask : TBackupTask);
var
  Iter : integer;
  pCur : TBackupTask;
begin
  if (pTask <> nil) and (pTask is TBackupTask) then
    begin
      Iter := 0;
      while self.GetTask(Iter, pCur) do
        if (pCur = pTask) then
          begin
            // Remove the Task File

            pTask.RemoveTask;

            // Delete the Task also

            objTaskList.Remove(pTask);
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Return True if a specified Task exists
//------------------------------------------------------------------------------
function TBackupTaskList.IsTask(const pTask : TObject): boolean;
var
  Iter : integer;
  pCur : TBackupTask;
begin
  result := false;

  if (pTask <> nil) and (pTask is TBackupTask) then
    begin
      Iter := 0;
      while self.GetTask(Iter, pCur) do
        if (pCur = pTask) then
          begin
            result := true;
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Return True if a specified Folder Pair exists
//------------------------------------------------------------------------------
function TBackupTaskList.IsPair(const pPair : TBackupPair): boolean;
var
  Iter  : integer;
  pTask : TBackupTask;
begin
  result := false;

  Iter := 0;
  while self.GetTask(Iter, pTask) do
    if pTask.IsPair(pPair) then
      begin
        result := true;
        break;
      end;
end;
//------------------------------------------------------------------------------
// Return the Parent Task of a Folder Pair
//------------------------------------------------------------------------------
Function TBackupTaskList.GetTaskFromPair(const pPair : TBackupPair): TBackupTask;
var
  Iter  : integer;
  pTask : TBackupTask;
begin
  result := nil;

  if (pPair <> nil) and (pPair is TBackupPair) then
    begin
      Iter := 0;
      while self.GetTask(Iter, pTask) do
        if pTask.IsPair(pPair) then
          begin
            result := pTask;
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh the Task View Control
//------------------------------------------------------------------------------
procedure TBackupTaskList.RefreshTaskView(const TaskView : TGenTreeView);
var
  Iter   : integer;
  pNode  : TGenTreeNode;
  pTask  : TBackupTask;
begin
  if (TaskView <> nil) then
    begin
      //------------------------------------------------------------------------
      // Walk all Task in Task List and make sure a TreeNode exist
      //------------------------------------------------------------------------

      Iter := 0;
      while self.GetTask(Iter, pTask) do
        begin 
          // If the Task Tree Node does not exist, create it

          pNode := self.FindTaskNode(TaskView,pTask);
          if (pNode = nil) then
            begin
              pNode := TGenTreeNode.Create;
              pNode.Caption := pTask.pName;
              pNode.Data := pTask;

              TaskView.AddNode(pNode);
            end;

          // Make sure its got the right name

          if (not AnsiSameStr(pTask.pName, pNode.Caption)) then
            pNode.Caption := pTask.pName;

          if pTask.IsValid then
            pNode.ImageId := 0
          else
            pNode.ImageId := 1;

          // Refresh This Task's Folder Pair Tree Nodes also

          pTask.RefreshNodePairs(TaskView, pNode);
        end;

      //------------------------------------------------------------------------
      // Walk all TreeNodes in Task View and remove not existing tasks
      //------------------------------------------------------------------------

      // Get First Node

      Iter := 0;
      while TaskView.GetNextNode(Iter, pNode) do
        begin
          // If not in Task List, remove Node

          if (not IsTask(pNode.Data)) then
            begin
              pNode.Delete;

              // Restart iteration

              Iter := 0;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupTaskList);
end.

