Main Objects:

Task
A Task is a collection of one or many Folder Pairs. So just make a Task using New Task menu and call it something nice.

Folder Pair
A Folder Pair has a Name (your choice), one Source Folder that contain the Files and Folders you want to Backup, and one Target Folder that will be where your backup will be copied. So basically one Target Folder for Each Source Folder you want to Backup. You can place all Target Folders in a parent Backup Folder if you need to keep them together.

BackupModes:
Source To Target (default). Source Folders and Files are copied to Target if they dont exist in Target. Newer Files in Source are copied to Target. Folders and Files in Target that dont exist in Source are deleted from Target.

Source Merge Target. Same as Source To Target except than Folders and Files in Target that dont exist in Source are not deleted.

Main Functions:

Review. Make a Run of Selected Task (all its Pairs), or a Selected Folder Pair. You will get a List if differences that was found and also a summary of the Reviewed Folder Pair(s). Nothing will be Copied or Deleted.

Backup. Does a Review and then continues with a real Backup of found differences.

Preferences:

Verify	Runs a Review after each Backup so you will know everything is OK. (On by Default)
Hint	Show hints for each control.
 
Notes:	
1) Use Escape Key to Cancel a Review or a Backup. Closing the Application will also do it( Its actually safe to do so). Dont kill using Task Manager, Target files can be corrupt and possibly Locked.
2) The Source will not ever be tuched, no files will be changed or deleted in the Source.
3) It will create a TaskName.tsk file on Application Folder for each Task added. It might be nice to backup these, so you dont need to recreate them if they are lost.

Main features:
1) Simple, only does one thing. Backup!
2) Fast, because its simple. (Only the capabilities of your drives are the limit)
3) Reliable, because its simple. There might be some bugs, but it wont destroy source. (Testing can only prove existance of bugs, not the other way around).
4) Does not make an Archive with a specialized format that no others can read. It just Copies files asis to Target Folders, that are normal Folders.
5) Small Footprint / No Installation:
    - Just one exe-file that you can place where you want (your choice), preferably in a Folder of its own, and one Readme.txt file (this one).
    - Make a link to it where you want to access the Application (your choice).
    - It will create an Ini file in the folder it is placed, and store Task files there. 
    - Doesn't mess with registry at all.
    - Nothing to Save or Load. It will manage all that while you add, change or delete tasks and pairs.
    - Sizes and Positions of Windows, Lists, Column headers etc will be stored from each run.
    - Only one dialog/question for the User: "Do you really want to delete this Task / Folder Pair". It was a nice thing to have.
    - No Buttons, only one Popup Menu.
    - If you have a Target on a removable media (Usb etc.) The the Task and Pair Icons will show if its avalable or not. 
    - No undo. Some regard this as not a feature... so if you are of that persuation, watch what you are doing. If you for instance understand the concept of source and target the wrong way, you really should use some other backup tool.

Command Line:

Max 10 of each and any combination of:

/t Task Name. Will run all Folder Pairs of that Task (if drives are available)
/p Folder Pair Name. Will run the first found Folder Pair with that name (if drives are available)

If you think this is too simple:
If you need Compression of backup targets: Turn on Windows Compression for Target Folder. That will give you some compression.
If you need Encryption of backup targets: Use True Crypt (open source and a perfect tool), and set target folder to a folder managed by True Crypt. You will know if its opened or not from the Task and Pair Icons.
If you need it to mail your mother each time its finished, start writing a mail when you start the backup. The backup will be finished long before you have started the mail program. Mothers likes mail so keep doing this.
If you need sheduled backups, use Windows Sceduler and Command Line options.
If you need versions of you files, use a Source Control System, and Backup these files.
If you need a Synchrinization worth the name, use another tool. Including a real synchronization in this tool will make it much slower and complex (more bugs) since it need to store old file and folder timestamps for all possible Source and Target combinations, and match these with delete actions. Use Microsoft SynchToy (first version, that was a good tool, though it missed (messed it up) for me sometimes). Managing real good synchronization is tricky, there are a number of misstakes a User can do even if the Tool is working, So I'm staying away from it. This is a Backup Tool only.

The basic principles why you need backup:

Hardware will always break. Its not about if, its all about when. Mostly when its most inconvinient (when you havent made a backup for a while).
Hard-drives are a kind of hardware, but more so.
CD/DVD are a kind of hardware, as good/bad as Hard-drives, not better, just breaks in different ways.
Software can work, but you ever really dont know for sure. Even if it works, if will soner or later go obsolete (not supported any more, dont work with new Operativer System, the standard used for storing important data has changed. etc...)

Specifics to consider:

Windows is a really messy operating system. started with a really bad operating system (DOS) with a Window Management System built on top of it with no real layering in between. Then it evolved through a number of different versions, all ot them built using the same basic principles: Mess things up more, dont correct misstakes, just add new ones, add new layers on top of old mess, dont hide the old mess. Called backwards compatability and is normally a good thing, but not in an Operating System. This means that you need to backup a lot of files and folders, and you need to find them again and again when the OS changes (Automatic Mess Update should be on for other reasons).

Applications have somehow been influenced by Windows. They are normally also messy. So finding out where they keep their important files can be difficult, and will mostly change with new versions.

Hard-drives gets cheaper and cheaper per capacity all the time. The MB/$ increase over time is so steep that you dont need to consider a drive to go obsolete. You will instead copy your data over to new drives. The curve is currently about doubled size per $ for each year. So you will probably have a hard-drive for 3-4 years and then get rid of it. This means the standards the hard-drive is using is not an issue, only the standard of the files on the drives.

CD/DVD is not good for backup any more. The capacity per $ is currently much lower than drives. The quality of the media is not good enough. The capacity of a CD or a DVD is too small. Get bigger drives instead. Currently 1TB gives most capacity / $, this will increase to 2TB next year. Dont even consider Dual Layer DVD's or BlueRay. Nobody knows, their quality or even if they will be along in a few years, and they are too expensive compared to drives.

NAS is good for storing working files when you have a network with several Computers that need the same data. But is not specifically good for backups. Its really slow, about 20MB/s. It consists of a lot of Hardware and also Software. Using RAID you can manage Hard-drive failiure, but the NAS it-self will break someday, so you need to buy 2 at the same time. You probably cant buy a new when its broken, the new products will probably not work with your drives, so you will lose all data. The best reason for not using NAS as backup is that it will cost you about 3-5 times than just using Drives.

Wireless NAS. Consider this only if there is absolutly no way to connect with cables. Wireless is slower and the security you need to apply will make it even slower.

The best way so far is just get a simple internal Hard-drive Case for 3-4 Drives that is Hot-Swappable, then get a couple of 1TB drives to store backup. Dont have these running all the time, just when doing backups. Their life expactancy will be very good, but they will eventlually die.

Consider format standards of your files. Dont ever rely on Applications own internal formats, if you dont have to. Examples: Pictures can be stored in TIFF, Documents in RTF, Music in MP3 (high bitrates) or in WAV etc. Movies is tricky (You need to consider container standard, video encoder standard and audio encoder standard, and a lot of properties for these. Currently using AVI, DIVX, and AC3). Look for standards that are not too lossy (high bitrate), and have been around for a long time. If you can't then you need to look out for new changes in the Applications you are using and make sure you convert old files to new versions before the old version gets obsolete.

Case:

Your working Computer should have at-least 2 drives. 
One for the Windows Operating System and one for you daily working files. The System drive will also include things (Mail, AddressBook, Favorites etc.) you want to backup and you should daily backup these to the Data drive. 
One Data drive for you data (Pictures, Movies, Music, Documants, Development Code etc.). Some of this data you change daily and you should backup it to the System Disk, other data you just need handy, meaning it will not change daily. So you dont need to backup it daily.
Get an Internal Drive-Case with 3-4 drive bays (SATA). It should be hot-swappable. Dont cost much.
Buy as many drives that you need and use them to copy the data to from time to time. This will be the third copy of all you daily work and second copy of all data you need handy.
If you have data that normally is not on your System or Working Data drive. You need to store this on at least two backup drives.
You should store the second copy drive in another geographical location also.

If your Working PC is a notebook, you should probably go for USB-Backup drives, one for daily backup and one more as second copy of archived data not on your working PC. They are almost as cheap as internal Drives. Usb-3 will come along soon, so wait with eSata.
