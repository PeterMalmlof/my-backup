object PairDetailFrame: TPairDetailFrame
  Left = 0
  Top = 0
  Width = 251
  Height = 98
  TabOrder = 0
  OnResize = FrameResize
  object LabelSource: TLabel
    Left = 8
    Top = 32
    Width = 34
    Height = 13
    Caption = 'Source'
  end
  object LabelTarget: TLabel
    Left = 8
    Top = 56
    Width = 31
    Height = 13
    Caption = 'Target'
  end
  object LabelName: TLabel
    Left = 8
    Top = 8
    Width = 18
    Height = 13
    Caption = 'Pair'
  end
  object LabelTotCount: TLabel
    Left = 112
    Top = 32
    Width = 31
    Height = 13
    Caption = 'Count:'
  end
  object LabelTotSize: TLabel
    Left = 168
    Top = 32
    Width = 23
    Height = 13
    Caption = 'Size:'
  end
  object LabelDate: TLabel
    Left = 112
    Top = 56
    Width = 26
    Height = 13
    Caption = 'Date:'
  end
  object SourceFile: TEdit
    Left = 48
    Top = 32
    Width = 57
    Height = 19
    Hint = 'Source folder to backup from. Dbl Click to select folder'
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = 'SourceFile'
    OnChange = SourceFileChange
    OnDblClick = SourceFileDblClick
    OnEnter = SourceFileEnter
  end
  object TargetFile: TEdit
    Left = 48
    Top = 56
    Width = 57
    Height = 19
    Hint = 'Target Folder to backup to. Dbl Click to select folder'
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = 'TargetFile'
    OnChange = TargetFileChange
    OnDblClick = TargetFileDblClick
    OnEnter = TargetFileEnter
  end
  object PairName: TEdit
    Left = 48
    Top = 8
    Width = 57
    Height = 19
    Hint = 'Name of the Backup Pair'
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Text = 'PairName'
    OnChange = PairNameChange
    OnEnter = PairNameEnter
  end
  object MergeTarget: TCheckBox
    Left = 112
    Top = 8
    Width = 97
    Height = 17
    Caption = 'MergeTarget'
    TabOrder = 3
    OnClick = MergeTargetClick
  end
end
