object TaskFrame: TTaskFrame
  Left = 0
  Top = 0
  Width = 134
  Height = 43
  TabOrder = 0
  OnResize = FrameResize
  object LabelName: TLabel
    Left = 8
    Top = 8
    Width = 24
    Height = 13
    Caption = 'Task'
  end
  object TaskName: TEdit
    Left = 39
    Top = 3
    Width = 74
    Height = 19
    Hint = 'The name of the Backup Task'
    Ctl3D = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = 'TaskName'
    OnChange = TaskNameChange
    OnEnter = TaskNameEnter
  end
end
