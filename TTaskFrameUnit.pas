unit TTaskFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls,

  TBackupTaskUnit;

type
  TTaskFrame = class(TFrame)
    LabelName: TLabel;
    TaskName: TEdit;
    procedure FrameResize(Sender: TObject);
    procedure TaskNameChange(Sender: TObject);
    procedure TaskNameEnter(Sender: TObject);
  private
    objCurTask : TBackupTask;
  public
    // Set Selected Task

    procedure StartUp;
    procedure ShutDown;

    procedure SetCurTask(const pTask : TBackupTask);

    procedure RefreshTask;

    procedure ResizeFrame;

    procedure SetReadOnly(const Value : boolean);
  end;

implementation

{$R *.dfm}

uses
  TPmaLogUnit,        // Log
  TGenFileSystemUnit, // File System
  MainBackupUnit,     // Main Backup Form
  TPmaClassesUnit;    // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TTaskFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TTaskFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');
end;
//------------------------------------------------------------------------------
//  Set Current Pair
//------------------------------------------------------------------------------
procedure TTaskFrame.SetReadOnly(const Value : boolean);
begin
  TaskName.ReadOnly := Value;
end;
//------------------------------------------------------------------------------
//  Set Current Task
//------------------------------------------------------------------------------
procedure TTaskFrame.SetCurTask(const pTask : TBackupTask);
begin
  if (pTask <> nil) and (pTask is TBackupTask) then
    begin
      objCurTask := pTask;
      TaskName.Text := objCurTask.pName;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TTaskFrame.RefreshTask;
begin
  if (objCurTask <> nil) and TaskList.IsTask(objCurTask) then
    begin
      if (not AnsiSameStr(TaskName.Text, objCurTask.pName)) then
        begin
          TaskName.Text := objCurTask.pName;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User has Changed Task Name
//------------------------------------------------------------------------------
procedure TTaskFrame.TaskNameChange(Sender: TObject);
begin
  if (objCurTask <> nil) and TaskList.IsTask(objCurTask) then
    begin
      if (not AnsiSameText(Trim(objCurTask.pName), Trim(TaskName.Text))) then
        begin

          if (not TGenFileSystem.FileNameValid(TaskName.Text)) then
            begin
              TaskName.Text := objCurTask.pName;
              Log(strErrInvalidChar);
            end
          else
            begin
              objCurTask.pName := TaskName.Text;
              MainBackup.RefreshViews;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TTaskFrame.FrameResize(Sender: TObject);
begin
  ResizeFrame;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TTaskFrame.ResizeFrame;
begin

  LabelName.Left := brd;
  LabelName.Top  := brd + (TaskName.Height - LabelName.Height) div 2;

  TaskName.Left  := LabelName.Left + labelwdt + brd;
  TaskName.Width := self.ClientWidth - brd - TaskName.Left;
  TaskName.Top   := brd;

  self.Height := TaskName.Height + brd;
end;
//------------------------------------------------------------------------------
//  Entered Task Name
//------------------------------------------------------------------------------
procedure TTaskFrame.TaskNameEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TTaskFrame);
end.
