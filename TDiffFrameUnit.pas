unit TDiffFrameUnit;

interface

uses
  Windows,  Classes, Controls, Forms, ComCtrls, ImgList, ExtCtrls, StdCtrls,
  SysUtils, Math,

  TGenAppPropUnit,    // Application Properties
  TBackupFoleUnit,    // Base Fole Object
  TBackupFolderUnit,  // Folder Object
  TBackupFileUnit,    // File Object
  TBackupTaskUnit,    // Task Object
  TBackupPairUnit,    // Pair Object
  TPmaListViewUnit;   // List View

//------------------------------------------------------------------------------
// Difference FRame
//------------------------------------------------------------------------------
type
  TDiffFrame = class(TFrame)
    DiffImages: TImageList;
    HelpText: TMemo;
    SumList: TPmaListView;
    DiffView: TPmaListView;
    procedure FrameResize(Sender: TObject);
    procedure FrameMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FrameMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FrameMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure HelpTextChange(Sender: TObject);
    function DiffViewSort(Item1, Item2: TPmaListItem; Column: Integer;
      SortDown: Boolean): Integer;
    function SumListSort(Item1, Item2: TPmaListItem; Column: Integer;
      SortDown: Boolean): Integer;
    procedure DiffViewLog(sLine: String);
  private
    objSplitterProp  : TGenAppPropInt;

    objSplitterDown  : boolean;
    objSplitterBegin : integer;

    objReadMeDirty : boolean;

    class function ActionToIcon (
        const bSource : boolean;
        const Action  : TFileAction): integer;

    procedure SaveHelp;
  public
    procedure StartUp;
    procedure ShutDown;

    procedure ClearDiff;

    procedure AddDiffFole(const pFole : TBackupFole);

    procedure AddTaskPair(
            const pTask : TBackupTask;
            const pPair : TBackupPair);

    procedure ShowHelp;
    procedure HideHelp;

    procedure ResizeFrame;
  end;

implementation

{$R *.dfm}

uses
  TPmaLogUnit,        // Logging
  TGenStrUnit,        // Strings
  TPmaDateTimeUnit,   // Date Time
  MainBackupUnit,     // Main Form
  TPmaClassesUnit;    // Classes

const
  prfPref      = 'DiffFrame';
  prfSplittHgt = 'SplitterHgt';

  // Icon Definitions

  DiffIconNoAction   = 0;
  DiffIconToTarget   = 1;
  DiffIconToSource   = 2;

  // DiffView Column Indexes

  ColDiff   = 0;
  ColAction = 1;
  ColMod    = 2;
  ColDelta  = 3;
  ColSize   = 4;
  ColName   = 5;

  // DiffView Column Ini Names

  sPrfDiff     = 'DiffView';

  // SumList Column Indexes

  slTask     = 0;
  slPair     = 1;
  slTotSize  = 2;
  slTotCount = 3;
  slErrCount = 4;
  slCopySize = 5;

  // SumList Column Ini Names

  sPrfSum      = 'SumList';
  sPrfHeight   = 'Height';

  HelpFileName = 'readme.txt';

resourcestring

  resHelpSaved = 'Help Saved';
  resHelpFile  = 'Help File';
  resDontExist = 'Dont Exist';

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  StartUp Diff Frame
//------------------------------------------------------------------------------
procedure TDiffFrame.StartUp;
begin
  objReadMeDirty := false;

  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  DiffView.DoubleBuffered := true;
  SumList.DoubleBuffered  := true;

  objSplitterProp := App.CreatePropInt(prfPref,prfSplittHgt, SumList.Height);
  SumList.Height := objSplitterProp.pInt;

  // Load Diff View Columns and Settings

  DiffView.AddColumn('Diff');
  DiffView.AddColumn('Action');
  DiffView.AddColumn('Mod');
  DiffView.AddColumn('Delta');
  DiffView.AddColumn('Size');
  DiffView.AddColumn('Path');

  DiffView.Startup;
  DiffView.LoadSettings(sPrfDiff);

  // Load Sum List Columns and Settings

  SumList.AddColumn('Task');
  SumList.AddColumn('Pair');
  SumList.AddColumn('Size');
  SumList.AddColumn('Count');
  SumList.AddColumn('Errors');
  SumList.AddColumn('Err Size');
  SumList.ColumnAdjust := 5;

  SumList.Startup;
  SumList.LoadSettings(sPrfSum);

  ResizeFrame;
end;
//------------------------------------------------------------------------------
//  ShutDown Diff Frame
//------------------------------------------------------------------------------
procedure TDiffFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  DiffView.SaveSettings(sPrfDiff);
  DiffView.ShutDown;

  SumList.SaveSettings(sPrfSum);
  SumList.ShutDown;

  SaveHelp;
end;
//------------------------------------------------------------------------------
//  Clear all Content
//------------------------------------------------------------------------------
procedure TDiffFrame.ClearDiff;
begin
  DiffView.Clear;
  SumList.Clear;
end;
//------------------------------------------------------------------------------
//  Translate DiffType to Icon Image
//------------------------------------------------------------------------------
class function TDiffFrame.ActionToIcon(
        const bSource : boolean;
        const Action  : TFileAction): integer;
begin
  if bSource then
    begin
      // Walking Source Folders and Files

      case Action of
        faNone : result := DiffIconNoAction;    // Dont do anything
        faCopy : result := DiffIconToTarget;    // Copy to Target
        faDel  : result := DiffIconToSource;    // Delete Source
      else
        result := DiffIconNoAction;
      end;
    end
  else
    begin
      // Walking Target Folders and Files

      case Action of
        faNone : result := DiffIconNoAction;    // Dont do anything
        faCopy : result := DiffIconToSource;    // Copy to Source
        faDel  : result := DiffIconToTarget;    // Delete Target
      else
        result := DiffIconNoAction;
      end;
    end;
end;
//------------------------------------------------------------------------------
//  Add a Missing Folder
//------------------------------------------------------------------------------
procedure TDiffFrame.AddDiffFole(const pFole : TBackupFole);
var
  Item : TPmaListItem;
begin
  if (pFole <> nil) then
    begin
      Item         := DiffView.AddItem(
                      pFole.pSourceStr + ' ' + pFole.pDiffStr);
      Item.Data    := pFole;

      Item.ImageId := ActionToIcon(pFole.pSource, pFole.pAction);

      Item.AddColumn(pFole.pActionStr);

      Item.AddColumn(TPmaDateTime.FileTimeToStr(pFole.pTimeMod));

      if pFole.pMatch <> nil then
        Item.AddColumn(TPmaDateTime.FileTimeDiffToStr(
            pFole.pTimeMod, pFole.pMatch.pTimeMod))
      else
        Item.AddColumn('-');

      Item.AddColumn(SizeToStr(pFole.pTotSize));
      Item.AddColumn(pFole.pPath);
    end;
end;
//------------------------------------------------------------------------------
//  Add a Task Pair Summary
//------------------------------------------------------------------------------
procedure TDiffFrame.AddTaskPair(
            const pTask : TBackupTask;
            const pPair : TBackupPair);
var
  Item : TPmaListItem;
begin
  if (pTask <> nil) and (pPair <> nil) then
    begin

      Item := SumList.AddItem(pTask.pName);
      Item.Data := pPair;

      Item.AddColumn(pPair.pName);
      Item.AddColumn(SizeToStr(pPair.pTotSize));
      Item.AddColumn(IntToStr (pPair.pTotCount));
      Item.AddColumn(IntToStr (pPair.pErrCount));
      Item.AddColumn(SizeToStr(pPair.pErrSize));
    end;
end;
//------------------------------------------------------------------------------
//
//                                   RESIZING
//
//------------------------------------------------------------------------------
//  Resize FRame
//------------------------------------------------------------------------------
procedure TDiffFrame.FrameResize(Sender: TObject);
begin
  ResizeFrame;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TDiffFrame.ResizeFrame;
begin
  // Set SumList first, its Height is key

  // Secure SumList Height within bounds

  if (SumList.Height < 80) then
    SumList.Height := 80;

  if (SumList.Height > (self.ClientHeight - 180)) then
    SumList.Height := self.ClientHeight - 180;

  SumList.Left   := 0;
  SumList.Width  := self.ClientWidth;
  SumList.Top    := Self.ClientHeight - SumList.Height;

  // Set DiffView above Splitter

  DiffView.Left   := 0;
  DiffView.Width  := self.ClientWidth;
  DiffView.Top    := 0;
  DiffView.Height := SumList.Top - brd * 2;

  if HelpText.Visible then
    begin
      HelpText.Left   := 0;
      HelpText.Width  := self.ClientWidth;
      HelpText.Top    := 0;
      HelpText.Height := self.ClientHeight;
    end;
end;
//------------------------------------------------------------------------------
//
//                                   SPLITTER
//
//------------------------------------------------------------------------------
//  Splitter Mouse Down
//------------------------------------------------------------------------------
procedure TDiffFrame.FrameMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (X > 0) and (X < self.ClientWidth) and
     (Y > DiffView.Height) and (Y < (DiffView.Height + brd*2)) then
    begin
      objSplitterDown  := true;
      objSplitterBegin := Y;
    end;
end;
//------------------------------------------------------------------------------
//  Splitter Mouse Move
//------------------------------------------------------------------------------
procedure TDiffFrame.FrameMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  Hgt : integer;
  dy  : integer;
begin
  if objSplitterDown then
    begin
      dy := Y - objSplitterBegin;
      objSplitterBegin := Y;

      if (dy <> 0) then
        begin
          Hgt := SumList.Height - dy;

          if (Hgt > 80) and (Hgt < (self.ClientHeight - 180)) then
            begin
              SumList.Top    := SumList.Top + dy;
              SumList.Height := SumList.Height - dy;

              DiffView.Height := SumList.Top - brd * 2;
            end;
        end;
    end
  else
    begin
      // Change Cursor if inside Splitter Area
      
      if (X > 0) and (X < self.ClientWidth) and
         (Y > DiffView.Height) and (Y < (DiffView.Height + brd*2)) then
          Cursor := crvsplit
        else
          Cursor := crDefault;
    end;
end;
//------------------------------------------------------------------------------
//  Splitter Mouse Up
//------------------------------------------------------------------------------
procedure TDiffFrame.FrameMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if objSplitterDown then
    objSplitterProp.pInt := SumList.Height;

  objSplitterDown := false;
end;
//------------------------------------------------------------------------------
//  DiffView Sorting
//------------------------------------------------------------------------------
function TDiffFrame.DiffViewSort(Item1, Item2: TPmaListItem;
  Column: Integer; SortDown: Boolean): Integer;
var
  pFole1   : TBackupFole;
  pFole2   : TBackupFole;
  ft1, ft2 : TFileTime;
begin
  result := 0;
  if (Item1 <> nil) and (Item2 <> nil) then
    begin
      pFole1 := TObject(Item1.Data) as TBackupFole;
      pFole2 := TObject(Item2.Data) as TBackupFole;

      case Column of

        ColDiff : result := CompareStr(Item1.Caption, Item2.Caption);

        ColAction : result := CompareStr(pFole1.pActionStr, pFole2.pActionStr);

        ColMod : result := TPmaDateTime.CompareFileTime(pFole1.pTimeMod, pFole2.pTimeMod);

        ColDelta  :
          begin
            if Assigned(pFole1.pMatch) and Assigned(pFole2.pMatch) then
              begin
                ft1 := TPmaDateTime.FileTimeSubtract(
                          pFole1.pMatch.pTimeMod, pFole1.pTimeMod);
                ft2 := TPmaDateTime.FileTimeSubtract(
                          pFole2.pMatch.pTimeMod, pFole2.pTimeMod);

                result := TPmaDateTime.CompareFileTime(ft1, ft2);
              end
            else if Assigned(pFole1.pMatch) then
              result := -1
            else if Assigned(pFole2.pMatch) then
              result := 1
            else
              result := 0;
          end;

        ColSize : result := CompareValue(pFole1.pTotSize, pFole2.pTotSize);

        ColName : result := CompareStr(Item1.Caption, Item2.Caption);
      end;
    if not SortDown then
      result := - result;
  end;
end;
//------------------------------------------------------------------------------
//  SumList Sorting
//------------------------------------------------------------------------------
function TDiffFrame.SumListSort(Item1, Item2: TPmaListItem;
  Column: Integer; SortDown: Boolean): Integer;
var
  pPair1   : TBackupPair;
  pPair2   : TBackupPair;
begin
  result := 0;

  if (Item1 <> nil) and (Item2 <> nil) then
    begin
      pPair1 := TObject(Item1.Data) as TBackupPair;
      pPair2 := TObject(Item2.Data) as TBackupPair;

      case Column of
        slTask     : result := CompareStr(Item1.Caption, Item2.Caption);
        slPair     : result := CompareStr(pPair1.pName,pPair2.pName);
        slTotSize  : result := CompareValue(pPair1.pTotSize, pPair2.pTotSize);
        slTotCount : result := CompareValue(pPair1.pTotCount, pPair2.pTotCount);
        slErrCount : result := CompareValue(pPair1.pErrSize, pPair2.pErrSize);
        slCopySize : result := CompareValue(pPair1.pErrSize, pPair2.pErrSize);
      end
    end;

  if not SortDown then result := - result;
end;
//------------------------------------------------------------------------------
//
//                                 HELP FILE
//
//------------------------------------------------------------------------------
//  Show Help File
//------------------------------------------------------------------------------
procedure TDiffFrame.ShowHelp;
begin
  HelpText.Left   := 0;
  HelpText.Width  := self.ClientWidth;
  HelpText.Top    := 0;
  HelpText.Height := self.ClientHeight;

  HelpText.Visible := true;;
  DiffView.Visible := false;
  SumList.Visible  := false;

  HelpText.ScrollBars := ssVertical;
  HelpText.Lines.Clear;

  if SysUtils.FileExists(HelpFileName) then
    begin
      HelpText.Lines.LoadFromFile(HelpFileName);
    end
  else
    begin
      HelpText.Lines.Add(resHelpFile + ' ' + HelpFileName + ' ' + resDontExist);

    end;
  objReadMeDirty := false;
end;
//------------------------------------------------------------------------------
//  Hide Help
//------------------------------------------------------------------------------
procedure TDiffFrame.HideHelp;
begin
  if HelpText.Visible then
    begin
      // Save Help Text if it become Dirty

      SaveHelp;

      HelpText.Visible := false;;
      DiffView.Visible := true;
      SumList.Visible  := true;
    end;
end;
//------------------------------------------------------------------------------
//  Set Dirty Flag of the Help Text
//------------------------------------------------------------------------------
procedure TDiffFrame.HelpTextChange(Sender: TObject);
begin
  objReadMeDirty := true;
end;
//------------------------------------------------------------------------------
//  Save Help Text if it is Dirty
//------------------------------------------------------------------------------
procedure TDiffFrame.SaveHelp;
begin
  if objReadMeDirty then
    begin
      if (HelpText.Lines.Count > 0) then
        begin
          HelpText.Lines.SaveToFile(HelpFileName);
          Log(resHelpSaved);
        end;

      objReadMeDirty := false;
    end;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TDiffFrame.DiffViewLog(sLine: String);
begin
  Log(sLine);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TDiffFrame);
end.
