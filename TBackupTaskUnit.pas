unit TBackupTaskUnit;

interface

uses
  Windows, Contnrs, ComCtrls, Classes, SysUtils, StrUtils, Forms,

  TBackupPairUnit,
  TGenTreeViewUnit;

//------------------------------------------------------------------------------
//  My Open Folder Object
//------------------------------------------------------------------------------
type TBackupTask = Class(TObject)
  protected
    objName     : string;       // Name of Task
    objPairList : TObjectList;  // Folder Pairs for this Task
    objDirty    : boolean;      // Task has Changed and need saving

    function GetPair(var Iter : integer; out pPair : TBackupPair):boolean;

    // Return Dirty flag if Task or any Pair is Dirty

    function IsDirty : boolean;

    // Get the Task File Name from its Task Name

    function GetPath: string;

    // Set a New Task Name

    procedure SetName (const Value : string);

    // Load Task from File

    procedure LoadTask;

    // Return Folder Pair Node from specified Pair Object

    function FindPairNode(
          const PairNode : TGenTreeNode;
          const pPair    : TBackupPair): TGenTreeNode;

    function GetTotCount: integer; // Get total Count of Files/Folders
    function GetErrCount: integer; // Get Count of Error Files/Folders

    function GetTotSize: int64; // Get total Size of Files/Folders
    function GetErrSize: int64; // Get Size of Error Files/Folders

  public
    constructor Create(const sName : string);
    destructor  Destroy; override;

    // Clear Content of Task (Pair data)

    procedure Clear;

    // Remove Task File (Not destroying object)

    procedure RemoveTask;

    function FindPair(const sName : string): TBackupPair;

    // Return if this Task is Valid (Pair Folders is available)

    function IsValid: boolean;

    // Return true if Foldser Pair exist in this Task

    function IsPair(const pPair : TObject): boolean;

    // Add a new Folder Pair to this Task

    procedure AddPair    (const pPair : TBackupPair);

    // Remove a Folder Pair from this Task (Deletes object also)

    procedure RemovePair (const pPair : TBackupPair);

    // Save this Task to file

    procedure SaveTask;

    // Refresh Task Folder Pairs in Tree View

    procedure RefreshNodePairs(
                const TaskView : TGenTreeView;
                const TaskNode : TGenTreeNode);

    // Review all Pairs of this Task

    procedure ReviewBackup(const bPerform : boolean);

    // Perform a Backup of all Task in this Pair

    procedure PerformBackup;

    // Get Task Extension

    class function GetTaskExt: string;

    class function GetTaskNewName: string;

    // Properties

    property pName : string  read objName write SetName;

    property pTotCount : integer read GetTotCount;
    property pErrCount : integer read GetErrCount;
    property pTotSize  : int64   read GetTotSize;
    property pErrSize  : int64   read GetErrSize;
end;

implementation

uses
  TGenTextFileUnit, // Text Files
  TPmaEscapeUnit,
  MainBackupUnit,   // Main Backup Form
  TPmaClassesUnit;  // Classes

const
  strTaskExt     = '.tsk';     // Task File Extension
  strTaskNewName = 'New Task'; // Task Default New Name

//------------------------------------------------------------------------------
//  Create Task Object from a Name
//------------------------------------------------------------------------------
constructor TBackupTask.Create(const sName : string);
begin
  inherited Create;

  // Set New Name

  objName := sName;

  // Create Folder Pair List (it will own the objects)

  objPairList := TObjectList.Create(true);

  // Load Content from File if File exists

  LoadTask;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupTask.Destroy;
begin

  // Destroy the Folder Pair List (all pairs will be destroyed also)

  objPairList.Free;
  
  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Iterator
//------------------------------------------------------------------------------
function TBackupTask.GetPair(
            var Iter : integer; out pPair : TBackupPair):boolean;
begin
  result := false;

  if (Iter >= 0) and (Iter < objPairList.Count) and
     (objPairList[Iter] is TBackupPair) then
    begin
      pPair  := objPairList[Iter] as TBackupPair;
      result := true;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//
//                                LOAD & SAVE
//
//------------------------------------------------------------------------------
//  Internal: Get Absolute File Name of Task
//------------------------------------------------------------------------------
procedure TBackupTask.LoadTask;
var
  TF     : TGenTextFile;
  sLine  : string;
  pPair  : TBackupPair;
begin

  // Load Content from File if File exists

  if SysUtils.FileExists(self.GetPath) then
    begin
      // Open The File for read

      TF := TGenTextFile.CreateForRead(self.GetPath);

      while TF.Read(sLine) do
        begin
          // If this is start of a Pair load it

          if TBackupPair.IsNewPair(sLine) then
            begin
              // Create a New Folder Pair

              pPair := TBackupPair.CreateFromFile(self, TF);
              if (pPair <> nil) then
                self.AddPair(pPair);
            end;
        end;

      // Close File

      TF.Free;

      // Its not dirty

      objDirty := false;
    end
  else
    begin
      // It does not exists, set it dirty so it will be saved

      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Save this Task to File
//------------------------------------------------------------------------------
procedure TBackupTask.SaveTask;
var
  TF    : TGenTextFile;
  Iter  : integer;
  pPair : TBackupPair;
begin
  // Save content to File if it, or any pair, is Dirty

  if IsDirty then
    begin
      // Open the File for Writing

      TF := TGenTextFile.CreateForWrite(self.GetPath);

      // Save all Folder Pairs

      Iter := 0;
      while self.GetPair(Iter, pPair) do
        pPair.SavePair(TF);

      // Close File

      TF.Free;

      // Not Dirty any more

      objDirty := false;
    end;
end;
//------------------------------------------------------------------------------
//
//                                INTERNAL METHODS
//
//------------------------------------------------------------------------------
// Return true if Task can be backupped
//------------------------------------------------------------------------------
function TBackupTask.IsValid : boolean;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := true;

  // It must have a Name

  if length(objName) = 0 then
    begin
      result := false;
    end
  else
    begin
      // There must be a Pair, and All Pairs must be valid

      if (objPairList.Count > 0) then
        begin
          Iter := 0;
          while self.GetPair(Iter, pPair) do
            if (not pPair.IsValid) then
              begin
                result := false;
                break;
              end;
        end
      else
        begin
          result := false;
        end;
    end;
end;
//------------------------------------------------------------------------------
// Return Dirty flag if Task or any Pair is Dirty
//------------------------------------------------------------------------------
function TBackupTask.IsDirty : boolean;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  if objDirty then
    result := true
  else
    begin
      result := false;

      Iter := 0;
      while self.GetPair(Iter, pPair) do
        if pPair.pDirty then
          begin
            result := true;
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Internal: Get Total Count of all Folders and Files
//------------------------------------------------------------------------------
function TBackupTask.GetTotCount: integer;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := 0;

  Iter := 0;
  while self.GetPair(Iter, pPair) do
    result := result + pPair.pTotCount;
end;
//------------------------------------------------------------------------------
//  Internal: Get Error Count of all Folders and Files
//------------------------------------------------------------------------------
function TBackupTask.GetErrCount: integer;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := 0;
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    result := result + pPair.pErrCount;
end;
//------------------------------------------------------------------------------
//  Internal: Get Total Count of all Folders and Files
//------------------------------------------------------------------------------
function TBackupTask.GetTotSize: int64;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := 0;
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    result := result + pPair.pErrCount;
end;
//------------------------------------------------------------------------------
//  Internal: Get Error Count of all Folders and Files
//------------------------------------------------------------------------------
function TBackupTask.GetErrSize: int64;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := 0;
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    result := pPair.pErrSize;
end;
//------------------------------------------------------------------------------
//  Internal: Get the Task File Name from its Task Name
//------------------------------------------------------------------------------
function TBackupTask.GetPath: string;
begin
  result := SysUtils.IncludeTrailingPathDelimiter(
                ExtractFileDir(Application.ExeName)) + objName + strTaskExt;
end;
//------------------------------------------------------------------------------
//  Internal: Set a New Task Name
//------------------------------------------------------------------------------
procedure TBackupTask.SetName (const Value : string);
begin
  if (not AnsiSameText(objName, trim(Value))) then
    begin
      // If File exist, delete it

      if SysUtils.FileExists(self.GetPath) then
        SysUtils.DeleteFile(self.GetPath);

      // Set New File

      objName := trim(Value);

      // Save it

      objDirty := true;
      self.SaveTask;
    end;
end;
//------------------------------------------------------------------------------
//  Internal: Return Folder Pair Node from Pair Object
//------------------------------------------------------------------------------
function TBackupTask.FindPairNode(
          const PairNode : TGenTreeNode;
          const pPair    : TBackupPair): TGenTreeNode;
begin
  result := PairNode.ChildByData(pPair);
end;
//------------------------------------------------------------------------------
//  Internal: Return Folder Pair Object from its Name
//------------------------------------------------------------------------------
function TBackupTask.FindPair(const sName : string): TBackupPair;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  result := nil;

  Iter := 0;
  while self.GetPair(Iter, pPair) do
    if AnsiSameText(pPair.pName, sName) then
      begin
        result := pPair;
        break;
      end;
end;
//------------------------------------------------------------------------------
//
//                                EXTERNAL METHODS
//
//------------------------------------------------------------------------------
//  Remove a Task from File
//------------------------------------------------------------------------------
procedure TBackupTask.RemoveTask;
begin
  // Delete File if it exists

  if SysUtils.FileExists(self.GetPath) then
    begin
      // Delete Task File

      SysUtils.DeleteFile(self.GetPath);
    end;
end;
//------------------------------------------------------------------------------
//  Does a Folder Pair Exist
//------------------------------------------------------------------------------
function TBackupTask.IsPair(const pPair : TObject): boolean;
var
  Iter : integer;
  pCur : TBackupPair;
begin
  result := false;

  if (pPair <> nil) and (pPair is TBackupPair) then
    begin
      Iter := 0;
      while self.GetPair(Iter, pCur) do
        if (pCur = pPair) then
          begin
            result := true;
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Add a Backup Task
//------------------------------------------------------------------------------
procedure TBackupTask.AddPair(const pPair : TBackupPair);
begin
  if (pPair <> nil) and (pPair is TBackupPair) then
    begin
      objPairList.Add(pPair);
      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Add a Backup Task
//------------------------------------------------------------------------------
procedure TBackupTask.RemovePair(const pPair : TBackupPair);
var
  Iter : integer;
  pCur : TBackupPair;
begin
  if (pPair <> nil) and (pPair is TBackupPair) then
    begin
      Iter := 0;
      while self.GetPair(Iter, pCur) do
        if (pCur = pPair) then
          begin
            // Delete the Pair from Pair List

            objPairList.Remove(pPair);
            objDirty := true;
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh Pairs
//------------------------------------------------------------------------------
procedure TBackupTask.RefreshNodePairs(
                const TaskView : TGenTreeView;
                const TaskNode : TGenTreeNode);
var
  Iter   : integer;
  pNode  : TGenTreeNode;
  pPair  : TBackupPair;
begin
  if (TaskView <> nil) and (TaskNode <> nil) then
    begin
      //------------------------------------------------------------------------
      // Walk all Pairs in Pair List
      //------------------------------------------------------------------------

      Iter := 0;
      while self.GetPair(Iter, pPair) do
        begin
          // If the Task Tree Node does not exist, create it

          pNode := self.FindPairNode(TaskNode,pPair);
          if (pNode = nil) then
            begin
              pNode := TGenTreeNode.Create;
              pNode.Caption := pPair.pName;
              pNode.Data := pPair;

              TaskNode.AddChild(pNode);
            end;

          // Make sure its got the right name

          if (not AnsiSameStr(pPair.pName, pNode.Caption)) then
            pNode.Caption := pPair.pName;

          if pPair.IsValid then
            pNode.ImageId := 0
          else
            pNode.ImageId := 1;
        end;

      //------------------------------------------------------------------------
      // Walk all TreeNodes in Task View
      //------------------------------------------------------------------------

      if (TaskNode <> nil) then
        begin
          // Get First Node

          Iter := 0;
          while TaskNode.GetNextChild(Iter, pNode) do
            begin
              if (not IsPair(pNode.Data)) then
                begin
                  pNode.Delete;
                  Iter := 0;
                end;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Review Task
//------------------------------------------------------------------------------
procedure TBackupTask.ReviewBackup(const bPerform : boolean);
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    begin
      if PmaEscape.HasEscaped then BREAK;
      pPair.DoReview(bPerform);
    end;
end;
//------------------------------------------------------------------------------
//  Review Task
//------------------------------------------------------------------------------
procedure TBackupTask.PerformBackup;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    begin
      if PmaEscape.HasEscaped then BREAK;
      pPair.DoBackup(true);
    end;

  Iter := 0;
  while self.GetPair(Iter, pPair) do
    begin
      if PmaEscape.HasEscaped then BREAK;
      pPair.DoBackup(false);
    end;
end;
//------------------------------------------------------------------------------
//  Clear Task Content
//------------------------------------------------------------------------------
procedure TBackupTask.Clear;
var
  Iter  : integer;
  pPair : TBackupPair;
begin
  Iter := 0;
  while self.GetPair(Iter, pPair) do
    pPair.Clear;
end;
//------------------------------------------------------------------------------
//
//                               CLASS METHODS
//
//------------------------------------------------------------------------------
// Get Task Extension
//------------------------------------------------------------------------------
class function TBackupTask.GetTaskExt: string;
begin
  result := strTaskExt;
end;
//------------------------------------------------------------------------------
// Get Task New Name
//------------------------------------------------------------------------------
class function TBackupTask.GetTaskNewName: string;
begin
  result := strTaskNewName;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupTask);
end.
