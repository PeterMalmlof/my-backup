unit MainBackupUnit;

interface

uses
  Windows, Classes, Controls, Forms, ExtCtrls, ComCtrls, StdCtrls, Messages,
  SysUtils,

  TBackupTaskUnit,        // Backup Task                      Object
  TBackupPairUnit,        // Backup Folder Pair               Object
  TBackupTaskListUnit,    // Singleton TaskList               Object

  TaskViewFrameUnit,      // Task and Pair TreeView and Menu  Frame
  TTaskFrameUnit,         // Task Detailed FRame              Frame
  TPairDetailFrameUnit,   // Folder Pair Detailed FRame       Frame
  TDiffFrameUnit,         // Show Difference FRame            Frame

  TPmaLogUnit,            // Logging                          Component
  TGenAppPropUnit,        // Apprication Properties           Component
  TWmMsgFactoryUnit,      // Message Queue                    Component
  TGenProgressBarUnit,    // Progress Bar                     Component
  TGenMemoUnit,           // Memo                             Component
  TPmaEscapeUnit;         // Escape                           Component

//------------------------------------------------------------------------------
// Files and Folders;
//  TBackupFoleUnit        Base class for Files and Folders              OK
//  TBackupFileUnit        File Object                                   OK
//  TBackupFolderUnit      Folder Object                                 OK
//  TBackupFileListUnit    File List Object                              OK
//  TBackupFolderListUnit  Folder List Object                            OK
//
// Generic Functions:
//  TLogUnit            Logging                                          OK
//  TGenStrUnit         Generic String Functions                         OK
//  TGenPickFolderUnit  Pick Folder                                      OK
//
// File Uitilities:
//  TGenTextFileUnit    TextFile Object     Object
//  TPmaDateTimeUnit    Date Time           Object
//  TPmaProcessUtils    Process Utilities   Functions
//  TGenFileSystemUnit  File System         Component
//  TPmaListViewUnit    List View           Component
//  TGenTreeViewUnit    Tree View           Component

const
  MSG_ME_PROGRESSBAR = WM_USER + 88;

type
  TMainBackup = class(TForm)
    ViewPanel        : TPanel;
    DetailPanel      : TPanel;
    ViewSplitter     : TSplitter;
    TaskFrame1       : TTaskFrame;
    TaskViewFrame1: TTaskViewFrame;
    PmaLog1: TPmaLog;
    AppProperties: TAppProp;
    PairDetailFrame1: TPairDetailFrame;
    DiffFrame1: TDiffFrame;
    WmMsgFactory1: TWmMsgFactory;
    ProgressBar: TGenProgressBar;
    LogMemo: TGenMemo;
    PmaEscape1: TPmaEscape;

    procedure FormCreate        (Sender: TObject);
    procedure ViewPanelResize   (Sender: TObject);
    procedure FormDestroy       (Sender: TObject);
    procedure DetailPanelResize (Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogMemoEnter(Sender: TObject);
    procedure PmaLog1Log(sLine: String);
    procedure ViewSplitterMoved(Sender: TObject);
    procedure WmMsgFactory1Log(sLine: String);
  private

    objTimer : TTimer;                // Command Line Timer

    objViewWdt    : TGenAppPropInt;   // Left Splitter Property
    objVerifyProp : TGenAppPropBool;  // Verify Property

    objWorking : boolean;             // True if performing Backup

    FSubscriber : TWmMsgSubscriber;

    procedure WMDeviceChange(Var Msg : TMessage); message WM_DEVICECHANGE;

    procedure PerformCmdLine(Sender: TObject);

    function  GetVerify:boolean;
    procedure SetVerify(const Value : boolean);

    procedure ProcessMsg(var Msg : TMsg);
  public

    // User has Changed Selection

    procedure SetCurSelection(
        const pTask : TBackupTask;
        const pPair : TBackupPair);

    procedure BeginWork;
    procedure EndWork;
    function  IsOpen: boolean;

    // Resize Controls depending on Size and Selection

    procedure ResizeForm;

    // Refresh All Views

    procedure RefreshViews;

    procedure LogMem;
    
    property  pDiffFrame : TDiffFrame read DiffFrame1;

    procedure ShowHelp;
    procedure HideHelp;

    property  pVerify : boolean read GetVerify write SetVerify;
  end;

var
  MainBackup: TMainBackup;

//------------------------------------------------------------------------------
//  Singleton: Task List is a natural Singleton and holds all current Task
//------------------------------------------------------------------------------
var TaskList : TBackupTaskList;

const
  brd      = 4;
  PBHGT    = 16;
  labelwdt = 40;
  colorok  = $00FFFFFF; // White
  colorerr = $0097CBFF; // Light Orange
  colordis = $00EAEAEA; // Light Grey

resourcestring
  strDeviceAdded     = 'Drive Added';
  strDeviceRemoved   = 'Drive Removed';
  strAppTitle        = 'My Backup';

  strErrInvalidChar  = 'ERROR; Invalid Character';
  strErrApp          = 'Program error';
  strErrCount        = 'ERROR: Object Count not Zero';
  strErrFiles        = 'Files';
  strErrFolders      = 'Folders';

  strRunTask         = 'Run Task ';
  strRunPair         = 'Run Pair ';
  strErrTaskNotFound = 'ERROR: Task Not Found';
  strErrPairNotFound = 'ERROR: Task Not Found';

implementation

{$R *.dfm}

uses
  TGenStrUnit,        // String Functions
  TBackupFoleUnit,    // Base Fole Object
  TBackupFileUnit,    // File Object
  TBackupFolderUnit,  // Folder Object
  TPmaClassesUnit;    // Classes

//------------------------------------------------------------------------------
//  Ini settings
//------------------------------------------------------------------------------
const
  prfPref       = 'Preferences';
  sPrfPos       = 'WinPos';
  sPrfPosLeft   = 'Left';
  sPrfPosWidth  = 'Width';
  sPrfPosTop    = 'Top';
  sPrfPosHeight = 'Height';
  sPrfSplitter  = 'Splitter';

  prfViewWdt = 'ViewWidth';
  prfVerify  = 'Verify';

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Main Form
//------------------------------------------------------------------------------
procedure TMainBackup.FormCreate(Sender: TObject);
begin
  Log('Main Create');

  LogMemo.StartUp;

  objWorking := false;

  //----------------------------------------------------------------------------
  //  Application Startup
  //----------------------------------------------------------------------------

  App.StartUp;
  App.pAutoSave := true;

  objViewWdt    := App.CreatePropInt(prfPref, prfViewWdt, 200);
  objVerifyProp := App.CreatePropBool(prfPref, prfVerify, false);

  if Assigned(MsgFactory) then
    begin
      // Subscribe on The Message

      FSubscriber := MsgFactory.Subscribe(
          self.ClassName + '.' + self.Name, ProcessMsg);

      //FSubscriber.AddMessage (WM_KEYDOWN);
    end;

  PairDetailFrame1.StartUp;

  //objEscapeKeyDown  := false;
  //objEscapeAccepted := false;

  ViewPanel.Width := objViewWdt.pInt;

  // Create TaskList singleton and load it from File

  TaskList := TBackupTaskList.Create;

  // Startup Task View Frame

  TaskViewFrame1.Startup;
  DiffFrame1.StartUp;
  TaskFrame1.StartUp;

  ProgressBar.CmdId := MSG_ME_PROGRESSBAR;

  LogMem;

  Application.ProcessMessages;

  // If Command Line Option run them now

  objTimer := nil;
  if ParamCount > 0 then
    begin
      objTimer := TTimer.Create(self);
      objTimer.Interval := 500;
      objTimer.OnTimer  := PerformCmdLine;
      objTimer.Enabled  := true;
    end;

  Log('Main Created');
  Log(StringOfChar('-',80));
end;
//------------------------------------------------------------------------------
//  USer Click X button
//------------------------------------------------------------------------------
procedure TMainBackup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Tell any Process To Escape

  if Assigned(PmaEscape) then PmaEscape.Escape;
  //MainBackup.AcceptEscape(true);
end;
//------------------------------------------------------------------------------
//  Destroy Main Form
//------------------------------------------------------------------------------
procedure TMainBackup.FormDestroy(Sender: TObject);
var
  tStr : string;
begin
  Log(StringOfChar('-',80));
  Log('Main Destroy');

  if Assigned(PmaEscape) then PmaEscape.Escape;

  MsgFactory.LogSubscribers;

  Log('');
  Log('Closing Frames');

  DiffFrame1.ShutDown;
  ProgressBar.ShutDown;

  // Free TaskList and Save all Changes

  TaskList.Free;

  // Write DebugCount

  if (TBackupFolder.GetDebugCount <> 0) then
    begin
      tStr := strErrCount + IntToStr(TBackupFolder.GetDebugCount);
      Application.MessageBox(PAnsiChar(tStr),
      PAnsiChar(strErrApp + ' ' + strErrFolders));
    end;

  if (TBackupFile.GetDebugCount <> 0) then
    begin
      tStr := strErrCount + IntToStr(TBackupFile.GetDebugCount);
      Application.MessageBox(PAnsiChar(tStr),
      PAnsiChar(strErrApp + ' ' + strErrFiles));
    end;

  TaskViewFrame1.ShutDown;
  LogMemo.ShutDown;
  TaskFrame1.ShutDown;

  App.ShutDown;

  if Assigned(MsgFactory) and Assigned(FSubscriber) then
    MsgFactory.DeSubscribe(FSubscriber);

  MsgFactory.LogIt;

  ClassFactory.SaveandClose;

  Log('Main Destroyed');
end;
//------------------------------------------------------------------------------
// Begin Work
//------------------------------------------------------------------------------
procedure TMainBackup.BeginWork;
begin
  objWorking := true;

  Log('BeginWork');

  Screen.Cursor := crHourGlass;

  PmaEscape.Reset;

  MainBackup.pDiffFrame.ClearDiff;

  PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));

  MainBackup.TaskViewFrame1.SetReadOnly(true);
  MainBackup.TaskFrame1.SetReadOnly(true);
  MainBackup.PairDetailFrame1.SetReadOnly(true);
end;
//------------------------------------------------------------------------------
// Begin Work
//------------------------------------------------------------------------------
procedure TMainBackup.EndWork;
begin
  objWorking := false;

  Log('EndWork');

  PmaEscape.Close;

  PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));
  PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(0));

  MainBackup.RefreshViews;

  Screen.Cursor := crDefault;
  MainBackup.LogMem;

  MainBackup.TaskViewFrame1.SetReadOnly(false);
  MainBackup.TaskFrame1.SetReadOnly(false);
  MainBackup.PairDetailFrame1.SetReadOnly(false);
end;
//------------------------------------------------------------------------------
// Begin Work
//------------------------------------------------------------------------------
function  TMainBackup.IsOpen: boolean;
begin
  result := not objWorking;
end;
//------------------------------------------------------------------------------
// Get Silent
//------------------------------------------------------------------------------
function TMainBackup.GetVerify:boolean;
begin
  if Assigned(objVerifyProp) then
    result := objVerifyProp.pBool
  else
    result := false;
end;
//------------------------------------------------------------------------------
// Get Silent
//------------------------------------------------------------------------------
procedure TMainBackup.SetVerify(const Value : boolean);
begin
  if (not objWorking) and Assigned(objVerifyProp) then
    objVerifyProp.pBool := Value;
end;
//------------------------------------------------------------------------------
// Event: Some Device has changed
//------------------------------------------------------------------------------
Procedure TMainBackup.WMDeviceChange(Var Msg: TMessage);
Const
  DBT_DEVICEARRIVAL           = $8000; // A device has been inserted and is now available
  DBT_DEVICEQUERYREMOVE       = $800;  // Permission to remove a device is requested
  DBT_DEVICEQUERYREMOVEFAILED = $8002; // Request to remove a device has been canceled
  DBT_DEVICEREMOVEPENDING     = $8003; // Device is about to be removed
  DBT_DEVICEREMOVECOMPLETE    = $8004; // Device has been removed
  DBT_DEVICETYPESPECIFIC      = $8005; // Device Specific Event
  DBT_CONFIGCHANGED           = $008;  // Current configuration has changed
Begin

  // If Drive Arrived or was Removed we trigger a refresh of my Computer Node
  // Then we tell TreeView and FileView to refresh also

  if (Msg.WParam = DBT_DEVICEARRIVAL)then
    begin
      TaskViewFrame1.RefreshTaskView;
      Log(strDeviceAdded);
    end;

  if (Msg.WParam = DBT_DEVICEREMOVECOMPLETE) or
     (Msg.WParam = DBT_DEVICEREMOVEPENDING) then
    begin
      TaskViewFrame1.RefreshTaskView;
      Log(strDeviceRemoved);
    end;
End;
//------------------------------------------------------------------------------
//  Run Command Line
//------------------------------------------------------------------------------
procedure TMainBackup.PerformCmdLine(Sender: TObject);
var
  Ind   : integer;

  sTasks : array [1..10] of string;
  nTasks : integer;
  bTask  : boolean;

  sPairs : array [1..10] of string;
  nPairs : integer;
  bPair  : boolean;

  pTask : TBackupTask;
  pPair : TBackupPair;
begin
  objTimer.Enabled := false;
  objTimer.Free;
  objTimer := nil;
  
  if ParamCount > 0 then
    begin
      nTasks := 0;
      nPairs := 0;
      bTask := false;
      bPair := false;

      for Ind := 1 to ParamCount do
        begin
          if AnsiSameText(ParamStr(Ind), '/T') then
            begin
              Inc(nTasks);
              bTask := true;
              bPair := false;
            end
          else if AnsiSameText(ParamStr(Ind), '/P') then
            begin
              Inc(nPairs);
              bTask := false;
              bPair := true;
            end
          else
            begin
              if bTask and (nTasks > 0) and (nTasks < 11) then
                sTasks[nTasks] := sTasks[nTasks] + ParamStr(Ind) + ' '
              else if bPair and (nPairs > 0) and (nPairs < 11) then
                sPairs[nPairs] := sPairs[nPairs] + ParamStr(Ind) + ' '
            end;
        end;

      // Run all Tasks

      if (nTasks > 0) then
        for Ind := 1 to nTasks do
          begin
            pTask := TaskList.FindTask(trim(sTasks[Ind]));
            if (pTask <> nil) then
              begin
                Log(strRunTask + trim(sTasks[Ind]));
                self.TaskViewFrame1.BackupTask(pTask, false);
              end
            else
              begin
                Log(strErrTaskNotFound + trim(sTasks[Ind]));
              end;
          end;

      // Run all Pairs

      if (nPairs > 0) then
        for Ind := 1 to nPairs do
          begin
            pPair := TaskList.FindPair(trim(sPairs[Ind]));
            if (pPair <> nil) then
              begin
                Log(strRunPair + trim(sPairs[Ind]));
                self.TaskViewFrame1.BackupPair(pPair, false);
              end
            else
              begin
                Log(strErrPairNotFound + trim(sPairs[Ind]));
              end;
          end;
    end;
end;
//------------------------------------------------------------------------------
//
//                             ESCAPE MANAGEMENT
//
//------------------------------------------------------------------------------
//  Message Pump
//------------------------------------------------------------------------------
procedure TMainBackup.ProcessMsg(var Msg : TMsg);
begin
  (* case Msg.message of

    WM_KEYDOWN : ;
     begin
        if Msg.wParam = VK_ESCAPE then
          begin
            objEscapeKeyDown := true;
          end;
      end; 
  end;  *)
end;
//------------------------------------------------------------------------------
//
//                                  RESIZING
//
//------------------------------------------------------------------------------
//  Resize View Panel
//------------------------------------------------------------------------------
procedure TMainBackup.ViewPanelResize(Sender: TObject);
const
  LogHgt = 200;
begin
  TaskViewFrame1.Left   := brd;
  TaskViewFrame1.Top    := brd;

  TaskViewFrame1.Width  := ViewPanel.ClientWidth - brd;
  TaskViewFrame1.Height := ViewPanel.ClientHeight - LogHgt - brd*4;

  LogMemo.Left   := brd;
  LogMemo.Width  := ViewPanel.ClientWidth - brd;
  LogMemo.Top    := ViewPanel.ClientHeight - LogHgt - Brd;
  LogMemo.Height := LogHgt;
end;
//------------------------------------------------------------------------------
//  Resize Detail Panel
//------------------------------------------------------------------------------
procedure TMainBackup.DetailPanelResize(Sender: TObject);
begin
  ResizeForm;
end;
//------------------------------------------------------------------------------
//  Resize Detail Panel
//------------------------------------------------------------------------------
procedure TMainBackup.ResizeForm;
begin

  // Task Frame is always visible

  TaskFrame1.Left  := 0;
  TaskFrame1.Width := DetailPanel.ClientWidth;
  TaskFrame1.Top   := 0;

  TaskFrame1.ResizeFRame;

  // Pair Frame is only visible when a Pair is Selected

  if (TaskViewFRame1.GetCurPair <> nil) then
    begin
      PairDetailFrame1.Visible := true;
      PairDetailFrame1.Left  := 0;
      PairDetailFrame1.Width := DetailPanel.ClientWidth;
      PairDetailFrame1.Top   := TaskFrame1.Height;

      PairDetailFrame1.ResizeFrame;

      DiffFRame1.Top    := PairDetailFrame1.Top + PairDetailFrame1.Height + brd;
    end
  else
    begin
      PairDetailFrame1.Visible := false;
      PairDetailFrame1.Left  := 0;
      PairDetailFrame1.Width := DetailPanel.ClientWidth;
      PairDetailFrame1.Top   := TaskFrame1.Height;

      PairDetailFrame1.ResizeFrame;

      DiffFRame1.Top    := TaskFrame1.Top + TaskFrame1.Height + brd;
    end;

  DiffFRame1.Left   := 0;
  DiffFRame1.Width  := DetailPanel.ClientWidth - brd;
  DiffFRame1.Height := self.ClientHeight - DiffFRame1.Top - BRD * 2 - PBHGT;

  DiffFRame1.ResizeFrame;

  ProgressBar.Left   := 0;
  ProgressBar.Width  := DetailPanel.ClientWidth - brd;
  ProgressBar.Top    := DetailPanel.ClientHeight - PBHGT - brd;
  ProgressBar.Height := PBHGT; 
end;
//------------------------------------------------------------------------------
//  Remember Spliier Position
//------------------------------------------------------------------------------
procedure TMainBackup.ViewSplitterMoved(Sender: TObject);
begin
  self.Invalidate;

  objViewWdt.pInt :=  ViewPanel.Width;
end;
//------------------------------------------------------------------------------
//  User has Changed Selection
//------------------------------------------------------------------------------
procedure TMainBackup.SetCurSelection(
        const pTask : TBackupTask;
        const pPair : TBackupPair);
begin
  if not objWorking then
    begin
      // Update Task Detail Frame

      TaskFrame1.SetCurTask(pTask);

      // Update Pair Detail Frame

      PairDetailFrame1.SetCurPair(pPair);

      // Refresh Controls

      ResizeForm;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh all Views
//------------------------------------------------------------------------------
procedure TMainBackup.RefreshViews;
begin

  // Refresh Task View Frame

  TaskViewFrame1.RefreshTaskView;

  // Refresh Task Detail Frame
  
  TaskFrame1.RefreshTask;

  // Refresh Pair Detailed View

  PairDetailFrame1.RefreshFrame;
end;
//------------------------------------------------------------------------------
//
//                                  LOGGING
//
//------------------------------------------------------------------------------
//  Set Allocated Memory
//------------------------------------------------------------------------------
procedure TMainBackup.LogMem;
begin
  self.Caption := strAppTitle + ' ' + SizeToStr(AllocMemSize);
end;
//------------------------------------------------------------------------------
//  Add a Log Line to Memo
//------------------------------------------------------------------------------
procedure TMainBackup.PmaLog1Log(sLine: String);
begin
  LogMemo.Append(sLine);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TMainBackup.WmMsgFactory1Log(sLine: String);
begin
  Log(sLine);
end;
//------------------------------------------------------------------------------
//
//                                    HELP
//
//------------------------------------------------------------------------------
//  Show Help
//------------------------------------------------------------------------------
procedure TMainBackup.ShowHelp;
begin
  DiffFrame1.ShowHelp;
end;
//------------------------------------------------------------------------------
//  Hide Help
//------------------------------------------------------------------------------
procedure TMainBackup.HideHelp;
begin
  DiffFrame1.HideHelp;
end;
//------------------------------------------------------------------------------
//  Entered Memo
//------------------------------------------------------------------------------
procedure TMainBackup.LogMemoEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMainBackup);
end.
