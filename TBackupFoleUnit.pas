unit TBackupFoleUnit;

interface

uses
  Windows, Forms, SysUtils, StrUtils;

//------------------------------------------------------------------------------
//  Synchronization Types
//------------------------------------------------------------------------------
Type TBackupMode = (
  // Source To Target
  //  1) All Folders and Files in Source are copied to Target
  //     If they are missing or different in Target
  //  2) Files and Folders existing only in Target are deleted

  bmSourceToTarget    = 0,

  // Source Merge Target:
  //  1) All Folders and Files in Source are copied to Target
  //     If they are missing or different in Target
  //  2) Files and Folders existing only in Target are left as is

  bmSourceMergeTarget = 1,

  // Synchronize:
  //  1) All Folders and File in Source are copied to Target, if they
  //     are newer or missing in Target
  //  2) All Folders and File in Target are copied to Source, if they
  //     are newer or missing in Source

  bmSynchronize = 2);

//------------------------------------------------------------------------------
//  File Difference Types
//------------------------------------------------------------------------------
type TFileDiff = (
  fdUndef    = 0,  // File Diff undefined
  fdSame     = 1,  // Files are the same
  fdExist    = 2,  // File Exist only on this side
  fdNewer    = 3,  // This File is Newer, the other Older
  fdOlder    = 4); // This File is Older, the other Newer

//------------------------------------------------------------------------------
//  File Action Types
//------------------------------------------------------------------------------
type TFileAction = (
  faNone  = 0,  // Dont do anything
  faCopy  = 1,  // Copy to the Other Side
  faDel   = 2); // Delete the File

//------------------------------------------------------------------------------
//  Files Not to Copy
//------------------------------------------------------------------------------
const
  BackupIdFile1 = 'TheBackupFolderId.txt';
  BackupIdFile2 = 'SyncToyDirectoryId.txt';

//------------------------------------------------------------------------------
//  Generic File Object
//------------------------------------------------------------------------------
type TBackupFole = Class(TObject)
  protected
    objName   : string;     // File Leaf Name
    objParent : TObject;    // Pointer to Parent Folder (TBackupFolderUnit)
    objSource : boolean;    // Source = true, Target = false

    // Matching File on the Other side. If <> nil there is a perfect Match
    // (Still the size or times can differ). If = nil then the file
    // doesnt exist on the other side

    objMatch  : TBackupFole;

    // Diff (Type) tells if Matched file is Same, Newer or Older

    objDiff   : TFileDiff;

    // Action (Type) tells what to do with it: None, Delete or Copy
    
    objAction : TFileAction;

    objSize    : int64;     // Size in Bytes
    objTimeCre : TFileTime; // Create   File Time
    objTimeMod : TFileTime; // Modified File Time
    objTimeAcc : TFileTime; // Accessed File Time

    function GetPath: string; virtual; // Get Full Path Name of File

    // Return Src or Trg depending on Side

    function GetSourceStr : string;
    function GetDiffStr   : string;
    function GetActionStr : string;

    function  GetTotCount : integer; virtual; // Return Total Count
    function  GetTotSize  : int64;   virtual; // Return Total Size
    function  GetErrCount : integer; virtual; // Return Error Count
    function  GetErrSize  : int64;   virtual; // Return Size To Copy

    function GetMatchedName:string;
  public
    constructor Create (
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
                          virtual;

    destructor  Destroy; override;
    
    // Delete this File or Folder

    procedure Delete; virtual;
    procedure Copy;   virtual;

    procedure LogFole; virtual;

    procedure CalcDiffAction(const Mode : TBackupMode); virtual;

    procedure ShowDiff(
        const DiffFrame : TFrame); virtual;

    procedure PerformBackup(const Del : boolean); virtual;

    function IsAction: boolean;

    // Backup Mode Translations

    class Function BackupModeToIndex (const Mode : TBackupMode): integer;
    class Function BackupIndexToMode (const Ind  : integer)    : TBackupMode;
    class Function BackupModeToStr   (const Mode : TBackupMode): string;

    property pName      : string      read objName;
    property pPath      : string      read GetPath;
    property pParent    : TObject     read objParent;
    property pTimeCre   : TFileTime   read objTimeCre;
    property pTimeMod   : TFileTime   read objTimeMod;
    property pTimeAcc   : TFileTime   read objTimeAcc;
    property pSource    : boolean     read objSource;
    property pSourceStr : string      read GetSourceStr;

    property pTotSize  : int64   read GetTotSize;
    property pTotCount : integer read GetTotCount;
    property pErrSize  : int64   read GetErrSize;
    property pErrCount : integer read GetErrCount;

    property pMatch   : TBackupFole  read objMatch write objMatch;
    property pDiff    : TFileDiff    read objDiff;
    property pDiffStr : string       read GetDiffStr;

    property pAction    : TFileAction  read objAction;
    property pActionStr : string       read GetActionStr;
end;

resourcestring
  strCancelByUser = 'ERROR: Canceled by User';
  strErrNoMatch   = 'ERROR: No Match to ';

  strDiffUndef    = 'Unknown'; // Files are not Compared yet
  strDiffSame     = 'Same';    // Files are the same
  strDiffExist    = 'Exist';   // File Exists only on this Side
  strDiffNewer    = 'Newer';   // This File is Newer, the other Older
  strDiffOlder    = 'Older';   // This File is Older, the other Newer

  strActionUndef  = 'Undefined';   // Dont do anything
  strActionNone   = 'Do Nothing';  // Dont do anything
  strActionDel    = 'Delete';      // Delete the File
  strActionCopy   = 'Copy';        // Copy to the Other Side

  strSource       = 'Src';
  strTarget       = 'Trg';

  strUndef        = 'Undefined';

  strSourceToTarget    = 'Source To Target';
  strSourceMergeTarget = 'Source Merge Target';
  strSynchronize       = 'Synchronize';

implementation

uses
  TPmaLogUnit,        // Log
  TBackupFolderUnit,  // FOlder Object
  TDiffFrameUnit,     // Show Difference Frame
  MainBackupUnit,     // Main Backup Form
  TPmaClassesUnit;    // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Folder Object from a Name
//------------------------------------------------------------------------------
constructor TBackupFole.Create(
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // File Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
begin
  inherited Create;

  // Set In data or default data

  objName    := sName;
  objParent  := pParent;
  objSource  := bSource;

  objMatch   := nil;
  objDiff    := fdUndef;
  objAction  := faNone;

  objSize    := nSize;
  objTimeCre := TimeCre;
  objTimeMod := TimeMod;
  objTimeAcc := TimeAcc;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupFole.Destroy;
begin
  // Nothing to destroy

  inherited Destroy;
end;
//------------------------------------------------------------------------------
// Return Src or Trg depending on Side
//------------------------------------------------------------------------------
function TBackupFole.GetSourceStr: string;
begin
  if objSource then result := strSource else result := strTarget;
end;
//------------------------------------------------------------------------------
// Return true if Action
//------------------------------------------------------------------------------
function TBackupFole.IsAction: boolean;
begin
  result := (objAction <> faNone);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TBackupFole.LogFole;
begin
end;
//------------------------------------------------------------------------------
//  Delete Fole
//------------------------------------------------------------------------------
procedure TBackupFole.Delete;
begin
  Log('Delete Abstract');
end;
//------------------------------------------------------------------------------
//  Delete File
//------------------------------------------------------------------------------
procedure TBackupFole.Copy;
begin
  Log('Copy Abstract');
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
function TBackupFole.GetPath:string;
begin
  // Add Leaf Name

  result := objName;

  // Ask the parent for its full path and add it

  if (objParent <> nil) and (objParent is TBackupFolder) then
    result := SysUtils.IncludeTrailingPathDelimiter(
                TBackupFolder(objParent).pPath) + result;
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
function TBackupFole.GetMatchedName:string;
var
  pPar   : TBackupFolder;
  sThis  : string;
  sMatch : string;
begin
  result := '';

  if (objMatch <> nil) then
    begin
      // Use the Match Directly

      result := objMatch.pPath;
    end
  else
    begin
      // Walk parents to get the first Matched pairs
      // This cant fail, since base Source and Target is Matched

      pPar := objParent as TBackupFolder;
      while (pPar <> nil) and (pPar.pMatch = nil) do
        begin
          pPar := pPar.pParent as TBackupFolder;
        end;

      // Did we got anything

      if (pPar <> nil) and (pPar.pMatch <> nil) then
        begin
          // Get full Path of this file where the Match occurred

          sThis  := pPar.pPath;

          // Get the full path of the other side where the match occurred

          sMatch := pPar.pMatch.pPath;

          // Substitute the left part of this side to the other side
          // Leave the right part as is

          result := AnsiRightStr(self.pPath,length(self.pPath)-length(sThis));
          result := sMatch + result;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Return Total Count
//------------------------------------------------------------------------------
function TBackupFole.GetTotCount:integer;
begin
  result := 1;
end;
//------------------------------------------------------------------------------
//  Return Size of File
//------------------------------------------------------------------------------
function TBackupFole.GetTotSize:int64;
begin
  result := objSize;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TBackupFole.GetErrCount:integer;
begin
  result := 0;
  if (objAction <> faNone) then result := result + self.GetTotCount;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy (Dont care about delete)
//------------------------------------------------------------------------------
function TBackupFole.GetErrSize:int64;
begin
  result := 0;
  if (objAction = faCopy) then result := result + self.GetTotSize;
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
procedure TBackupFole.CalcDiffAction(const Mode : TBackupMode);
begin
  // Abstract: Do nothing
end;
//------------------------------------------------------------------------------
//  Show File Difference
//------------------------------------------------------------------------------
procedure TBackupFole.ShowDiff(
        const DiffFrame : TFrame);
begin
  if (DiffFrame <> nil) then
    TDiffFrame(DiffFrame).AddDiffFole(self);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TBackupFole.PerformBackup;
begin
  // Abstract: Do Nothing
end;
//------------------------------------------------------------------------------
//
//                                CLASS METHODS
//
//------------------------------------------------------------------------------
//  Translate File Difference to String
//------------------------------------------------------------------------------
function TBackupFole.GetDiffStr: string;
begin
  result := strDiffUndef;

  case objDiff of
    fdSame    : result := strDiffSame;
    fdExist   : result := strDiffExist;
    fdNewer   : result := strDiffNewer;
    fdOlder   : result := strDiffOlder;   
  end;
end;
//------------------------------------------------------------------------------
//  Translate File Actions to String
//------------------------------------------------------------------------------
function TBackupFole.GetActionStr: string;
begin
  result := strActionUndef;

  case objAction of
    faNone : result := strActionNone;
    faCopy : result := strActionCopy;
    faDel  : result := strActionDel;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to Icon
//------------------------------------------------------------------------------
class function TBackupFole.BackupModeToIndex(const Mode : TBackupMode): integer;
begin
  result := 0;

  case Mode of
    bmSourceToTarget    : result := 0;
    bmSourceMergeTarget : result := 1;
    bmSynchronize       : result := 2;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to Icon
//------------------------------------------------------------------------------
class function TBackupFole.BackupIndexToMode(const Ind : integer): TBackupMode;
begin
  result := bmSourceToTarget;

  case Ind of
    0 : result := bmSourceToTarget;
    1 : result := bmSourceMergeTarget;
    2 : result := bmSynchronize;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to String
//------------------------------------------------------------------------------
class Function TBackupFole.BackupModeToStr  (const Mode : TBackupMode): string;
begin
  result := strUndef;

  case Mode of
    bmSourceToTarget    : result := strSourceToTarget;
    bmSourceMergeTarget : result := strSourceMergeTarget;
    bmSynchronize       : result := strSynchronize;
  end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupFole);
end.
 