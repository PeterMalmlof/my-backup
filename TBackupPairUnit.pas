unit TBackupPairUnit;

interface

uses
  Windows, SysUtils, StrUtils, Forms,

  TBackupFoleUnit,     // Base Fole Object
  TGenTextFileUnit,    // Text File Object
  TBackupFolderUnit;   // Folder Object

//------------------------------------------------------------------------------
//  My Open Folder Object
//------------------------------------------------------------------------------
type TBackupPair = Class(TObject)
  protected

    objTask : TObject;

    objName     : string;     // Name of the Pair
    objSource   : string;     // Source Folder
    objTarget   : string;     // Target Folder
    objDateTime : TDateTime;  // Last Run Date

    objMatched : boolean;

    objSourceObj : TBackupFolder;  // Source Folder Name
    objTargetObj : TBackupFolder;  // Target Folder Name

    objMode : TBackupMode; // Backup Mode

    objTotCount : integer; // Number of files in Source
    objTotSize  : int64;   // Total Size in Source
    objErrCount : integer; // Number of missmatches found
    objErrSize  : int64;   // Number of Bytes to Copy

    objDirty : boolean; // Has Changed since loading

    procedure SetName   (const Value : string);
    procedure SetSource (const Value : string);
    procedure SetTarget (const Value : string);
    procedure SetMode   (const Value : TBackupMode);

  public
    constructor Create(
          const pTask : TObject;
          const sName : string);

    constructor CreateFromFile(
          const pTask : TObject;
          const TF    : TGenTextFile);

    destructor  Destroy; override;

    // Clear Preview Data

    procedure Clear;

    // Save the Folder Pair to File

    procedure SavePair(const TF : TGenTextFile);

    function IsValid : boolean;
    function IsSource: boolean;
    function IsTarget: boolean;

    // Perform a Review

    procedure DoReview(const bPerform : boolean);

    // Perform a Backup

    procedure DoBackup(const Del : boolean);

    // Splits a Line in Name/Value when Loading

    class procedure SplitLine(
          const sLine  : string;
          out   sName  : string;
          out   sValue : string);

    // Return default Name of a New Pair

    class function GetPairNewName: string;

    // IS this Line a Start of anew Pair

    class function IsNewPair(const sLine : string):boolean;

    //--------------------------------------------------------------------------
    // Properties

    property pName    : string  read objName    write SetName;
    property pSource  : string  read objSource  write SetSource;
    property pTarget  : string  read objTarget  write SetTarget;
    property pDateTime    : TDateTime  read objDateTime  write objDateTime;

    property pTotCount : integer  read objTotCount;
    property pTotSize  : int64    read objTotSize;
    property pErrCount : integer  read objErrCount;
    property pErrSize  : int64    read objErrSize;

    property pMode  : TBackupMode read objMode  write SetMode;
    property pDirty : boolean     read objDirty write objDirty;

    property pMatched  : boolean read objMatched;
end;


implementation

uses
  TPmaLogUnit,      // Log
  TBackupFileUnit,  // Files
  TPmaDateTimeUnit, // Date Time
  TBackupTaskUnit,  // Backup Task
  TPmaEscapeUnit,
  MainBackupUnit,   // Main Backup Form
  TPmaClassesUnit;  // Classes

const
  PairSaveDelimit = '=';
  PairSaveStart   = '<PairStart';
  PairSaveEnd     = '/PairEnd';
  PairSaveName    = '<Pair';
  PairSaveSource  = '<Source';
  PairSaveTarget  = '<Target';
  PairSaveMode    = '<Mode';
  PairSaveCount   = '<Count';
  PairSaveSize    = '<Size';
  PairSaveDate    = '<SDate';

  strPairNewName   = 'New Pair';

  strErrBadProp = 'ERROR Unknown Pair Property';

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;

//------------------------------------------------------------------------------
//  Create Folder Pair Object from a Name
//------------------------------------------------------------------------------
constructor TBackupPair.Create(
          const pTask : TObject;
          const sName : string);
begin
  inherited Create;

  objTask    := pTask;

  objName     := sName;
  objSource   := '';
  objTarget   := '';
  objMode     := bmSourceToTarget;
  objDateTime := 0;

  objMatched := false;

  objTotCount := 0;
  objTotSize  := 0;
  objErrCount := 0;
  objErrSize  := 0;

  objDirty   := false;
end;
//------------------------------------------------------------------------------
//  Create Folder Pair Object from a Name
//------------------------------------------------------------------------------
constructor TBackupPair.CreateFromFile(
          const pTask : TObject;
          const TF    : TGenTextFile);
var
  sLine  : string;
  sName  : string;
  sValue : string;
begin
  inherited Create;

  // The File is already opened and PairSaveStart has been read

  objTask    := pTask;

  objName    := 'X';
  objSource  := '';
  objTarget  := '';
  objMode    := bmSourceToTarget;
  objMatched := false;

  objTotCount := 0;
  objTotSize  := 0;
  objErrCount := 0;
  objErrSize  := 0;

  // Read next LIne

  while Tf.Read(sLine) do
    begin
      TBackupPair.SplitLine(sLine, sName, sValue);

      if AnsiSameText(sName, PairSaveName) then
        begin
          objName := sValue;
        end
      else if AnsiSameText(sName, PairSaveSource) then
        begin
          objSource := sValue;
        end
      else if AnsiSameText(sName, PairSaveTarget) then
        begin
          objTarget := sValue;
        end
      else if AnsiSameText(sName, PairSaveMode) then
        begin
          objMode := TBackupFole.BackupIndexToMode(StrToInt(sValue));
        end
      else if AnsiSameText(sName, PairSaveCount) then
        begin
          objTotCount := StrToInt(sValue);
        end
      else if AnsiSameText(sName, PairSaveSize) then
        begin
          objTotSize := StrToInt64(sValue);
        end
      else if AnsiSameText(sName, PairSaveDate) then
        begin
          objDateTime := StrToDateTime(sValue);
        end
      else if AnsiSameText(sName, PairSaveEnd) then
        begin
          // End of this Pair, get out

          break;
        end
      else
        begin
          // something wrong, get out

          Log(strErrBadProp + ' ' + sLine);
          break;
        end;
    end;

  objDirty  := false;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupPair.Destroy;
begin
  if (objSourceObj <> nil) then objSourceObj.Free;
  if (objTargetObj <> nil) then objTargetObj.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Internal: Split a Saved Line into Name and Value
//------------------------------------------------------------------------------
class procedure TBackupPair.SplitLine(
          const sLine  : string;
          out   sName  : string;
          out   sValue : string);
var
  I : integer;
begin
  sName  := sLine; // If no Name Value Pair return whole Line as Name
  sValue := '';

  // Walk Line from left to first equal sign (=)

  for I := 1 to length(sLine) do
    if sLine[I] = PairSaveDelimit then
      begin
        // Name is all character left of equal sign

        sName  := AnsiLeftStr(sLine, I - 1);

        // Value is all characters right of equal sign

        sValue := AnsiRightStr(sLine, length(sLine) - I);

        // We are done
        
        break;
      end;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
procedure TBackupPair.Clear;
begin
  if (objSourceObj <> nil) then objSourceObj.Free;
  objSourceObj := nil;

  if (objTargetObj <> nil) then objTargetObj.Free;
  objTargetObj := nil;

end;
//------------------------------------------------------------------------------
// Save the Folder Pair to File
//------------------------------------------------------------------------------
procedure TBackupPair.SavePair(const TF : TGenTextFile);
begin
  // Save Pair begin

  TF.Write(PairSaveStart);

  // Save Name

  TF.Write(PairSaveName + PairSaveDelimit + objName);

  // Save Source Folder Name

  TF.Write(PairSaveSource + PairSaveDelimit + objSource);

  // Save Target Folder Name

  TF.Write(PairSaveTarget + PairSaveDelimit + objTarget);

  TF.Write(PairSaveMode  + PairSaveDelimit +
            IntToStr(TBackupFole.BackupModeToIndex(objMode)));

  TF.Write(PairSaveCount + PairSaveDelimit + IntToStr(objTotCount));
  TF.Write(PairSaveSize  + PairSaveDelimit + IntToStr(objTotSize));
  TF.Write(PairSaveDate  + PairSaveDelimit + DateTimeToStr(objDateTime));

  // Save Pair Endf

  TF.Write(PairSaveEnd);

  objDirty := false;
end;
//------------------------------------------------------------------------------
//  Change Name of Current Task
//------------------------------------------------------------------------------
procedure TBackupPair.SetName (const Value : string);
begin
  if (not AnsiSameText(objName, Value)) then
    begin
      // Set New Folder Pair Name

      objName := Value;

      // Mark it Dirty

      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Change Source Folder of this Folder Pair
//------------------------------------------------------------------------------
procedure TBackupPair.SetSource (const Value : string);
begin
  if (not AnsiSameText(objSource, Value)) then
    begin
      // Set New Folder Pair Name

      objSource := Value;

      // Mark it Dirty

      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Change Target Folder of this Folder Pair
//------------------------------------------------------------------------------
procedure TBackupPair.SetTarget (const Value : string);
begin
  if (not AnsiSameText(objTarget, Value)) then
    begin
      // Set New Folder Pair Name

      objTarget := Value;

      // Mark it Dirty

      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Set Pair Mode
//------------------------------------------------------------------------------
procedure TBackupPair.SetMode(const Value : TBackupMode);
begin
  if (objMode <> Value) then
    begin
      objMode  := Value;
      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Return True if Source is Valid
//------------------------------------------------------------------------------
function TBackupPair.IsValid:boolean;
begin
  result :=  IsSource and IsTarget;
end;
//------------------------------------------------------------------------------
//  Return True if Source is Valid
//------------------------------------------------------------------------------
function TBackupPair.IsSource:boolean;
begin
  result :=  SysUtils.DirectoryExists(objSource);
end;
//------------------------------------------------------------------------------
//  Return True if Source is Valid
//------------------------------------------------------------------------------
function TBackupPair.IsTarget:boolean;
begin
  result :=  SysUtils.DirectoryExists(objTarget);
end;
//------------------------------------------------------------------------------
//  Review Pair
//------------------------------------------------------------------------------
procedure TBackupPair.DoReview(const bPerform : boolean);
var
  t : TFileTime;
begin
  if IsValid then
    begin
      if (not bPerform) then
        Log('Review Folder Pair ' + self.pName);

      t.dwLowDateTime  := 0;
      t.dwHighDateTime := 0;

      // Create Source Folder Hiearchies

      if (objSourceObj <> nil) then objSourceObj.Free;
      objSourceObj := TBackupFolder.Create(true, nil, objSource, 0,
        TPmaDateTime.FileTimeNow,
        TPmaDateTime.FileTimeZero, TPmaDateTime.FileTimeZero);

      if PmaEscape.HasEscaped then
        begin
          objSourceObj.Free;
          objSourceObj := nil;
          EXIT;
        end;

      // Log('Source: ' + IntToStr(objSourceObj.pTotCount));
      // objSourceObj.LogFole;

      // Create Target Folder Hiearchies

      if (objTargetObj <> nil) then objTargetObj.Free;
      objTargetObj := TBackupFolder.Create(false, nil, objTarget, 0, 
        TPmaDateTime.FileTimeNow,
        TPmaDateTime.FileTimeZero, TPmaDateTime.FileTimeZero);

      if PmaEscape.HasEscaped then
        begin
          objSourceObj.Free;
          objSourceObj := nil;
          objTargetObj.Free;
          objTargetObj := nil;
          EXIT;
        end;

      // Log('Target: ' + IntToStr(objTargetObj.pTotCount));
      // objTargetObj.LogFole;

      // Now we cant Escape any more

      objMatched := true;

      // Match Source to Target

      objSourceObj.MatchFolder(objMode, objTargetObj);
      objTargetObj.MatchFolder(objMode, objSourceObj);

      // Match Source and Target togehter also

      objSourceObj.pMatch := objTargetObj;
      objTargetObj.pMatch := objSourceObj;

      // Get Total Count and Size

      objTotCount := objSourceObj.pTotCount - 1;
      objTotSize  := objSourceObj.pTotSize;

      objErrCount := objSourceObj.pErrCount + objTargetObj.pErrCount;
      objErrSize  := objSourceObj.pErrSize  + objTargetObj.pErrSize;

      // Show Difference

      objSourceObj.ShowFolderDiff (MainBackup.pDiffFrame);
      objTargetObj.ShowFolderDiff (MainBackup.pDiffFrame);

      // Add Summary

      MainBackup.pDiffFrame.AddTaskPair(objTask as TBackupTask, self);

      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Review Pair
//------------------------------------------------------------------------------
procedure TBackupPair.DoBackup(const Del : boolean);
begin
  if IsValid then
    begin
      Log('Backup Pair ' + self.pName);

      // Do the Source things

      objSourceObj.PerformBackup(Del);

      // Do the Target Things

      objTargetObj.PerformBackup(Del);

      objDateTime := Now;
    end;
end;
//------------------------------------------------------------------------------
//
//                                CLASS METHODS
//
//------------------------------------------------------------------------------
//  Get Default Name of a New Pair
//------------------------------------------------------------------------------
class function TBackupPair.GetPairNewName: string;
begin
  result := strPairNewName;
end;
//------------------------------------------------------------------------------
//  Is this Line a Start of a New Pair
//------------------------------------------------------------------------------
class function TBackupPair.IsNewPair(const sLine : string):boolean;
var
  sName, sValue : string;
begin
  // Split Line into Name and Value

  TBackupPair.SplitLine(sLine, sName, sValue);
  result := AnsiSameText(sName, PairSaveStart);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupPair);
end.

