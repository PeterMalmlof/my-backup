unit TGenFolderUnit;

interface

uses
  Windows, Contnrs, ComCtrls, Forms,   

  TGenFoleUnit,     // Base Fole Object
  TGenFileUnit,     // File Object
  TGenFileListUnit; // File List Object
  
//------------------------------------------------------------------------------
//  Generic Folder Object
//------------------------------------------------------------------------------
type TGenFolder = Class(TGenFole)
  protected
    objFolderList : TObject;      // All Folders in this Folder
    objFileList   : TGenFileList; // All Files in This Folder

    //--------------------------------------------------------------------------
    // Internal Methods

    function  GetTotCount : integer; override; // Return Total Count
    function  GetTotSize  : int64;   override; // Return Total Size
    function  GetErrCount : integer; override; // Return Error Count
    function  GetErrSize  : int64;   override; // Return Size To Copy

    // Walk this Folder and update content objects

    procedure WalkFolder;
  public
    constructor Create (
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
                          override;

    destructor  Destroy; override;

    procedure Delete; override;
    procedure Copy;   override;

    // Return SubFolder Object from Leaf Name

    function  FindFolder(const sFolder : string): TGenFolder;

    // Return SubFile Object from Leaf Name

    function  FindFile (const sFile : string): TGenFile;

    // Log this Folder

    procedure LogFole; override;

    // Match Files and Folders in this already matched Folder

    procedure MatchFolder(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TGenFolder);  // The other Side Folder

    // Calculate Difference to Matched File

    procedure CalcDiffAction(const Mode : TBackupMode); override;

    procedure ShowFolderDiff(
        const DiffFrame : TFrame);

    procedure PerformBackup(const Del : boolean); override;

    // Get Debug Count
    
    class function GetDebugCount: integer;
end;

implementation

uses
  SysUtils,
  ShellAPi,

  TPmaProcessUtils,
  TPmaLogUnit,        // Log Unit
  TGenFolderListUnit, // Folder List Object
  TDiffFrameUnit,     // Show Difference Frame
  MainBackupUnit,     // Main Backup Form
  TPmaClassesUnit;    // Classes

var
  DebugCount : integer = 0;

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;

//------------------------------------------------------------------------------
//  Create Folder Object from a Name
//------------------------------------------------------------------------------
constructor TGenFolder.Create(
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
begin
  inherited Create(bSource, pParent, sName, nSize, TimeCre, TimeMod, TimeAcc);

  objFolderList := TGenFolderList.Create;
  objFileList   := TGenFileList.Create;

  Inc(DebugCount);

  MainBackup.ProgBarInc;

  WalkFolder;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenFolder.Destroy;
begin

  objFolderList.Free;
  objFileList.Free;

  Dec(DebugCount);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete Fole
//------------------------------------------------------------------------------
procedure TGenFolder.Delete;
var
  sErr      : string;
begin
  if MainBackup.HasEscaped then EXIT;

  // Fist we delete all Folders recursivly

  TGenFolderList(objFolderList).DeleteFolders;
  if MainBackup.HasEscaped then EXIT;

  //Then we delete all Files Recursivly

  objFileList.DeleteFiles;
  if MainBackup.HasEscaped then EXIT;

  // Last we can delete the empty Folder itself

  // Log('Delete Folder: ' + self.GetPath);

  if not ShDelete(self.pPath, sErr) then
    begin
      Log(sErr);
    end; 

  MainBackup.ProgBarInc;
end;
//------------------------------------------------------------------------------
//  Delete File
//------------------------------------------------------------------------------
procedure TGenFolder.Copy;
var
  topath    : string;
begin
  if MainBackup.HasEscaped then EXIT;

  // First we create an empty Folder

  // Get Target File Name to Copy to from the matched pair

  if (objMatch <> nil) then
    topath := objMatch.pPath
  else
    topath := self.GetMatchedName;

  // Log('Create Folder: ' + toPath);

  if not Windows.CreateDirectory(PAnsiChar(toPath), nil) then
    begin
      Log('ERROR: Could Not Create Folder ' + toPath + ' ' +
           GetErrorString(Windows.GetLastError()));
    end;

  // Then we Copy all SubFolders of the Folder

  if not MainBackup.HasEscaped then
    TGenFolderList(objFolderList).CopyFolders;

  // Last we Copy all The Files

  if not MainBackup.HasEscaped then
    objFileList.CopyFiles;

  MainBackup.ProgBarInc;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TGenFolder.GetTotCount:integer;
begin
  result := inherited GetTotCount +
            TGenFolderList(objFolderList).pTotCount + objFileList.pTotCount;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy
//------------------------------------------------------------------------------
function TGenFolder.GetTotSize:int64;
begin
  result := TGenFolderList(objFolderList).pTotSize + objFileList.pTotSize;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TGenFolder.GetErrCount:integer;
begin
  result := inherited GetErrCount +
            TGenFolderList(objFolderList).pErrCount + objFileList.pErrCount;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy
//------------------------------------------------------------------------------
function TGenFolder.GetErrSize:int64;
begin
  result := inherited GetErrSize +
            TGenFolderList(objFolderList).pErrSize + objFileList.pErrSize;
end;
//------------------------------------------------------------------------------
//  Walk all Folders and Files
//------------------------------------------------------------------------------
procedure TGenFolder.WalkFolder;
const
  WildCard     = '*.*';
  ThisFolder   = '.';
  ParentFolder = '..';
  CardMax      = 4294967295;
var
  fa      : Integer;
  sr      : TSearchRec;
  sPath   : string;
  pFolder : TGenFolder;
  pFile   : TGenFile;
begin
  // Remove all old Folders and Files

  TGenFolderList(objFolderList).Clear; 
  objFileList.Clear;

  // Look for any thing in this Folder

  fa    := SysUtils.faAnyFile;

  sPath := IncludeTrailingPathDelimiter(GetPath);
  if SysUtils.FindFirst(sPath + WildCard, fa, sr) = 0 then
    begin
      repeat
        // Test if User has hit Escape key

        if MainBackup.WantToEscape then
          begin
            MainBackup.AcceptEscape(true);
            Log(strCancelByUser);
            EXIT;
          end;

        if MainBackup.HasEscaped then EXIT;

        // filter out some things

        if (sr.Name <> ThisFolder) and (sr.Name <> ParentFolder) then
          begin
            // Is it a Folder

            if ((sr.Attr and faDirectory) = faDirectory) then
              begin
                // Create a New Folder object and Load that

                pFolder := TGenFolder.Create(
                      objSource,
                      self,
                      sr.Name,
                      0,
                      sr.FindData.ftCreationTime,
                      sr.FindData.ftLastWriteTime,
                      sr.FindData.ftLastAccessTime);

                TGenFolderList(objFolderList).AddFolder(pFolder);
              end
            else
              begin
                // Create a new File Object and Load That

                pFile := TGenFile.Create(
                      objSource,
                      self,
                      sr.Name,
                      sr.FindData.nFileSizeLow +
                      sr.FindData.nFileSizeHigh * CardMax,
                      sr.FindData.ftCreationTime,
                      sr.FindData.ftLastWriteTime,
                      sr.FindData.ftLastAccessTime);

                objFileList.AddFile(pFile);
              end;
          end;

      until SysUtils.FindNext(sr) <> 0;

      SysUtils.FindClose(sr);
    end;
end;
//------------------------------------------------------------------------------
//  Get a Sub Folder of this Folder
//------------------------------------------------------------------------------
function TGenFolder.FindFolder(const sFolder : string): TGenFolder;
begin
  result := TGenFolderList(objFolderList).FindFolder(sFolder);
end;
//------------------------------------------------------------------------------
//  Get a Sub Folder of this Folder
//------------------------------------------------------------------------------
function TGenFolder.FindFile(const sFile : string): TGenFile;
begin
  result := objFileList.FindFile(sFile);
end;
//------------------------------------------------------------------------------
//
//                             DIFFERENCE CALCULATION
//
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
procedure TGenFolder.CalcDiffAction(const Mode : TBackupMode);
begin
  // Is there a Match

  if (objMatch = nil) then
    begin
      case Mode of
        //----------------------------------------------------------------------
        // Synchronize Source to Target
        //  A) Copy Missing FOlders in Target from Source
        //  B) Delete Folders in Target that are missing in Source
        //----------------------------------------------------------------------
        bmSourceToTarget :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Delete Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faDel;
              end
          end;

        //----------------------------------------------------------------------
        // Merge Source to Target
        //  A) Copy Missing Folders in Target from Source
        //  B) Dont delete FOlders in Target that dont exist in Source
        //----------------------------------------------------------------------
        bmSourceMergeTarget :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Leave It
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faNone;
              end

          end;

        //----------------------------------------------------------------------
        // Synchronize Source and Target
        //  A) Copy Missing Folders in Target from Source
        //  B) Copy Missing Folders in Source from Target
        //----------------------------------------------------------------------
        bmSynchronize :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Copy to Source
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end

          end;
      end;
    end
  else
    begin
      //------------------------------------------------------------------------
      // There is a Match, dont do anything
      //------------------------------------------------------------------------

      objDiff   := fdSame;
      objAction := faNone;
    end;
end;
//------------------------------------------------------------------------------
//  Match Two Folder Pairs on each other (self is Source, other is Target)
//------------------------------------------------------------------------------
procedure TGenFolder.LogFole;
begin
  Log('Fold: ' + self.pPath);

  // Match SubFolders to Each other

  TGenFolderList(objFolderList).LogFolders;

  // Match Files to Each other

  objFileList.LogFoles;
end;
//------------------------------------------------------------------------------
//  Match Two Folder Pairs on each other (self is Source, other is Target)
//------------------------------------------------------------------------------
procedure TGenFolder.MatchFolder(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TGenFolder);  // The other Side Folder
begin
  // Match SubFolders to Each other

  TGenFolderList(objFolderList).MatchFolders(Mode, pFolder);

  // Match Files to Each other

  objFileList.MatchFiles(Mode, pFolder);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TGenFolder.PerformBackup(const Del : boolean);
begin
  if MainBackup.WantToEscape then
    begin
      MainBackup.AcceptEscape(true);
      Log(strCancelByUser);
      EXIT;
    end;

  if MainBackup.HasEscaped then EXIT;

  // Perform on this Folder if applicable

  case objAction of
    faCopy  : if (not Del) then Copy;
    faDel   : if Del       then Delete;
  end;

  // Then do the SubFolders and SubFiles

  TGenFolderList(objFolderList).PerformBackup(Del);
  TGenFileList(objFileList).PerformBackup(Del);
end;
//------------------------------------------------------------------------------
//
//                              SHOW DIFFERENCES
//
//------------------------------------------------------------------------------
//  Show Diff Frame result
//------------------------------------------------------------------------------
procedure TGenFolder.ShowFolderDiff(
        const DiffFrame : TFrame);
begin
  if (DiffFrame is TDiffFrame) then
    begin
      // Walk all Folders and Show Differences

      TGenFolderList(objFolderList).ShowDiff(DiffFrame);

      // Walk all Files and Show Differences

      objFileList.ShowDiff(DiffFrame);
    end;
end;
//------------------------------------------------------------------------------
//  Get Debug Count
//------------------------------------------------------------------------------
class function TGenFolder.GetDebugCount: integer;
begin
  result := DebugCount;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TGenFolder);
end.
