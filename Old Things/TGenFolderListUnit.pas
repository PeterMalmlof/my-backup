unit TGenFolderListUnit;

interface

uses
  Windows, Contnrs, ComCtrls, Forms,

  TGenFoleUnit,
  TGenFolderUnit; // Folder Object

//------------------------------------------------------------------------------
//  Generic Folder List Object
//------------------------------------------------------------------------------
type TGenFolderList = Class(TObject)
  protected
    objFolderList : TObjectList;

    function GetFolder(var Iter : integer; out pFolder : TGenFolder):boolean;

    function GetTotSize : int64;
    function GetErrSize : int64;

    function GetTotCount : integer;
    function GetErrCount : integer;

  public
    constructor Create;
    destructor  Destroy; override;

    procedure Clear;

    procedure DeleteFolders;
    procedure CopyFolders;

    procedure LogFolders;

    procedure AddFolder(const pFolder : TGenFolder);

    function  FindFolder(const sFolder : string): TGenFolder;

    procedure MatchFolders(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TGenFolder);  // The other Side Folder

    procedure ShowDiff(
        const DiffFrame : TFrame);

    procedure PerformBackup(const Del : boolean);

    property pTotSize : int64 read GetTotSize;
    property pErrSize : int64 read GetErrSize;

    property pTotCount : integer read GetTotCount;
    property pErrCount : integer read GetErrCount;
end;

implementation

uses
  SysUtils,
  StrUtils,

  TPmaLogUnit,
  TDiffFrameUnit, // Show Difference Frame
  MainBackupUnit, // Main Backup Form
  TPmaClassesUnit;  // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Object
//------------------------------------------------------------------------------
constructor TGenFolderList.Create;
begin
  inherited Create;

  objFolderList := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenFolderList.Destroy;
begin
  objFolderList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete all Foles in a Folder List
//------------------------------------------------------------------------------
procedure TGenFolderList.DeleteFolders;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    pFolder.Delete;
end;
//------------------------------------------------------------------------------
//  Copy All Foles in a Folder List
//------------------------------------------------------------------------------
procedure TGenFolderList.CopyFolders;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    pFolder.Copy;
end;

//------------------------------------------------------------------------------
//  Iterator
//------------------------------------------------------------------------------
function TGenFolderList.GetFolder(
          var Iter : integer; out pFolder : TGenFolder):boolean;
begin
  result := false;

  if (Iter >= 0) and (Iter < objFolderList.Count) and
     (objFolderList[Iter] is TGenFolder) then
    begin
      pFolder := objFolderList[Iter] as TGenFolder;
      result := true;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//  Clear all Files in FileList
//------------------------------------------------------------------------------
procedure TGenFolderList.Clear;
begin
  objFolderList.Clear;
end;
//------------------------------------------------------------------------------
//  Log Folder
//------------------------------------------------------------------------------
procedure TGenFolderList.LogFolders;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    pFolder.LogFole;
end;
//------------------------------------------------------------------------------
//  Get Total COnt of Files
//------------------------------------------------------------------------------
function TGenFolderList.GetTotCount:integer;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  result := 0;

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    result := result + pFolder.pTotCount;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TGenFolderList.GetTotSize:int64;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  result := 0;

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    result := result + pFolder.pTotSize;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TGenFolderList.GetErrCount:integer;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  result := 0;

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    result := result + pFolder.pErrCount;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TGenFolderList.GetErrSize:int64;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  result := 0;

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    result := result + pFolder.pErrSize;
end;
//------------------------------------------------------------------------------
//  Add a File to File List
//------------------------------------------------------------------------------
procedure TGenFolderList.AddFolder(const pFolder : TGenFolder);
begin
  if (pFolder <> nil) and (pFolder is TGenFolder) then
    objFolderList.Add(pFolder);
end;
//------------------------------------------------------------------------------
//  Find a File from its Name
//------------------------------------------------------------------------------
function TGenFolderList.FindFolder(const sFolder : string): TGenFolder;
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  result := nil;

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    if AnsiSameText(pFolder.pName, sFolder) then
      begin
        result := pFolder;
        break;
      end;
end;
//------------------------------------------------------------------------------
//  Match Folders
//------------------------------------------------------------------------------
procedure TGenFolderList.MatchFolders(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TGenFolder);  // The other Side Folder
var
  Iter   : integer;
  pThis  : TGenFolder;
  pOther : TGenFolder;
begin

  // Match Files to Each other

  if (pFolder <> nil) and (pFolder is TGenFolder) then
    begin
      // Walk all Subfiles of this Folder

      Iter := 0;
      while self.GetFolder(Iter, pThis) do
        begin
          // Get the same on the other side with same name and not matched

          if (pThis.pMatch = nil) then
            begin
              pOther := pFolder.FindFolder(pThis.pName);
              if (pOther <> nil) and (pOther.pMatch = nil) then
                begin
                  // Match them together

                  pThis.pMatch  := pOther;
                  pOther.pMatch := pThis;
                end;
            end;

          if (pThis.pMatch <> nil) then
            begin
              // Now recursivly Match these to Folders

              pThis.MatchFolder(Mode, pThis.pMatch as TGenFolder);
            end;

          // Calculate Difference and Action

          pThis.CalcDiffAction(Mode);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Calc Differences after Matching
//------------------------------------------------------------------------------
procedure TGenFolderList.ShowDiff(
        const DiffFrame : TFrame);
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  // Walk all Subfolders of this Folder

  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    begin
      // Get the Difference to the File on other side

      if pFolder.IsAction then
        pFolder.ShowDiff(DiffFrame);

      // Show SubFolders recursivly, But only if Matched Folder exist
      // If no Matched Folder exists it will be copied/deleted anyway

      if (pFolder.pMatch <> nil) then
        pFolder.ShowFolderDiff(DiffFrame);
    end;
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TGenFolderList.PerformBackup(const Del : boolean);
var
  Iter    : integer;
  pFolder : TGenFolder;
begin
  Iter := 0;
  while self.GetFolder(Iter, pFolder) do
    begin
      if MainBackup.HasEscaped then EXIT;
      pFolder.PerformBackup(Del);
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TGenFolderList);
end.


