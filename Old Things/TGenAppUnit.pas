unit TGenAppUnit;

interface

uses
  Windows, Forms;

//------------------------------------------------------------------------------
//  Application Object
//------------------------------------------------------------------------------
type TGenApp = Class(TObject)
  protected
    objSilent : boolean;
    objVerify : boolean;
    objHint   : boolean;

    objMainLeft   : integer;
    objMainWidth  : integer;
    objMainTop    : integer;
    objMainHeight : integer;

    objViewWdt  : integer;

    procedure SetHint (const Value : boolean);

  public
    constructor Create;
    destructor  Destroy; override;

    class procedure StartUp;
    class procedure ShutDown;

    property pSilent : boolean read objSilent write objSilent;
    property pVerify : boolean read objVerify write objVerify;
    property pHint   : boolean read objHint   write SetHint;

    property pMainLeft   : integer read objMainLeft   write objMainLeft;
    property pMainWidth  : integer read objMainWidth  write objMainWidth;
    property pMainTop    : integer read objMainTop    write objMainTop;
    property pMainHeight : integer read objMainHeight write objMainHeight;

    property pViewWdt  : integer read objViewWdt  write objViewWdt;
end;

var TheApp : TGenApp = nil;

implementation

uses
  TGenIniFileUnit;

const
  sPrfPos       = 'WinPos';
  sPrfPosLeft   = 'Left';
  sPrfPosWidth  = 'Width';
  sPrfPosTop    = 'Top';
  sPrfPosHeight = 'Height';

  sPrfSplitter  = 'Splitter';

  sPrfPrf    = 'Preferences';
  sPrfSilent = 'Silent';
  sPrfVerify = 'Verify';
  sPrfHint   = 'Hint';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TGenApp.Create;
var
  IniFile : TGenIniFile;
begin
  inherited;

  // Set default Values

  objSilent := false;
  objVerify := true;
  objHint   := true;

  objMainLeft   := 150;
  objMainWidth  := 550;
  objMainTop    :=  50;
  objMainHeight := 450;

  objViewWdt := 200;

  // Read Inifile

  IniFile := TGenIniFile.Create;

  objSilent := IniFile.ReadBool(sPrfPrf, sPrfSilent, objSilent);
  objVerify := IniFile.ReadBool(sPrfPrf, sPrfVerify, objVerify);
  objHint   := IniFile.ReadBool(sPrfPrf, sPrfHint,   objHint);

  objMainLeft   := IniFile.ReadInteger(sPrfPos, sPrfPosLeft,   objMainLeft);
  objMainWidth  := IniFile.ReadInteger(sPrfPos, sPrfPosWidth,  objMainWidth);
  objMainTop    := IniFile.ReadInteger(sPrfPos, sPrfPosTop,    objMainTop);
  objMainHeight := IniFile.ReadInteger(sPrfPos, sPrfPosHeight, objMainHeight);

  objViewWdt := IniFile.ReadInteger(sPrfPos, sPrfSplitter, objViewWdt);

  IniFile.Free;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TGenApp.Destroy;
var
  IniFile : TGenIniFile;
begin

  // Read Inifile

  IniFile := TGenIniFile.Create;

  IniFile.WriteBool(sPrfPrf, sPrfSilent, objSilent);
  IniFile.WriteBool(sPrfPrf, sPrfVerify, objVerify);
  IniFile.WriteBool(sPrfPrf, sPrfHint,   objHint);

  IniFile.WriteInteger(sPrfPos, sPrfPosLeft,   objMainLeft);
  IniFile.WriteInteger(sPrfPos, sPrfPosWidth,  objMainWidth);
  IniFile.WriteInteger(sPrfPos, sPrfPosTop,    objMainTop);
  IniFile.WriteInteger(sPrfPos, sPrfPosHeight, objMainHeight);

  IniFile.WriteInteger(sPrfPos, sPrfSplitter, objViewWdt);

  IniFile.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Set Hint
//------------------------------------------------------------------------------
procedure TGenApp.SetHint (const Value : boolean);
begin
  if (objHint <> Value) then
    begin
      objHint := Value;
      Application.ShowHint := objHint;
    end;
end;
//------------------------------------------------------------------------------
// Startup
//------------------------------------------------------------------------------
class procedure TGenApp.StartUp;
begin
  if (TheApp = nil) then
    begin
      TheApp := TGenApp.Create;
    end;
end;
//------------------------------------------------------------------------------
// ShutDown
//------------------------------------------------------------------------------
class procedure TGenApp.ShutDown;
begin
  if (TheApp <> nil) then
    begin
      TheApp.Free;
      TheApp := nil;
    end;
end;

//------------------------------------------------------------------------------
//                                    END
//------------------------------------------------------------------------------
end.
 