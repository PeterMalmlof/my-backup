unit TLogUnit;

interface

uses
  Windows, Graphics, Controls, SysUtils, StdCtrls, StrUtils, Contnrs, Forms;


//------------------------------------------------------------------------------
//  My TextFile Object
//------------------------------------------------------------------------------
type TLog = Class(TObject)
  protected
    objLogFileName : string;
    objLogFile     : TextFile;
    objMemo        : TMemo;

  public
    constructor Create(const Memo : TMemo);
    destructor  Destroy; override;

    // Log to Log Windows and Log File

    procedure Log(const sLine : string);

    // Log only to File

    procedure LogToFile(const sLine : string);

    class procedure StartUp(const Memo : TMemo);
    class procedure ShutDown;
end;

//------------------------------------------------------------------------------
// Singleton: Log object handling all logging to file
//------------------------------------------------------------------------------
var TheLog : TLog = nil;

implementation

uses
  DateUtils;

const
  LOGEXT = '.log';

resourcestring
  resLogStart = 'Application Started';
  resLogEnd   = 'Application Stopped';

//------------------------------------------------------------------------------
// Create TMyTextFile object without opening it
//------------------------------------------------------------------------------
constructor TLog.Create(const Memo : TMemo);
begin
  inherited Create;

  objMemo := Memo;

  if (objMemo <> nil) and (objMemo is TMemo) then
    objMemo.Lines.Clear;

  //---- Set the appropriate name of the Normal Logfile ----------------

  objLogFileName := SysUtils.ChangeFileExt(Forms.Application.ExeName, LOGEXT);

  // if it exists, then delete it

  if SysUtils.FileExists(objLogFileName) then
    SysUtils.DeleteFile(objLogFileName);

  // now create it again as empty

  AssignFile (objLogFile, objLogFileName);
  Rewrite    (objLogFile);
  Flush      (objLogFile);
  CloseFile  (objLogFile);

  // This is the first log written

  Log(resLogStart);

end;
//------------------------------------------------------------------------------
//  Destroy
//------------------------------------------------------------------------------
destructor TLog.Destroy;
begin
  LogToFile(resLogEnd);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TLog.Log (const sLine : string);
begin
  LogToFile(sLine);

  if (objMemo <> nil) and (objMemo is TMemo) then
    objMemo.Lines.Add(sLine);
end;
//------------------------------------------------------------------------------
//  Log but write only to the File. Useful when Gui is closing down
//------------------------------------------------------------------------------
procedure TLog.LogToFile (const sLine : string);
begin
  if length(objLogFileName) > 0 then
    begin
      try
        AssignFile (objLogFile, objLogFileName);
        Append     (objLogFile);

        Writeln    (objLogFile,  TimeToStr(SysUtils.Time) + ' ' + sLine);
        Flush      (objLogFile);
      finally
        CloseFile  (objLogFile);
      end;
    end;
end;
//------------------------------------------------------------------------------
// Startup Logging
//------------------------------------------------------------------------------
class procedure TLog.StartUp(const Memo : TMemo);
begin
  if (TheLog = nil) then
    begin
      // Create the Log Object

      TheLog := TLog.Create(Memo);
    end;
end;
//------------------------------------------------------------------------------
// ShutDown Logging
//------------------------------------------------------------------------------
class procedure TLog.ShutDown;
begin
  if (TheLog <> nil) then
    begin

      // Free The Log Object

      TheLog.free;
      TheLog := nil;
    end;
end;
//-----------------------------------------------------------------------------
//                                      END
//-----------------------------------------------------------------------------
end.
