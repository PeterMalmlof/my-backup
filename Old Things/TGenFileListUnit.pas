unit TGenFileListUnit;

interface

uses
  Windows, Contnrs, Forms,

  TGenFoleUnit, // Base Fole Object
  TGenFileUnit; // File Object

//------------------------------------------------------------------------------
//  File List Object
//------------------------------------------------------------------------------
type TGenFileList = Class(TObject)
  protected
    objFileList : TObjectList;     // List Of Files

    function GetFile(var Iter : integer; out pFile : TGenFile):boolean;

    function GetTotSize : int64;
    function GetErrSize : int64;

    function GetTotCount : integer;
    function GetErrCount : integer;

  public
    constructor Create;
    destructor  Destroy; override;

    procedure Clear;

    procedure DeleteFiles;
    procedure CopyFiles;

    procedure LogFoles;

    procedure AddFile(const pFile : TGenFile);

    function  FindFile(const sFile : string): TGenFile;

    procedure MatchFiles(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TObject);     // The other Side Folder

    procedure ShowDiff(
        const DiffFrame : TFrame);

    procedure PerformBackup(const Del : boolean);

    property pTotSize : int64 read GetTotSize;
    property pErrSize : int64 read GetErrSize;

    property pTotCount : integer read GetTotCount;
    property pErrCount : integer read GetErrCount;
end;

implementation

uses
  SysUtils,
  StrUtils,

  TPmaLogUnit,
  TGenFolderUnit,  // Folder Object
  TDiffFrameUnit,  // Show Difference Frame
  MainBackupUnit,  // Main Backup Form
  TPmaClassesUnit; // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Object
//------------------------------------------------------------------------------
constructor TGenFileList.Create;
begin
  inherited Create;

  objFileList := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenFileList.Destroy;
begin
  objFileList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete all Foles in a Folder List
//------------------------------------------------------------------------------
procedure TGenFileList.DeleteFiles;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    begin
      if MainBackup.WantToEscape then
        begin
          MainBackup.AcceptEscape(true);
          Log(strCancelByUser);
          BREAK;
        end;

      pFile.Delete;
    end;
end;
//------------------------------------------------------------------------------
//  Copy All Foles in a Folder List
//------------------------------------------------------------------------------
procedure TGenFileList.CopyFiles;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    begin
      if MainBackup.WantToEscape then
        begin
          MainBackup.AcceptEscape(true);
          Log(strCancelByUser);
          BREAK;
        end;

      pFile.Copy;
    end;
end;
//------------------------------------------------------------------------------
//  Iterator
//------------------------------------------------------------------------------
function TGenFileList.GetFile(var Iter : integer; out pFile : TGenFile):boolean;
begin
  result := false;

  if (Iter >= 0) and (Iter < objFileList.Count) and
     (objFileList[Iter] is TGenFile) then
    begin
      pFile  := objFileList[Iter] as TGenFile;
      result := true;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//  Clear all Files in FileList
//------------------------------------------------------------------------------
procedure TGenFileList.Clear;
begin
  objFileList.Clear;
end;
//------------------------------------------------------------------------------
//  Log All Files
//------------------------------------------------------------------------------
procedure TGenFileList.LogFoles;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    pFile.LogFole;
end;
//------------------------------------------------------------------------------
//  Get File Count
//------------------------------------------------------------------------------
function TGenFileList.GetTotCount:integer;
begin
  result := objFileList.Count;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TGenFileList.GetTotSize:int64;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    result := result + pFile.pTotSize;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TGenFileList.GetErrSize:int64;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then result := result + pFile.pErrSize;
end;
//------------------------------------------------------------------------------
//  Get Error File Count
//------------------------------------------------------------------------------
function TGenFileList.GetErrCount:integer;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then result := result + 1;
end;
//------------------------------------------------------------------------------
//  Add a File to File List
//------------------------------------------------------------------------------
procedure TGenFileList.AddFile(const pFile : TGenFile);
begin
  if (pFile <> nil) and (pFile is TGenFile) then
    objFileList.Add(pFile);
end;
//------------------------------------------------------------------------------
//  Find a File from its Name
//------------------------------------------------------------------------------
function TGenFileList.FindFile(const sFile : string): TGenFile;
var
  Iter  : integer;
  pFile : TGenFile;
begin
  result := nil;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if AnsiSameText(pFile.pName, sFile) then
      begin
        result := pFile;
        break;
      end;
end;
//------------------------------------------------------------------------------
//  Match Files
//------------------------------------------------------------------------------
procedure TGenFileList.MatchFiles(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TObject);     // The other Side Folder
var
  Iter   : integer;
  pFile  : TGenFile;
  pOther : TGenFile;
begin

  // Match Files to Each other

  if (pFolder <> nil) and (pFolder is TGenFolder) then
    begin
      // Walk all Subfiles of this Folder

      Iter := 0;
      while self.GetFile(Iter, pFile) do
        begin
          if (pFile.pMatch = nil) then
            begin
              pOther := TGenFolder(pFolder).FindFile(pFile.pName);
              if (pOther <> nil) and (pOther.pMatch = nil) then
                begin
                  // Match them together

                  pFile.pMatch  := pOther;
                  pOther.pMatch := pFile;
                end;
            end;

            // Calculate Difference and Action

            pFile.CalcDiffAction(Mode);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Calc Differences after Matching
//------------------------------------------------------------------------------
procedure TGenFileList.ShowDiff(
        const DiffFrame : TFrame);
var
  Iter  : integer;
  pFile : TGenFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then pFile.ShowDiff(DiffFrame);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TGenFileList.PerformBackup(const Del : boolean);
var
  Iter  : integer;
  pFile : TGenFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    pFile.PerformBackup(Del);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TGenFileList);
end.
