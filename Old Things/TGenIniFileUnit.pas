unit TGenIniFileUnit;

interface

uses
  IniFiles;   // Ini File

//------------------------------------------------------------------------------
//  My Open Folder Object
//------------------------------------------------------------------------------
type TGenIniFile = Class(TIniFile)
  protected
    objFileName : string;

  public
    constructor Create;
    destructor  Destroy; override;

    property pFileName : string read objFileName write objFileName;
end;

implementation

uses
  SysUtils,
  Forms;

//------------------------------------------------------------------------------
//  Create Object
//------------------------------------------------------------------------------
constructor TGenIniFile.Create;
begin
  // Get Ini File Name from Application directory

  objFileName := ChangeFileExt(Application.ExeName, '.ini' );
  inherited Create(objFileName);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenIniFile.Destroy;
begin

  inherited Destroy;
end;

end.

