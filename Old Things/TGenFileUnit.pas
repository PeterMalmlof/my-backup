unit TGenFileUnit;

interface

uses
  Windows, Forms, 

  TGenFoleUnit;

//------------------------------------------------------------------------------
//  Generic File Object
//------------------------------------------------------------------------------
type TGenFile = Class(TGenFole)
  protected

  public
    constructor Create (
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // File Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
                          override;

    destructor  Destroy; override;

    procedure Delete; override;
    procedure Copy;   override;

    procedure LogFole; override;

    // Calculate Difference to Matched File

    procedure CalcDiffAction(const Mode : TBackupMode); override;

    procedure PerformBackup(const Del : boolean); override;

    class function GetDebugCount: integer;

end;

implementation

uses
  SysUtils,

  TPmaLogUnit,      // Log
  TPmaDateTimeUnit, // File Time
  TDiffFrameUnit,   // Show Difference Frame
  MainBackupUnit,   // Main Backup Form
  TPmaClassesUnit;  // Classes

var
  DebugCount : integer = 0;

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;

//------------------------------------------------------------------------------
//  Create Folder Object from a Name
//------------------------------------------------------------------------------
constructor TGenFile.Create(
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
begin
  inherited Create(bSource, pParent, sName, nSize, TimeCre, TimeMod, TimeAcc);

  MainBackup.ProgBarInc;
  
  Inc(DebugCount);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenFile.Destroy;
begin
  Dec(DebugCount);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete Fole
//------------------------------------------------------------------------------
procedure TGenFile.Delete;
var
  sErr      : string;
begin
  if MainBackup.HasEscaped then EXIT;

  // Delete the File

  // Log('Delete File: ' + self.GetPath);

  if not ShDelete(self.GetPath, sErr) then
    begin
      Log(sErr);
    end;

  MainBackup.ProgBarInc;
end;
//------------------------------------------------------------------------------
//  Delete File
//------------------------------------------------------------------------------
procedure TGenFile.Copy;
var
  topath    : string;
  sErr      : string;
begin
  if MainBackup.HasEscaped then EXIT;
  
  // Get Target File Name to Copy to from the matched pair

  if Assigned(objMatch) then
    topath := objMatch.pPath
  else
    topath := self.GetMatchedName;

  // Log('Copy File: ' + self.GetPath + ' To: ' + topath);

  if not ShCopy(self.GetPath, topath, sErr) then
    begin
      Log(sErr);
    end;

  MainBackup.ProgBarInc;

end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TGenFile.LogFole;
begin
  Log('File: ' + self.pPath);
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
procedure TGenFile.CalcDiffAction(const Mode : TBackupMode);
begin

  // Discard som files (used for backups and should no be copied)
  
  if (AnsiSameText(objName, BackupIdFile1)) or
     (AnsiSameText(objName, BackupIdFile2)) then
    begin
      objDiff   := fdSame;
      objAction := faNone;
    end

  // Is there a Match

  else if (objMatch = nil) then
    begin
      // No Match exists (Copy or Delete)

      case Mode of

        bmSourceToTarget :
          begin
            if objSource then
              begin
                // This is a Source file with no Target File, Copy to Target

                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                // This is a Target file with no Source File, Delete Target

                objDiff   := fdExist;
                objAction := faDel;
              end
          end;

        bmSourceMergeTarget :
          begin
            if objSource then
              begin
                // This is a Source file with no Target File, Copy to Target

                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                // This is a Target file with no Source File, Do Nothing

                objDiff   := fdExist;
                objAction := faNone;
              end
          end;

        bmSynchronize :
          begin
            if objSource then
              begin
                // This is a Source file with no Target File, Copy to Target

                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                // This is a Target file with no Source File, Copy to Source

                objDiff   := fdExist;
                objAction := faCopy;
              end
          end;
      end;
    end
  else
    begin
      // Match exist, Compare Modified Time

      case TPmaDateTime.CompareFileTime(objTimeMod, objMatch.pTimeMod) of

        ftOlder :
          begin
            // Older Files are Never copied anywhere, wherever they are

            objDiff   := fdOlder;
            objAction := faNone;
          end;

        ftNewer :
          begin
            case Mode of
              bmSourceToTarget :
                if objSource then
                  begin
                    // Source is Newer than Target, Copy To Target

                    objDiff   := fdNewer;
                    objAction := faCopy;
                  end
                else
                  begin
                    // Target is Newer than Source, Dont Do anything

                    objDiff   := fdNewer;
                    objAction := faNone;
                  end;

              bmSourceMergeTarget :
                if objSource then
                  begin
                    // Source is Newer than Target, Copy To Target

                    objDiff   := fdNewer;
                    objAction := faCopy;
                  end
                else
                  begin
                    // Target is Newer than Source, Dont Do anything

                    objDiff   := fdNewer;
                    objAction := faNone;
                  end;

              bmSynchronize :
                if objSource then
                  begin
                    // Source is Newer than Target, Copy To Target

                    objDiff   := fdNewer;
                    objAction := faCopy;
                  end
                else
                  begin
                    // Target is Newer than Source, Copy To Source

                    objDiff   := fdNewer;
                    objAction := faCopy;
                  end;
            end;
          end;

        ftSame :
          begin
            // Source and Target are same, dont do anything

            objDiff   := fdSame;
            objAction := faNone;
          end;

      end;
    end;
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TGenFile.PerformBackup;
begin
  if MainBackup.WantToEscape then
    begin
      MainBackup.AcceptEscape(true);
      Log(strCancelByUser);
      EXIT;
    end;

  if MainBackup.HasEscaped then EXIT;
  
  case objAction of
    faCopy : if (not Del) then self.Copy;
    faDel  : if Del       then self.Delete;
  end;
end;
//------------------------------------------------------------------------------
//
//                                CLASS METHODS
//
//------------------------------------------------------------------------------
//  Get Debug Count
//------------------------------------------------------------------------------
class function TGenFile.GetDebugCount: integer;
begin
  result := DebugCount;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TGenFile);
end.

