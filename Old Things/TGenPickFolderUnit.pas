unit TGenPickFolderUnit;

interface

uses
  Windows;

//------------------------------------------------------------------------------
//  My Open Folder Object
//------------------------------------------------------------------------------
type TGenPickFolder = Class(TObject)
  protected
    objFolder : string; // Input/Output Folder
    objTitle  : string; // Title
  public
    constructor Create;
    destructor  Destroy; override;

    function Open: boolean;

    property pFolder : string read objFolder  write objFolder;
    property pTitle  : string read objTitle   write objTitle;
end;

//------------------------------------------------------------------------------
//  Global Function Used
//------------------------------------------------------------------------------

function GenPickFolder(const Folder : string;
                       const Title  : string): string;

//------------------------------------------------------------------------------
//                                   IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

Uses
  SysUtils,
  ShlObj,
  Forms,

  TGenIniFileUnit,   // Ini Files
  TGenTextFileUnit;  // Files

var
  bInit      : boolean;
  InitFolder : string;

const
  Prf           = 'PickFolder';
  PrfWinLeft    = 'WinLeft';
  PrfWinWidth   = 'WinWidth';
  PrfWinTop     = 'WinTop';
  PrfWinHeight  = 'WinHeight';

//------------------------------------------------------------------------------
//  Create Object
//------------------------------------------------------------------------------
constructor TGenPickFolder.Create;
begin
  inherited Create;

  // Default is the Application Folder

  objFolder := SysUtils.ExtractFileDir(Application.ExeName);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenPickFolder.Destroy;
begin

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Internal Callback function
//------------------------------------------------------------------------------
function BrowseDialogCallBack
  (Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM):
  integer stdcall;
var
  wa, rect : TRect;
  dialogPT : TPoint;
  fIni     : TGenIniFile;
  sTmp     : string;
  l,t      : integer;
  R        : TRect;
begin
  // The Dialog is Iniialized, place it where it was last

  if uMsg = BFFM_INITIALIZED then
    begin
      bInit := true;

      if IsFile(ApplicationIniFile) then
        begin
          fIni := TGenIniFile.Create;

          GetWindowRect(Wnd, Rect);
          l := fIni.ReadInteger(Prf, PrfWinLeft,   100);
          //w := fIni.ReadInteger(Prf, PrfWinWidth,  120);
          t := fIni.ReadInteger(Prf, PrfWinTop,    100);
          //h := fIni.ReadInteger(Prf, PrfWinHeight, 100);
          
          fIni.Free;

          MoveWindow(Wnd, l, t, Rect.Right - Rect.Left,
                        Rect.Bottom - Rect.Top, true);
        end
      else
        begin
          // Place in Middle

          wa := Screen.WorkAreaRect;
          GetWindowRect(Wnd, Rect);
          dialogPT.X := ((wa.Right-wa.Left) div 2) -
                    ((rect.Right-rect.Left) div 2);
          dialogPT.Y := ((wa.Bottom-wa.Top) div 2) -
                    ((rect.Bottom-rect.Top) div 2);
          MoveWindow(Wnd,
               dialogPT.X,
               dialogPT.Y,
               Rect.Right - Rect.Left,
               Rect.Bottom - Rect.Top,
               True);
        end;

      // Now Select the INitial FOlder

      if (InitFolder <> '') then
        begin

            SendMessage(Wnd, BFFM_SETSELECTION, integer(LongBool(true)),
                        integer(PChar(InitFolder)));

        end;
    end

  // The User changed selection

  else if bInit and (uMsg = BFFM_SELCHANGED) then
    begin
      // Use this as excuse to save current window position and size

      sTmp := ChangeFileExt(Application.ExeName, '.ini' );
      fIni := TGenIniFile.Create;

      GetWindowRect(Wnd, R);

      fIni.WriteInteger(Prf, PrfWinLeft,   R.Left);
      fIni.WriteInteger(Prf, PrfWinWidth,  R.Right - R.Left);
      fIni.WriteInteger(Prf, PrfWinTop,    R.Top);
      fIni.WriteInteger(Prf, PrfWinHeight, R.Bottom - R.Top);

      fIni.Free;
    end;

  Result := 0;
end;

//------------------------------------------------------------------------------
// Let User Select a Filder in Windows
//------------------------------------------------------------------------------
function MyPickFolderInt(const aRoot    : integer;
                         const aCaption : string;
                         const aTitle   : string;
                         const aFolder  : string;
                         out   oFolder  : string):boolean;
var
  pPrograms,pBrowse : PItemIDList;
  hBrowseInfo       : TBROWSEINFO;
  hPChar            : PChar;
begin
  result := false;

  InitFolder := aFolder;

  if ((SHGetSpecialFolderLocation(
                    Getactivewindow,    // hwndOwner
                    aRoot,              // nFolder folder of interest
                    pPrograms)) <> NOERROR) then
    EXIT;

  // typedef struct _browseinfo {
  //   HWND hwndOwner;             A handle to the owner window for the dialog box
  //   PCIDLIST_ABSOLUTE pidlRoot; Root to start with, only subfolders will appear
  //   LPTSTR pszDisplayName;      Selected Folder return buffer
  //   LPCTSTR lpszTitle;          Title
  //   UINT ulFlags;               Flags specifying the options for the dialog box
  //   BFFCALLBACK lpfn;
  //   LPARAM lParam;
  //   int iImage;
  // } BROWSEINFO, *PBROWSEINFO, *LPBROWSEINFO;

  hPChar := StrAlloc(max_path);
  with hBrowseInfo do
    begin
      hwndOwner       := GetActiveWindow;
      pidlRoot        := pPrograms;
      pszDisplayName  := hPChar;
      lpszTitle       := pChar(aCaption);
      ulFlags         := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
      lpfn            := BrowseDialogCallBack;
      lParam          := 0;
    end;

  bInit := false;
  pBrowse := SHBrowseForFolder(hBrowseInfo);
  if (pBrowse <> nil) then 
    if (SHGetPathFromIDList(pBrowse, hPChar)) then
      begin
        oFolder := hPChar;
        result := true;
      end;

  StrDispose(hPChar);
end;
//------------------------------------------------------------------------------
//  Open the Component Browser
//------------------------------------------------------------------------------
function TGenPickFolder.Open: boolean;
var
  sTmp : string;
  nTmp : string;
begin
  result := false;

  sTmp := 'Pick ' + objTitle + ' Current: "' + objFolder + '"';

  // Initiate

  if MyPickFolderInt(0, sTmp, objTitle, objFolder, nTmp) then
    begin
      result    := true;
      objFolder := nTmp
    end;
end;
//------------------------------------------------------------------------------
//  External Function
//------------------------------------------------------------------------------
function GenPickFolder(const Folder : string;
                       const Title  : string): string;
var
  Dlg : TGenPickFolder;
begin
  result := Folder;

  Dlg := TGenPickFolder.Create;

  if (length(Folder) > 0) then
    Dlg.pFolder := Folder;

  Dlg.pTitle := Title;

  if Dlg.Open then
    begin
      result := Dlg.pFolder;
    end;

  Dlg.Free;
end;
//------------------------------------------------------------------------------
//                                     INIT
//------------------------------------------------------------------------------

end.


