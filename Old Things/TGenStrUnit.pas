unit TGenStrUnit;

interface

uses
  Windows;

  // Translate an int64 Size to a user understandable string

  function  SizeToStr (
      const dwSize   : int64;
      const sMeasure : string = 'b') : string;

implementation

uses
  SysUtils;

//------------------------------------------------------------------------------
// Convert a Disk Size, Used or Free number in MB to a meaningful string
//------------------------------------------------------------------------------
function SizeToStr (
      const dwSize : int64;
      const sMeasure : string = 'b') : string;
var
  fValue : double;
  nLevel : integer;
  sTmp   : string;
begin

  // Truncate to right size

  fValue := dwSize;
  nLevel := 0;
  while fValue >= 1000.0 do
    begin
      nLevel := nLevel +1;
      fValue := fValue / 1024;
    end;

  // Set the right format

  if fValue < 10.0 then
    sTmp := format('%1.2f',[fValue])
  else if fValue < 100.0 then
    sTmp := format('%2.1f',[fValue])
  else
    sTmp := IntToStr(Round(fValue));

  // Add the Letter Behind and base measure

  case nLevel of
    0 : SizeToStr := sTmp + sMeasure;
    1 : SizeToStr := sTmp + ' k' + sMeasure;
    2 : SizeToStr := sTmp + ' M' + sMeasure;
    3 : SizeToStr := sTmp + ' G' + sMeasure;
    4 : SizeToStr := sTmp + ' T' + sMeasure;
  else
        SizeToStr := sTmp + '?';
  end;
end;
end.
 