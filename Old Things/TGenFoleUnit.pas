unit TGenFoleUnit;

interface

uses
  Windows, Forms;

//------------------------------------------------------------------------------
//  Synchronization Types
//------------------------------------------------------------------------------
Type TBackupMode = (
  // Source To Target
  //  1) All Folders and Files in Source are copied to Target
  //     If they are missing or different in Target
  //  2) Files and Folders existing only in Target are deleted

  bmSourceToTarget    = 0,

  // Source Merge Target:
  //  1) All Folders and Files in Source are copied to Target
  //     If they are missing or different in Target
  //  2) Files and Folders existing only in Target are left as is

  bmSourceMergeTarget = 1,

  // Synchronize:
  //  1) All Folders and File in Source are copied to Target, if they
  //     are newer or missing in Target
  //  2) All Folders and File in Target are copied to Source, if they
  //     are newer or missing in Source

  bmSynchronize = 2);

//------------------------------------------------------------------------------
//  File Difference Types
//------------------------------------------------------------------------------
type TFileDiff = (
  fdUndef    = 0,  // File Diff undefined
  fdSame     = 1,  // Files are the same
  fdExist    = 2,  // File Exist only on this side
  fdNewer    = 3,  // This File is Newer, the other Older
  fdOlder    = 4); // This File is Older, the other Newer

//------------------------------------------------------------------------------
//  File Action Types
//------------------------------------------------------------------------------
type TFileAction = (
  faNone  = 0,  // Dont do anything
  faCopy  = 1,  // Copy to the Other Side
  faDel   = 2); // Delete the File

//------------------------------------------------------------------------------
//  Files Not to Copy
//------------------------------------------------------------------------------
const
  BackupIdFile1 = 'TheBackupFolderId.txt';
  BackupIdFile2 = 'SyncToyDirectoryId.txt';

//------------------------------------------------------------------------------
//  Generic File Object
//------------------------------------------------------------------------------
type TGenFole = Class(TObject)
  protected
    objName   : string;     // File Leaf Name
    objParent : TObject;    // Pointer to Parent Folder (TGenFolderUnit)
    objSource : boolean;    // Source = true, Target = false

    // Matching File on the Other side. If <> nil there is a perfect Match
    // (Still the size or times can differ). If = nil then the file
    // doesnt exist on the other side

    objMatch  : TGenFole;

    // Diff (Type) tells if Matched file is Same, Newer or Older

    objDiff   : TFileDiff;

    // Action (Type) tells what to do with it: None, Delete or Copy
    
    objAction : TFileAction;

    objSize    : int64;     // Size in Bytes
    objTimeCre : TFileTime; // Create   File Time
    objTimeMod : TFileTime; // Modified File Time
    objTimeAcc : TFileTime; // Accessed File Time

    function GetPath: string; virtual; // Get Full Path Name of File

    // Return Src or Trg depending on Side

    function GetSourceStr : string;
    function GetDiffStr   : string;
    function GetActionStr : string;

    function  GetTotCount : integer; virtual; // Return Total Count
    function  GetTotSize  : int64;   virtual; // Return Total Size
    function  GetErrCount : integer; virtual; // Return Error Count
    function  GetErrSize  : int64;   virtual; // Return Size To Copy

    function GetMatchedName:string;

    function ShDelete(
        const filename  : string;  // File to delete
        out   sErr      : string)  // Error String
                        : boolean; // True if deleted

    function ShCopy(
        const frompath  : string;  // File to copy
        const topath    : string;  // Destination
        out   sErr      : string)  // Error String
                        : boolean; // True if deleted
  public
    constructor Create (
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
                          virtual;

    destructor  Destroy; override;
    
    // Delete this File or Folder

    procedure Delete; virtual;
    procedure Copy;   virtual;

    procedure LogFole; virtual;

    procedure CalcDiffAction(const Mode : TBackupMode); virtual;

    procedure ShowDiff(
        const DiffFrame : TFrame); virtual;

    procedure PerformBackup(const Del : boolean); virtual;

    function IsAction: boolean;

    // Backup Mode Translations

    class Function BackupModeToIndex (const Mode : TBackupMode): integer;
    class Function BackupIndexToMode (const Ind  : integer)    : TBackupMode;
    class Function BackupModeToStr   (const Mode : TBackupMode): string;

    property pName      : string      read objName;
    property pPath      : string      read GetPath;
    property pParent    : TObject     read objParent;
    property pTimeCre   : TFileTime   read objTimeCre;
    property pTimeMod   : TFileTime   read objTimeMod;
    property pTimeAcc   : TFileTime   read objTimeAcc;
    property pSource    : boolean     read objSource;
    property pSourceStr : string      read GetSourceStr;

    property pTotSize  : int64   read GetTotSize;
    property pTotCount : integer read GetTotCount;
    property pErrSize  : int64   read GetErrSize;
    property pErrCount : integer read GetErrCount;

    property pMatch   : TGenFole     read objMatch write objMatch;
    property pDiff    : TFileDiff    read objDiff;
    property pDiffStr : string       read GetDiffStr;

    property pAction    : TFileAction  read objAction;
    property pActionStr : string       read GetActionStr;
end;

resourcestring
  strCancelByUser = 'ERROR: Canceled by User';
  strErrNoMatch   = 'ERROR: No Match to ';

  strDiffUndef   = 'Unknown'; // Files are not Compared yet
  strDiffSame    = 'Same';    // Files are the same
  strDiffExist   = 'Exist';   // File Exists only on this Side
  strDiffNewer   = 'Newer';   // This File is Newer, the other Older
  strDiffOlder   = 'Older';   // This File is Older, the other Newer

  strActionUndef = 'Undefined';   // Dont do anything
  strActionNone  = 'Do Nothing';  // Dont do anything
  strActionDel   = 'Delete';      // Delete the File
  strActionCopy  = 'Copy';        // Copy to the Other Side

  strSource = 'Src';
  strTarget = 'Trg';

  strUndef  = 'Undefined';

  strSourceToTarget    = 'Source To Target';
  strSourceMergeTarget = 'Source Merge Target';
  strSynchronize       = 'Synchronize';

  strErrNoPath       = 'ERROR: No Path';
  strErrDeleteFile   = 'ERROR: Delete File ';
  strErrCopyFile     = 'ERROR: Copy File ';
  strErrCode         = ' Code ';
  strErrNoErr        = 'ERROR: Unspecified error occurred ';
  strErrTo           = ' To ';

  strErr71 = 'Source and Destination same file';
  strErr72 = 'Multiple file paths error';
  strErr73 = 'Rename operation failed';
  strErr74 = 'Cannot move or rename a Root directory';
  strErr75 = 'Cancelled by User';
  strErr76 = 'The destination is a subtree of the source.';
  strErr78 = 'Security settings denied access to the source.';
  strErr79 = 'The source or destination path exceeded MAX_PATH.';
  strErr7A = 'Invalid multiple destination paths';
  strErr7C = 'The path in the source or destination or both was invalid.';
  strErr7D = 'The source and destination have the same parent folder.';
  strErr7E = 'The destination path is an existing file.';
  strErr80 = 'The destination path is an existing folder.';
  strErr81 = 'The name of the file exceeds MAX_PATH.';
  strErr82 = 'The destination is a read-only CD-ROM, possibly unformatted.';
  strErr83 = 'The destination is a read-only DVD, possibly unformatted.';
  strErr84 = 'The destination is a writable CD-ROM, possibly unformatted.';
  strErr85 = 'The file is too large for the destination media or file system.';
  strErr86 = 'The source is a read-only CD-ROM, possibly unformatted.';
  strErr87 = 'The source is a read-only DVD, possibly unformatted.';
  strErr88 = 'The source is a writable CD-ROM, possibly unformatted.';
  strErrB7 = 'MAX_PATH was exceeded during the operation.';

implementation
uses
  SysUtils,
  StrUtils,
  ShellApi,
  
  TPmaLogUnit,      // Log
  TGenFolderUnit,   // FOlder Object
  TDiffFrameUnit,   // Show Difference Frame
  MainBackupUnit,   // Main Backup Form
  TPmaClassesUnit;  // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;

//------------------------------------------------------------------------------
//  Create Folder Object from a Name
//------------------------------------------------------------------------------
constructor TGenFole.Create(
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
begin
  inherited Create;

  // Set In data or default data

  objName    := sName;
  objParent  := pParent;
  objSource  := bSource;

  objMatch   := nil;
  objDiff    := fdUndef;
  objAction  := faNone;

  objSize    := nSize;
  objTimeCre := TimeCre;
  objTimeMod := TimeMod;
  objTimeAcc := TimeAcc;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TGenFole.Destroy;
begin
  // Nothing to destroy

  inherited Destroy;
end;
//------------------------------------------------------------------------------
// Return Src or Trg depending on Side
//------------------------------------------------------------------------------
function TGenFole.GetSourceStr: string;
begin
  if objSource then result := strSource else result := strTarget;
end;
//------------------------------------------------------------------------------
// Return true if Action
//------------------------------------------------------------------------------
function TGenFole.IsAction: boolean;
begin
  result := (objAction <> faNone);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TGenFole.LogFole;
begin
end;
//------------------------------------------------------------------------------
//  Delete Fole
//------------------------------------------------------------------------------
procedure TGenFole.Delete;
begin
  Log('Delete Abstract');
end;
//------------------------------------------------------------------------------
//  Delete File
//------------------------------------------------------------------------------
procedure TGenFole.Copy;
begin
  Log('Copy Abstract');
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
function TGenFole.GetPath:string;
begin
  // Add Leaf Name

  result := objName;

  // Ask the parent for its full path and add it

  if (objParent <> nil) and (objParent is TGenFolder) then
    result := SysUtils.IncludeTrailingPathDelimiter(
                TGenFolder(objParent).pPath) + result;
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
function TGenFole.GetMatchedName:string;
var
  pPar   : TGenFolder;
  sThis  : string;
  sMatch : string;
begin
  result := '';

  if (objMatch <> nil) then
    begin
      // Use the Match Directly

      result := objMatch.pPath;
    end
  else
    begin
      // Walk parents to get the first Matched pairs
      // This cant fail, since base Source and Target is Matched

      pPar := objParent as TGenFolder;
      while (pPar <> nil) and (pPar.pMatch = nil) do
        begin
          pPar := pPar.pParent as TGenFolder;
        end;

      // Did we got anything

      if (pPar <> nil) and (pPar.pMatch <> nil) then
        begin
          // Get full Path of this file where the Match occurred

          sThis  := pPar.pPath;

          // Get the full path of the other side where the match occurred

          sMatch := pPar.pMatch.pPath;

          // Substitute the left part of this side to the other side
          // Leave the right part as is

          result := AnsiRightStr(self.pPath,length(self.pPath)-length(sThis));
          result := sMatch + result;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Return Total Count
//------------------------------------------------------------------------------
function TGenFole.GetTotCount:integer;
begin
  result := 1;
end;
//------------------------------------------------------------------------------
//  Return Size of File
//------------------------------------------------------------------------------
function TGenFole.GetTotSize:int64;
begin
  result := objSize;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TGenFole.GetErrCount:integer;
begin
  result := 0;
  //if (objAction <> faNone) then Inc(result);
  if (objAction <> faNone) then result := result + self.GetTotCount;
  //if (objAction = faDel) then result := result + 1;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy (Dont care about delete)
//------------------------------------------------------------------------------
function TGenFole.GetErrSize:int64;
begin
  result := 0;
  if (objAction = faCopy) then result := result + self.GetTotSize;
end;
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
procedure TGenFole.CalcDiffAction(const Mode : TBackupMode);
begin
  // Abstract: Do nothing
end;
//------------------------------------------------------------------------------
//  Show File Difference
//------------------------------------------------------------------------------
procedure TGenFole.ShowDiff(
        const DiffFrame : TFrame);
begin
  if (DiffFrame <> nil) then
    TDiffFrame(DiffFrame).AddDiffFole(self);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TGenFole.PerformBackup;
begin
  // Abstract: Do Nothing
end;
//------------------------------------------------------------------------------
//
//                                CLASS METHODS
//
//------------------------------------------------------------------------------
//  Translate File Difference to String
//------------------------------------------------------------------------------
function TGenFole.GetDiffStr: string;
begin
  result := strDiffUndef;

  case objDiff of
    fdSame    : result := strDiffSame;
    fdExist   : result := strDiffExist;
    fdNewer   : result := strDiffNewer;
    fdOlder   : result := strDiffOlder;   
  end;
end;
//------------------------------------------------------------------------------
//  Translate File Actions to String
//------------------------------------------------------------------------------
function TGenFole.GetActionStr: string;
begin
  result := strActionUndef;

  case objAction of
    faNone : result := strActionNone;
    faCopy : result := strActionCopy;
    faDel  : result := strActionDel;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to Icon
//------------------------------------------------------------------------------
class function TGenFole.BackupModeToIndex(const Mode : TBackupMode): integer;
begin
  result := 0;

  case Mode of
    bmSourceToTarget    : result := 0;
    bmSourceMergeTarget : result := 1;
    bmSynchronize       : result := 2;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to Icon
//------------------------------------------------------------------------------
class function TGenFole.BackupIndexToMode(const Ind : integer): TBackupMode;
begin
  result := bmSourceToTarget;

  case Ind of
    0 : result := bmSourceToTarget;
    1 : result := bmSourceMergeTarget;
    2 : result := bmSynchronize;
  end;
end;
//------------------------------------------------------------------------------
//  Translate BackupMode to String
//------------------------------------------------------------------------------
class Function TGenFole.BackupModeToStr  (const Mode : TBackupMode): string;
begin
  result := strUndef;

  case Mode of
    bmSourceToTarget    : result := strSourceToTarget;
    bmSourceMergeTarget : result := strSourceMergeTarget;
    bmSynchronize       : result := strSynchronize;
  end;
end;
//------------------------------------------------------------------------------
//  Return Shell Error Codes
//------------------------------------------------------------------------------
function GetShErrStr(const errcode : integer): string;
begin
  case errcode of
    $71 : result := strErr71;
    $72 : result := strErr72;
    $73 : result := strErr73;
    $74 : result := strErr74;
    $75 : result := strErr75;
    $76 : result := strErr76;
    $78 : result := strErr78;
    $79 : result := strErr79;
    $7A : result := strErr7A;
    $7C : result := strErr7C;
    $7D : result := strErr7D;
    $7E : result := strErr7E;
    $80 : result := strErr80;
    $81 : result := strErr81;
    $82 : result := strErr82;
    $83 : result := strErr83;
    $84 : result := strErr84;
    $85 : result := strErr85;
    $86 : result := strErr86;
    $87 : result := strErr87;
    $88 : result := strErr88;
    $B7 : result := strErrB7;
  else
    result := strErrNoErr;
  end;
end;
//------------------------------------------------------------------------------
//  Shell Delete
//------------------------------------------------------------------------------
function TGenFole.ShDelete(
        const filename  : string;  // File to delete
        out   sErr      : string)  // Error String
                        : boolean; // True if deleted
var
  fos  : ShellApi.TSHFileOpStruct;
  rc   : integer;
begin
  result    := true;
  sErr      := '';

  if (length(filename) = 0) then
    begin
      sErr   := strErrNoPath;
      result := false;
      EXIT;
    end;

  ZeroMemory(@fos, SizeOf(fos));

  with fos do
    begin
      wFunc  := FO_DELETE;
      pFrom  := PChar(filename + #0);
      fFlags := FOF_NOCONFIRMATION;
      fFlags := fFlags or FOF_SILENT
    end;

  rc := ShellApi.ShFileOperation(fos);
  if (rc <> 0) then
    begin
      sErr := strErrDeleteFile + filename + strErrCode + GetShErrStr(rc);
      result := false;
    end;
end;
//------------------------------------------------------------------------------
//  Shell Copy
//------------------------------------------------------------------------------
function TGenFole.ShCopy(
        const frompath  : string;  // File to copy
        const topath    : string;  // Destination
        out   sErr      : string)  // Error String
                        : boolean; // True if deleted
var
  fos : ShellApi.TSHFileOpStruct;
  rc  : integer;
begin
  result    := true;
  sErr      := '';

  if (length(frompath) = 0) or (length(topath) = 0) then
    begin
      sErr   := strErrNoPath;
      result := false;
      EXIT;
    end;

  // Set up Copy Shell Command

  ZeroMemory(@fos, SizeOf(fos));

  with fos do
    begin
      wFunc  := FO_COPY;
      fFlags := FOF_FILESONLY or FOF_NOCONFIRMATION;
      pFrom  := PChar(frompath + #0);
      pTo    := PChar(topath + #0);

      fFlags := fFlags or FOF_SILENT
    end;

  rc := ShellApi.ShFileOperation(fos);
  if (rc <> 0) then
    begin
      sErr := strErrCopyFile + frompath + strErrTo + topath +
                       strErrCode + GetShErrStr(rc);
      result := false;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TGenFole);
end.
 