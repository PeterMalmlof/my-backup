object MainBackup: TMainBackup
  Left = 297
  Top = 646
  Width = 429
  Height = 370
  Caption = 'MainBackup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ViewSplitter: TSplitter
    Left = 137
    Top = 0
    Width = 8
    Height = 336
    OnMoved = ViewSplitterMoved
  end
  object ViewPanel: TPanel
    Left = 0
    Top = 0
    Width = 137
    Height = 336
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'ViewPanel'
    Constraints.MaxWidth = 400
    Constraints.MinWidth = 100
    ParentColor = True
    TabOrder = 0
    OnResize = ViewPanelResize
    inline TaskViewFrame1: TTaskViewFrame
      Left = 16
      Top = 16
      Width = 115
      Height = 103
      TabOrder = 0
    end
    object LogMemo: TGenMemo
      Left = 8
      Top = 136
      Width = 121
      Height = 57
      ReadOnly = True
      WordWrap = False
      Border = True
      ColorFore = clMoneyGreen
      ColorHigh = clCream
      UseTabs = False
      Tabs = 0
      TabOrder = 1
    end
  end
  object DetailPanel: TPanel
    Left = 145
    Top = 0
    Width = 276
    Height = 336
    Align = alClient
    BevelOuter = bvNone
    Caption = 'DetailPanel'
    Constraints.MinWidth = 100
    ParentColor = True
    TabOrder = 1
    OnResize = DetailPanelResize
    inline TaskFrame1: TTaskFrame
      Left = 8
      Top = 0
      Width = 134
      Height = 43
      TabOrder = 0
    end
    inline PairDetailFrame1: TPairDetailFrame
      Left = 8
      Top = 32
      Width = 251
      Height = 98
      TabOrder = 1
    end
    inline DiffFrame1: TDiffFrame
      Left = 16
      Top = 152
      Width = 216
      Height = 106
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
    end
    object ProgressBar: TGenProgressBar
      Left = 16
      Top = 312
      Width = 249
      Height = 17
      CmdId = -1
      Vertical = False
      ValueMax = 100
      ValueCur = 33
      ValueMin = 0
      ColorFore = clBlack
      ColorHigh = clBlack
      TabOrder = 3
    end
  end
  object PmaLog1: TPmaLog
    OnLog = PmaLog1Log
    Left = 16
    Top = 200
  end
  object AppProperties: TAppProp
    pHintOn = True
    pBorder = True
    pFont.Charset = DEFAULT_CHARSET
    pFont.Color = clBlack
    pFont.Height = 10
    pFont.Name = 'Verdana'
    pFont.Style = []
    pBackColor = clBtnFace
    pForeColor = clSilver
    pHighColor = clCream
    DblBuff = True
    Left = 56
    Top = 200
  end
  object WmMsgFactory1: TWmMsgFactory
    OnLog = WmMsgFactory1Log
    Left = 16
    Top = 232
  end
  object PmaEscape1: TPmaEscape
    Left = 56
    Top = 232
  end
end
