program MyBackup;

uses
  Forms,
  MainBackupUnit in 'MainBackupUnit.pas' {MainBackup},
  TBackupTaskUnit in 'TBackupTaskUnit.pas',
  TBackupTaskListUnit in 'TBackupTaskListUnit.pas',
  TaskViewFrameUnit in 'TaskViewFrameUnit.pas' {TaskViewFrame: TFrame},
  TBackupPairUnit in 'TBackupPairUnit.pas',
  TTaskFrameUnit in 'TTaskFrameUnit.pas' {TaskFrame: TFrame},
  TPairDetailFrameUnit in 'TPairDetailFrameUnit.pas' {PairDetailFrame: TFrame},
  TBackupFolderUnit in 'TBackupFolderUnit.pas',
  TBackupFileUnit in 'TBackupFileUnit.pas',
  TDiffFrameUnit in 'TDiffFrameUnit.pas' {DiffFrame: TFrame},
  TBackupFileListUnit in 'TBackupFileListUnit.pas',
  TBackupFolderListUnit in 'TBackupFolderListUnit.pas',
  TBackupFoleUnit in 'TBackupFoleUnit.pas',
  TGenPickFolderUnit in '..\PmaComp\TGenPickFolderUnit.pas',
  TWmMsgFactoryUnit in '..\PmaComp\TWmMsgFactoryUnit.pas',
  TGenAppPropUnit in '..\PmaComp\TGenAppPropUnit.pas',
  TGenFileSystemUnit in '..\PmaComp\TGenFileSystemUnit.pas',
  TGenPopupMenuUnit in '..\PmaComp\TGenPopupMenuUnit.pas',
  TGenProgressBarUnit in '..\PmaComp\TGenProgressBarUnit.pas',
  TGenStrUnit in '..\PmaComp\TGenStrUnit.pas',
  TGenTextFileUnit in '..\PmaComp\TGenTextFileUnit.pas',
  TGenTreeViewUnit in '..\PmaComp\TGenTreeViewUnit.pas',
  TPmaClassesUnit in '..\PmaComp\TPmaClassesUnit.pas',
  TPmaDateTimeUnit in '..\PmaComp\TPmaDateTimeUnit.pas',
  TPmaFormUtils in '..\PmaComp\TPmaFormUtils.pas',
  TPmaListViewUnit in '..\PmaComp\TPmaListViewUnit.pas',
  TPmaLogUnit in '..\PmaComp\TPmaLogUnit.pas',
  TPmaProcessUtils in '..\PmaComp\TPmaProcessUtils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Backup';
  Application.CreateForm(TMainBackup, MainBackup);
  Application.Run;
end.
