unit TGenAppUinit;

interface

uses
  Windows, Forms;

//------------------------------------------------------------------------------
//  Application Object
//------------------------------------------------------------------------------
type TGenApp = Class(TObject)
  protected
    objSilent : boolean;
    objVerify : boolean;
    objHint   : boolean;

    procedure SetHint (const Value : boolean);

  public
    constructor Create;
    destructor  Destroy; override;

    class procedure StartUp;
    class procedure ShutDown;

    property pSilent : boolean read objSilent write objSilent;
    property pVerify : boolean read objVerify write objVerify;
    property pHint   : boolean read objHint   write SetHint;
end;

var TheApp : TGenApp = nil;

implementation

uses
  TGenIniFileUnit;

const
  sPrfPrf    = 'Preferences';
  sPrfSilent = 'Silent';
  sPrfVerify = 'Verify';
  sPrfHint   = 'Hint';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TGenApp.Create;
var
  IniFile : TGenIniFile;
begin
  inherited;

  // Set default Values

  objSilent := false;
  objVerify := true;
  objHint   := true;

  // Read Inifile

  IniFile := TGenIniFile.Create;

  objSilent := IniFile.ReadBool(sPrfPrf, sPrfSilent, objSilent);
  objVerify := IniFile.ReadBool(sPrfPrf, sPrfVerify, objVerify);
  objHint   := IniFile.ReadBool(sPrfPrf, sPrfHint,   objHint);

  IniFile.Free;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TGenApp.Destroy;
var
  IniFile : TGenIniFile;
begin

  // Read Inifile

  IniFile := TGenIniFile.Create;

  objSilent := IniFile.ReadBool(sPrfPrf, sPrfSilent,objSilent);
  objVerify := IniFile.ReadBool(sPrfPrf, sPrfVerify,objVerify);
  objHint   := IniFile.ReadBool(sPrfPrf, sPrfHint,objHint);

  IniFile.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Set Hint
//------------------------------------------------------------------------------
procedure TGenApp.SetHint (const Value : boolean);
begin
  of (objHint <> Value) then
    begin
      objHint := Value;
      Application.ShowHint := objHint;
    end;
end;
//------------------------------------------------------------------------------
// Startup
//------------------------------------------------------------------------------
class procedure TGenApp.StartUp;
begin
  if (TheApp = nil) then
    begin
      TheApp := TGenApp.Create;
    end;
end;
//------------------------------------------------------------------------------
// ShutDown
//------------------------------------------------------------------------------
class procedure TGenApp.ShutDown;
begin
  if (TheApp <> nil) then
    begin
      TheApp.Free;
      TheApp := nil;
    end;
end;

//------------------------------------------------------------------------------
//                                    END
//------------------------------------------------------------------------------
end.
 