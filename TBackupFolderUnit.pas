unit TBackupFolderUnit;

interface

uses
  Windows, Contnrs, ComCtrls, Forms, SysUtils,

  TBackupFoleUnit,     // Base Fole Object
  TBackupFileUnit,     // File Object
  TBackupFileListUnit; // File List Object
  
//------------------------------------------------------------------------------
//  Generic Folder Object
//------------------------------------------------------------------------------
type TBackupFolder = Class(TBackupFole)
  protected
    objFolderList : TObject;         // All Folders in this Folder
    objFileList   : TBackupFileList; // All Files in This Folder

    //--------------------------------------------------------------------------
    // Internal Methods

    function  GetTotCount : integer; override; // Return Total Count
    function  GetTotSize  : int64;   override; // Return Total Size
    function  GetErrCount : integer; override; // Return Error Count
    function  GetErrSize  : int64;   override; // Return Size To Copy

    // Walk this Folder and update content objects

    procedure WalkFolder;
  public
    constructor Create (
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
                          override;

    destructor  Destroy; override;

    procedure Delete; override;
    procedure Copy;   override;

    // Return SubFolder Object from Leaf Name

    function  FindFolder(const sFolder : string): TBackupFolder;

    // Return SubFile Object from Leaf Name

    function  FindFile (const sFile : string): TBackupFile;

    // Log this Folder

    procedure LogFole; override;

    // Match Files and Folders in this already matched Folder

    procedure MatchFolder(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TBackupFolder);  // The other Side Folder

    // Calculate Difference to Matched File

    procedure CalcDiffAction(const Mode : TBackupMode); override;

    procedure ShowFolderDiff(
        const DiffFrame : TFrame);

    procedure PerformBackup(const Del : boolean); override;

    // Get Debug Count
    
    class function GetDebugCount: integer;
end;

implementation

uses
  TPmaProcessUtils,       // Process Utilities
  TGenFileSystemUnit,     // File System
  TPmaLogUnit,            // Log Unit
  TWmMsgFactoryUnit,      // Message Factory
  TBackupFolderListUnit,  // Folder List Object
  TDiffFrameUnit,         // Show Difference Frame
  TPmaEscapeUnit,
  MainBackupUnit,         // Main Backup Form
  TPmaClassesUnit;        // Classes

var
  DebugCount : integer = 0;

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Folder Object from a Name
//------------------------------------------------------------------------------
constructor TBackupFolder.Create(
          const bSource : boolean;    // Source or Target
          const pParent : TObject;    // Parent Folder
          const sName   : string;     // Leaf Name
          const nSize   : int64;      // FIle Size in Bytes
          const TimeCre : TFileTime;  // Creation Time
          const TimeMod : TFileTime;  // Modified Time
          const TimeAcc : TFileTime); // Access Time
begin
  inherited Create(bSource, pParent, sName, nSize, TimeCre, TimeMod, TimeAcc);

  objFolderList := TBackupFolderList.Create;
  objFileList   := TBackupFileList.Create;

  Inc(DebugCount);

  PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_UP, 0);

  WalkFolder;
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupFolder.Destroy;
begin

  objFolderList.Free;
  objFileList.Free;

  Dec(DebugCount);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete Fole
//------------------------------------------------------------------------------
procedure TBackupFolder.Delete;
var
  sErr      : string;
  bCanceled : boolean;
begin
  if (not PmaEscape.HasEscaped) then
    begin
      // Fist we delete all Folders recursivly

      TBackupFolderList(objFolderList).DeleteFolders;

      //Then we delete all Files Recursivly

      if (not PmaEscape.HasEscaped) then
        objFileList.DeleteFiles;

      // Last we can delete the empty Folder itself

      // Log('Delete Folder: ' + self.GetPath);

      if (not PmaEscape.HasEscaped) then
        begin
          if (not TGenFileSystem.DelFileSh(
                      self.pPath, true, bCanceled, sErr)) then
            begin
              Log(sErr);
            end;
        end;

      PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_UP, 0);
    end;
end;
//------------------------------------------------------------------------------
//  Delete File
//------------------------------------------------------------------------------
procedure TBackupFolder.Copy;
var
  Path, sErr : string;
begin
  if (not PmaEscape.HasEscaped) then
    begin

      // First we create an empty Folder

      // Get Target File Name to Copy to from the matched pair

      if (objMatch <> nil) then
        Path := objMatch.pPath
      else
        Path := self.GetMatchedName;

      // Log('Create Folder: ' + toPath);

      if (not TGenFolder.CreateFolder(Path, sErr)) then
        begin
          Log(sErr);
        end;

      // Then we Copy all SubFolders of the Folder

      if not PmaEscape.HasEscaped then
        TBackupFolderList(objFolderList).CopyFolders;

      // Last we Copy all The Files

      if not PmaEscape.HasEscaped then
        objFileList.CopyFiles;

      PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_UP, 0);
    end;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TBackupFolder.GetTotCount:integer;
begin
  result := inherited GetTotCount +
            TBackupFolderList(objFolderList).pTotCount + objFileList.pTotCount;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy
//------------------------------------------------------------------------------
function TBackupFolder.GetTotSize:int64;
begin
  result := TBackupFolderList(objFolderList).pTotSize + objFileList.pTotSize;
end;
//------------------------------------------------------------------------------
//  Return Error Count after Matching
//------------------------------------------------------------------------------
function TBackupFolder.GetErrCount:integer;
begin
  result := inherited GetErrCount +
            TBackupFolderList(objFolderList).pErrCount + objFileList.pErrCount;
end;
//------------------------------------------------------------------------------
//  Return Size To Copy
//------------------------------------------------------------------------------
function TBackupFolder.GetErrSize:int64;
begin
  result := inherited GetErrSize +
            TBackupFolderList(objFolderList).pErrSize + objFileList.pErrSize;
end;
//------------------------------------------------------------------------------
//  Walk all Folders and Files
//------------------------------------------------------------------------------
procedure TBackupFolder.WalkFolder;
const
  WildCard     = '*.*';
  ThisFolder   = '.';
  ParentFolder = '..';
  CardMax      = 4294967295;
var
  fa      : Integer;
  sr      : TSearchRec;
  sPath   : string;
  pFolder : TBackupFolder;
  pFile   : TBackupFile;
begin
  // Remove all old Folders and Files

  TBackupFolderList(objFolderList).Clear;
  objFileList.Clear;

  // Look for any thing in this Folder

  fa    := SysUtils.faAnyFile;

  sPath := IncludeTrailingPathDelimiter(GetPath);
  if SysUtils.FindFirst(sPath + WildCard, fa, sr) = 0 then
    begin
      repeat
        // Test if User has hit Escape key

        if PmaEscape.HasEscaped then BREAK;

        //sleep(1);
        // filter out some things

        if (sr.Name <> ThisFolder) and (sr.Name <> ParentFolder) then
          begin
            // Is it a Folder

            if ((sr.Attr and faDirectory) = faDirectory) then
              begin
                // Create a New Folder object and Load that

                pFolder := TBackupFolder.Create(
                      objSource,
                      self,
                      sr.Name,
                      0,
                      sr.FindData.ftCreationTime,
                      sr.FindData.ftLastWriteTime,
                      sr.FindData.ftLastAccessTime);

                TBackupFolderList(objFolderList).AddFolder(pFolder);
              end
            else
              begin
                // Create a new File Object and Load That

                pFile := TBackupFile.Create(
                      objSource,
                      self,
                      sr.Name,
                      sr.FindData.nFileSizeLow +
                      sr.FindData.nFileSizeHigh * CardMax,
                      sr.FindData.ftCreationTime,
                      sr.FindData.ftLastWriteTime,
                      sr.FindData.ftLastAccessTime);

                objFileList.AddFile(pFile);
              end;
          end;
      until SysUtils.FindNext(sr) <> 0;

      SysUtils.FindClose(sr);
    end;
end;
//------------------------------------------------------------------------------
//  Get a Sub Folder of this Folder
//------------------------------------------------------------------------------
function TBackupFolder.FindFolder(const sFolder : string): TBackupFolder;
begin
  result := TBackupFolderList(objFolderList).FindFolder(sFolder);
end;
//------------------------------------------------------------------------------
//  Get a Sub Folder of this Folder
//------------------------------------------------------------------------------
function TBackupFolder.FindFile(const sFile : string): TBackupFile;
begin
  result := objFileList.FindFile(sFile);
end;
//------------------------------------------------------------------------------
//
//                             DIFFERENCE CALCULATION
//
//------------------------------------------------------------------------------
//  Get Full Path Name of File
//------------------------------------------------------------------------------
procedure TBackupFolder.CalcDiffAction(const Mode : TBackupMode);
begin
  // Is there a Match

  if (objMatch = nil) then
    begin
      case Mode of
        //----------------------------------------------------------------------
        // Synchronize Source to Target
        //  A) Copy Missing FOlders in Target from Source
        //  B) Delete Folders in Target that are missing in Source
        //----------------------------------------------------------------------
        bmSourceToTarget :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Delete Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faDel;
              end
          end;

        //----------------------------------------------------------------------
        // Merge Source to Target
        //  A) Copy Missing Folders in Target from Source
        //  B) Dont delete FOlders in Target that dont exist in Source
        //----------------------------------------------------------------------
        bmSourceMergeTarget :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Leave It
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faNone;
              end

          end;

        //----------------------------------------------------------------------
        // Synchronize Source and Target
        //  A) Copy Missing Folders in Target from Source
        //  B) Copy Missing Folders in Source from Target
        //----------------------------------------------------------------------
        bmSynchronize :
          begin
            if objSource then
              begin
                //--------------------------------------------------------------
                // This is a Source Folder with no Target File, Copy to Target
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end
            else
              begin
                //--------------------------------------------------------------
                // This is a Target Folder with no Source File, Copy to Source
                //--------------------------------------------------------------
                objDiff   := fdExist;
                objAction := faCopy;
              end

          end;
      end;
    end
  else
    begin
      //------------------------------------------------------------------------
      // There is a Match, dont do anything
      //------------------------------------------------------------------------

      objDiff   := fdSame;
      objAction := faNone;
    end;
end;
//------------------------------------------------------------------------------
//  Match Two Folder Pairs on each other (self is Source, other is Target)
//------------------------------------------------------------------------------
procedure TBackupFolder.LogFole;
begin
  Log('Fold: ' + self.pPath);

  // Match SubFolders to Each other

  TBackupFolderList(objFolderList).LogFolders;

  // Match Files to Each other

  objFileList.LogFoles;
end;
//------------------------------------------------------------------------------
//  Match Two Folder Pairs on each other (self is Source, other is Target)
//------------------------------------------------------------------------------
procedure TBackupFolder.MatchFolder(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TBackupFolder);  // The other Side Folder
begin
  // Match SubFolders to Each other

  TBackupFolderList(objFolderList).MatchFolders(Mode, pFolder);

  // Match Files to Each other

  objFileList.MatchFiles(Mode, pFolder);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TBackupFolder.PerformBackup(const Del : boolean);
begin
  if (not PmaEscape.HasEscaped) then
    begin

      // Perform on this Folder if applicable

      case objAction of
        faCopy  : if (not Del) then Copy;
        faDel   : if Del       then Delete;
      end;

      // Then do the SubFolders and SubFiles

      if (not PmaEscape.HasEscaped) then
        TBackupFolderList(objFolderList).PerformBackup(Del);

      if (not PmaEscape.HasEscaped) then
        TBackupFileList(objFileList).PerformBackup(Del);
    end;
end;
//------------------------------------------------------------------------------
//
//                              SHOW DIFFERENCES
//
//------------------------------------------------------------------------------
//  Show Diff Frame result
//------------------------------------------------------------------------------
procedure TBackupFolder.ShowFolderDiff(
        const DiffFrame : TFrame);
begin
  if (DiffFrame is TDiffFrame) then
    begin
      // Walk all Folders and Show Differences

      TBackupFolderList(objFolderList).ShowDiff(DiffFrame);

      // Walk all Files and Show Differences

      objFileList.ShowDiff(DiffFrame);
    end;
end;
//------------------------------------------------------------------------------
//  Get Debug Count
//------------------------------------------------------------------------------
class function TBackupFolder.GetDebugCount: integer;
begin
  result := DebugCount;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupFolder);
end.
