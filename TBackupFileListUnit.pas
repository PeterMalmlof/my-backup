unit TBackupFileListUnit;

interface

uses
  Windows, Contnrs, Forms, SysUtils, StrUtils,

  TBackupFoleUnit, // Base Fole Object
  TBackupFileUnit; // File Object

//------------------------------------------------------------------------------
//  File List Object
//------------------------------------------------------------------------------
type TBackupFileList = Class(TObject)
  protected
    objFileList : TObjectList;     // List Of Files

    function GetFile(var Iter : integer; out pFile : TBackupFile):boolean;

    function GetTotSize : int64;
    function GetErrSize : int64;

    function GetTotCount : integer;
    function GetErrCount : integer;

  public
    constructor Create;
    destructor  Destroy; override;

    procedure Clear;

    procedure DeleteFiles;
    procedure CopyFiles;

    procedure LogFoles;

    procedure AddFile(const pFile : TBackupFile);

    function  FindFile(const sFile : string): TBackupFile;

    procedure MatchFiles(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TObject);     // The other Side Folder

    procedure ShowDiff(
        const DiffFrame : TFrame);

    procedure PerformBackup(const Del : boolean);

    property pTotSize : int64 read GetTotSize;
    property pErrSize : int64 read GetErrSize;

    property pTotCount : integer read GetTotCount;
    property pErrCount : integer read GetErrCount;
end;

implementation

uses
  TPmaLogUnit,        // Log
  TBackupFolderUnit,  // Folder Object
  TDiffFrameUnit,     // Show Difference Frame
  TPmaEscapeUnit,
  MainBackupUnit,     // Main Backup Form
  TPmaClassesUnit;    // Classes

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create Object
//------------------------------------------------------------------------------
constructor TBackupFileList.Create;
begin
  inherited Create;

  objFileList := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
//  Destroy Object
//------------------------------------------------------------------------------
destructor TBackupFileList.Destroy;
begin
  objFileList.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
//  Delete all Foles in a Folder List
//------------------------------------------------------------------------------
procedure TBackupFileList.DeleteFiles;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    begin
      if PmaEscape.HasEscaped then BREAK;

      pFile.Delete;
    end;
end;
//------------------------------------------------------------------------------
//  Copy All Foles in a Folder List
//------------------------------------------------------------------------------
procedure TBackupFileList.CopyFiles;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    begin
      if PmaEscape.HasEscaped then BREAK;

      pFile.Copy;
    end;
end;
//------------------------------------------------------------------------------
//  Iterator
//------------------------------------------------------------------------------
function TBackupFileList.GetFile
            (var Iter : integer; out pFile : TBackupFile):boolean;
begin
  result := false;

  if (Iter >= 0) and (Iter < objFileList.Count) and
     (objFileList[Iter] is TBackupFile) then
    begin
      pFile  := objFileList[Iter] as TBackupFile;
      result := true;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//  Clear all Files in FileList
//------------------------------------------------------------------------------
procedure TBackupFileList.Clear;
begin
  objFileList.Clear;
end;
//------------------------------------------------------------------------------
//  Log All Files
//------------------------------------------------------------------------------
procedure TBackupFileList.LogFoles;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    pFile.LogFole;
end;
//------------------------------------------------------------------------------
//  Get File Count
//------------------------------------------------------------------------------
function TBackupFileList.GetTotCount:integer;
begin
  result := objFileList.Count;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TBackupFileList.GetTotSize:int64;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    result := result + pFile.pTotSize;
end;
//------------------------------------------------------------------------------
//  Get Size of all Files
//------------------------------------------------------------------------------
function TBackupFileList.GetErrSize:int64;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then result := result + pFile.pErrSize;
end;
//------------------------------------------------------------------------------
//  Get Error File Count
//------------------------------------------------------------------------------
function TBackupFileList.GetErrCount:integer;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  result := 0;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then result := result + 1;
end;
//------------------------------------------------------------------------------
//  Add a File to File List
//------------------------------------------------------------------------------
procedure TBackupFileList.AddFile(const pFile : TBackupFile);
begin
  if (pFile <> nil) and (pFile is TBackupFile) then
    objFileList.Add(pFile);
end;
//------------------------------------------------------------------------------
//  Find a File from its Name
//------------------------------------------------------------------------------
function TBackupFileList.FindFile(const sFile : string): TBackupFile;
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  result := nil;

  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if AnsiSameText(pFile.pName, sFile) then
      begin
        result := pFile;
        break;
      end;
end;
//------------------------------------------------------------------------------
//  Match Files
//------------------------------------------------------------------------------
procedure TBackupFileList.MatchFiles(
        const Mode    : TBackupMode;  // Backup Mode
        const pFolder : TObject);     // The other Side Folder
var
  Iter   : integer;
  pFile  : TBackupFile;
  pOther : TBackupFile;
begin

  // Match Files to Each other

  if (pFolder <> nil) and (pFolder is TBackupFolder) then
    begin
      // Walk all Subfiles of this Folder

      Iter := 0;
      while self.GetFile(Iter, pFile) do
        begin
          if (pFile.pMatch = nil) then
            begin
              pOther := TBackupFolder(pFolder).FindFile(pFile.pName);
              if (pOther <> nil) and (pOther.pMatch = nil) then
                begin
                  // Match them together

                  pFile.pMatch  := pOther;
                  pOther.pMatch := pFile;
                end;
            end;

            // Calculate Difference and Action

            pFile.CalcDiffAction(Mode);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Calc Differences after Matching
//------------------------------------------------------------------------------
procedure TBackupFileList.ShowDiff(
        const DiffFrame : TFrame);
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    if pFile.IsAction then pFile.ShowDiff(DiffFrame);
end;
//------------------------------------------------------------------------------
//  Perform Backup
//------------------------------------------------------------------------------
procedure TBackupFileList.PerformBackup(const Del : boolean);
var
  Iter  : integer;
  pFile : TBackupFile;
begin
  Iter := 0;
  while self.GetFile(Iter, pFile) do
    pFile.PerformBackup(Del);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TBackupFileList);
end.
