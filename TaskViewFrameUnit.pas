unit TaskViewFrameUnit;

interface

uses
  Windows, ImgList, Classes, Controls, Forms, Menus, ComCtrls, SysUtils,

  TGenAppPropUnit,    // Application Properties
  TBackupPairUnit,    // Backup Pair Object
  TBackupTaskUnit,    // Backup Task Object
  TGenPopupMenuUnit,  // Popup Menu
  TGenTreeViewUnit;   // Tree View

type
  TTaskViewFrame = class(TFrame)
    ViewImages   : TImageList;
    TaskViewMenu : TGenPopupMenu;
    TaskView: TGenTreeView;

    procedure FrameResize      (Sender: TObject);
    procedure TaskViewMenuPopup(Sender: TObject);
    procedure TaskViewEnter    (Sender: TObject);
    procedure TaskViewSelect(Sender: TObject; Node: TGenTreeNode;
      Selected: Boolean);
    procedure TaskViewEdit(Node: TGenTreeNode; var Value: String;
      var Allow: Boolean);
    procedure TaskViewLog(sLine: String);
  private
    objRunning : boolean;

    // App Menues

    procedure OnAppHelp (Sender: TObject);
    procedure OnAppExit (Sender: TObject);

    // Pref Menues

    procedure OnAppVerify (Sender: TObject);

    // Task Menues

    procedure OnTaskNew    (Sender: TObject);
    procedure OnTaskDelete (Sender: TObject);
    procedure OnReviewTask (Sender: TObject);
    procedure OnBackupTask (Sender: TObject);
    procedure OnClearTask  (Sender: TObject);

    // Folder Pair Menues

    procedure OnPairAdd    (Sender: TObject);
    procedure OnPairDel    (Sender: TObject);
    procedure OnReviewPair (Sender: TObject);
    procedure OnBackupPair (Sender: TObject);
    procedure OnClearPair  (Sender: TObject);
  public
    // StartUp the View FRame

    procedure Startup;
    procedure ShutDown;

    procedure SetReadOnly(const Value : boolean);

    // Get Current Selected Task

    function  GetCurTask: TBackupTask;

    // Get Current Selected Folder Pair

    function  GetCurPair: TBackupPair;

    procedure BackupTask(const pTask : TBackupTask;const Perform : boolean);
    procedure BackupPair(const pPair : TBackupPair;const Perform : boolean);

    // Refresh Content of Task View Frame

    procedure RefreshTaskView;
  end;

implementation

{$R *.dfm}

uses
  TPmaLogUnit,          // Log
  TBackupTaskListUnit,  // Backup Task List
  MainBackupUnit,       // Main Backup Form
  TWmMsgFactoryUnit,    // Message Factory
  TPmaEscapeUnit,
  TPmaClassesUnit;      // Classes

const
  DoYou1  = 'Do You really want to delete "';
  DoYou2  = '" ?';
  DelTask = 'Delete Task';
  DelPair = 'Delete Folder Pair';

  strTaskNotValid = 'ERROR: Task Not Valid ';
  strPairNotValid = 'ERROR: Pair Not Valid ';

//------------------------------------------------------------------------------
//  Menu Commands
//------------------------------------------------------------------------------
const
  strSplitter = '-';
  strTask     = 'Task';
  strPair     = 'Pair';
  strNew      = 'New ';
  strDel      = 'Delete ';
  strReview   = 'Review ';
  strBackup   = 'Backup ';
  strClear    = 'Clear ';
  
  strPref     = 'Preferences';
  strSilent   = 'Silent';
  strVerify   = 'Verify';
  strHint     = 'Hint';
  strHelp     = 'Help';
  strExit     = 'Exit';

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;

//------------------------------------------------------------------------------
//  Startup Task View Frame
//------------------------------------------------------------------------------
procedure TTaskViewFrame.Startup;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  TaskView.StartUp;
  TaskViewMenu.StartUp;
  RefreshTaskView;

  objRunning := true;
end;
//------------------------------------------------------------------------------
//  Startup Task View Frame
//------------------------------------------------------------------------------
procedure TTaskViewFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objRunning := false;

  TaskView.ShutDown;
  TaskViewMenu.ShutDown;
end;
//------------------------------------------------------------------------------
//  Set Current Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.SetReadOnly(const Value : boolean);
begin
  //TaskView.readOnly := true;
end;
//------------------------------------------------------------------------------
//  Resize View Panel
//------------------------------------------------------------------------------
procedure TTaskViewFrame.FrameResize(Sender: TObject);
begin
  TaskView.Left   := 0;
  TaskView.Top    := 0;
  TaskView.Width  := self.ClientWidth;
  TaskView.Height := self.ClientHeight;
end;
//------------------------------------------------------------------------------
//  Refresk Task View
//------------------------------------------------------------------------------
procedure TTaskViewFrame.RefreshTaskView;
begin
  // Walk all Tasks and make sure there is a Task View Item

  TaskList.RefreshTaskView(TaskView);
end;
//------------------------------------------------------------------------------
//  User has Changed Selection
//------------------------------------------------------------------------------
procedure TTaskViewFrame.TaskViewSelect(Sender: TObject;
  Node: TGenTreeNode; Selected: Boolean);
begin
  if objRunning then
    MainBackup.SetCurSelection(self.GetCurTask, self.GetCurPair);
end;
//------------------------------------------------------------------------------
//  Get Current Task
//------------------------------------------------------------------------------
function TTaskViewFrame.GetCurTask: TBackupTask;
begin
  result := nil;

  if objRunning then
    begin
      if Assigned(TaskView.Selected) and
         Assigned(TaskView.Selected.Data) and
         (TObject(TaskView.Selected.Data) is TBackupTask) then
        begin
          // Its a Task Node

          result := TaskView.Selected.Data as TBackupTask;
        end
      else if Assigned(TaskView.Selected) and
              Assigned(TaskView.Selected.Data) and
              (TObject(TaskView.Selected.Data) is TBackupPair) then
        begin
          // Its a Folder Pair Node, get its Task

          result := TaskList.GetTaskFromPair(
                    TBackupPair(TaskView.Selected.Data));
        end;
  end;
end;
//------------------------------------------------------------------------------
//  Get Current Pair
//------------------------------------------------------------------------------
function TTaskViewFrame.GetCurPair: TBackupPair;
begin
  result := nil;

  if objRunning and Assigned(TaskView.Selected) and
     Assigned(TaskView.Selected.Data) and
     (TObject(TaskView.Selected.Data) is TBackupPair) then
    begin
      // Its a Folder Pair Node

      result := TaskView.Selected.Data as TBackupPair;
    end
end;
//------------------------------------------------------------------------------
//
//                                     MENUES
//
//------------------------------------------------------------------------------
//  Popup Task View Menu
//------------------------------------------------------------------------------
procedure TTaskViewFrame.TaskViewMenuPopup(Sender: TObject);
var
  pTask : TBackupTask;
  pPair : TBackupPair;
  pPref : TGenMenuItem;
  pMenu : TGenMenuItem;
begin
  if not MainBackup.IsOpen then EXIT;

  pTask := self.GetCurTask;
  pPair := self.GetCurPair;

  TaskViewMenu.Items.Clear;

  TaskViewMenu.Font      := App.pFont;
  TaskViewMenu.BackColor := App.pBackColor;
  TaskViewMenu.ForeColor := App.pForeColor;
  TaskViewMenu.HighColor := App.pHighColor;

  if (pPair = nil) then
    begin
      //------------------------------------------------------------------------
      // Build Task Menu
      //------------------------------------------------------------------------

      // Add New Task Command

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strNew + strTask;
      pMenu.OnClick := OnTaskNew;
      TaskViewMenu.Items.Add(pMenu);

      // Add Delete Task Command

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strDel + strTask;
      pMenu.OnClick := OnTaskDelete;
      pMenu.Enabled := (pTask <> nil);
      TaskViewMenu.Items.Add(pMenu);

      // Add line

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strSplitter;
      TaskViewMenu.Items.Add(pMenu);

      // Add Review Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strReview + strTask;
      pMenu.OnClick := OnReviewTask;
      pMenu.Enabled := (pTask <> nil) and pTask.IsValid;
      TaskViewMenu.Items.Add(pMenu);

      // Add Backup Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strBackup + strTask;
      pMenu.OnClick := OnBackupTask;
      pMenu.Enabled := (pTask <> nil) and pTask.IsValid;
      TaskViewMenu.Items.Add(pMenu);

      // Add Clear Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strClear + strTask;
      pMenu.OnClick := OnClearTask;
      pMenu.Enabled := (pTask <> nil);
      TaskViewMenu.Items.Add(pMenu);

      // Add line

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strSplitter;
      TaskViewMenu.Items.Add(pMenu);

      // Add New Pair

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strNew + strPair;
      pMenu.OnClick := OnPairAdd;
      TaskViewMenu.Items.Add(pMenu);

      // Add line

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strSplitter;
      TaskViewMenu.Items.Add(pMenu);

      // Add Preferences

      pPref := TGenMenuItem.Create(TaskViewMenu);
      pPref.Caption := strPref;
      TaskViewMenu.Items.Add(pPref);

        pMenu := TGenMenuItem.Create(TaskViewMenu);
        pMenu.Caption := strVerify;
        pMenu.OnClick := OnAppVerify;
        pMenu.Checked := MainBackup.pVerify;
        pPref.Add(pMenu);

        App.AddPrefMenu(TaskViewMenu,pPref,false,true);

      // Add Help Command

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strHelp;
      pMenu.OnClick := OnAppHelp;
      TaskViewMenu.Items.Add(pMenu);

      // Add Exit Command

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strExit;
      pMenu.OnClick := OnAppExit;
      TaskViewMenu.Items.Add(pMenu);
    end
  else
    begin
      //------------------------------------------------------------------------
      // Build Pair Menu
      //------------------------------------------------------------------------

      // Delete Pair Menu

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strDel + strPair;
      pMenu.OnClick := OnPairDel;
      pMenu.Enabled := (pPair <> nil);
      TaskViewMenu.Items.Add(pMenu);

      // Add line

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strSplitter;
      TaskViewMenu.Items.Add(pMenu);

      // Add Review Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strReview + strPair;
      pMenu.OnClick := OnReviewPair;
      pMenu.Enabled := (pPair <> nil) and pPair.IsValid;
      TaskViewMenu.Items.Add(pMenu);

      // Add Backup Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strBackup + strPair;
      pMenu.OnClick := OnBackupPair;
      pMenu.Enabled := (pPair <> nil) and pPair.IsValid;
      TaskViewMenu.Items.Add(pMenu);

      // Add Clear Task

      pMenu := TGenMenuItem.Create(TaskViewMenu);
      pMenu.Caption := strClear + strPair;
      pMenu.OnClick := OnClearPair;
      pMenu.Enabled := (pPair <> nil);
      TaskViewMenu.Items.Add(pMenu); 
    end;
end;
//------------------------------------------------------------------------------
//
//                                   APP MENUES
//
//------------------------------------------------------------------------------
//  Exit Application
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnAppExit(Sender: TObject);
begin
  PmaEscape.Escape;
  //MainBackup.AcceptEscape(true);
  Application.Terminate;
end;
//------------------------------------------------------------------------------
//  Exit Application
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnAppHelp(Sender: TObject);
begin
  MainBackup.ShowHelp;
end;
//------------------------------------------------------------------------------
//
//                               PREFERENCES MENUES
//
//------------------------------------------------------------------------------
//  Toggle Verify Mode
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnAppVerify(Sender: TObject);
begin
  MainBackup.pVerify := not MainBackup.pVerify;
end;
//------------------------------------------------------------------------------
//
//                                   TASK MENUES
//
//------------------------------------------------------------------------------
//  Create a New Task
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnTaskNew(Sender: TObject);
var
  sTask : string;
begin
  sTask := TaskList.GetUniqueTaskName(TBackupTask.GetTaskNewName);
  TaskList.AddTask(TBackupTask.Create(sTask));
  RefreshTaskView;
end;
//------------------------------------------------------------------------------
//  Delete an Existing Task
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnTaskDelete(Sender: TObject);
var
  pTask : TBackupTask;
  sTmp  : string;
begin
  pTask := GetCurTask;
  if (pTask <> nil) then
    begin
      sTmp := DoYou1 + pTask.pName + DoYou2;

      if Application.MessageBox(
              PAnsiChar(sTmp), DelTask, MB_OKCANCEL) = IDOK then
        begin
          TaskView.Selected := nil;
          TaskList.RemoveTask(pTask);
          TaskList.RefreshTaskView(TaskView);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Review Task Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.BackupTask(
            const pTask : TBackupTask;const Perform : boolean);
begin
  if (pTask <> nil) and pTask.IsValid then
    begin
      MainBackup.BeginWork;

      if Perform then
        Log(strBackup + strTask + ': ' + pTask.pName)
      else
        Log(strBackup + strTask + ': ' + pTask.pName);

      if (pTask.pTotCount > 0) then
        PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pTask.pTotCount * 2))
      else
        PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(200));

      Application.ProcessMessages;

      pTask.ReviewBackup(Perform);

      if Perform and (not PmaEscape.HasEscaped) then
        begin
          Application.ProcessMessages;

          PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, 0);
          PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pTask.pErrCount));

          pTask.PerformBackup;

          if MainBackup.pVerify then
            begin
              //PmaEscape.Reset;
              MainBackup.pDiffFrame.ClearDiff;

              if (pTask.pTotCount > 0) then
                PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pTask.pTotCount * 2))
              else
                PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(200));

              PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));

              Application.ProcessMessages;

              pTask.ReviewBackup(Perform);
            end;
        end;

      MainBackup.EndWork;
    end
  else if (pTask <> nil) then
    begin
      Log(strTaskNotValid + pTask.pName);
    end;
end;
//------------------------------------------------------------------------------
//  Review Task Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnReviewTask(Sender: TObject);
begin
  BackupTask(self.GetCurTask, false);
end;
//------------------------------------------------------------------------------
//  Backup Task Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnBackupTask(Sender: TObject);
begin
  BackupTask(self.GetCurTask, true);
end;
//------------------------------------------------------------------------------
//  Backup Folder Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnClearTask(Sender: TObject);
var
  pTask : TBackupTask;
begin
  pTask := self.GetCurTask;
  if (pTask <> nil) and pTask.IsValid then
    begin
      //MainBackup.AcceptEscape(false);
      MainBackup.pDiffFrame.ClearDiff;
      PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));

      pTask.Clear;

      MainBackup.LogMem;
    end;
end;
//------------------------------------------------------------------------------
//
//                                FOLDER PAIR MENUES
//
//------------------------------------------------------------------------------
//  Add a new Folder Pair to Current Task
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnPairAdd(Sender: TObject);
var
  pTask : TBackupTask;
  pPair : TBackupPair;
begin
  pTask := self.GetCurTask;
  if (pTask <> nil) then
    begin
      pPair := TBackupPair.Create(pTask, TBackupPair.GetPairNewName);
      pTask.AddPair(pPair);
      RefreshTaskView;
    end;
end;
//------------------------------------------------------------------------------
//  Remove an Existing Folder Pair From Current Task
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnPairDel(Sender: TObject);
var
  pPair : TBackupPair;
  pTask : TBackupTask;
  sTmp  : string;
begin
  pTask := self.GetCurTask;
  pPair := self.GetCurPair;
  if Assigned(pTask) and Assigned(pPair) then
    begin
      sTmp := DoYou1 + pPair.pName + DoYou2;

      if Application.MessageBox(
              PAnsiChar(sTmp), DelPair, MB_OKCANCEL) = IDOK then
        begin
          TaskView.Selected := nil;
          pTask.RemovePair(pPair);
          self.RefreshTaskView;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Review Folder Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.BackupPair(
        const pPair : TBackupPair;const Perform : boolean);
begin
  if (pPair <> nil) and pPair.IsValid then
    begin
      MainBackup.BeginWork;

      if (pPair.pTotCount > 0) then
        PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pPair.pTotCount * 2))
      else
        PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(200));

      Application.ProcessMessages;

      pPair.DoReview(Perform);

      if Perform and (not PmaEscape.HasEscaped) then
        begin
          Application.ProcessMessages;

          PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));
          PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pPair.pErrCount));

          pPair.DoBackup(true);   // Delete first
          pPair.DoBackup(false);  // Copy Then

          if MainBackup.pVerify then
            begin
              //MainBackup.AcceptEscape(false);
              MainBackup.pDiffFrame.ClearDiff;

              if (pPair.pTotCount > 0) then
                PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(pPair.pTotCount*2))
              else
                PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_MAX, DWORD(200));

              PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));

              Application.ProcessMessages;

              pPair.DoReview(Perform);
            end;
        end;

      MainBackup.EndWork;
    end
  else if (pPair <> nil) then
    begin
      Log(strPairNotValid + pPair.pName);
    end;
end;
//------------------------------------------------------------------------------
//  Review Folder Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnReviewPair(Sender: TObject);
begin
  BackupPair(self.GetCurPair, false);
end;
//------------------------------------------------------------------------------
//  Backup Folder Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnBackupPair(Sender: TObject);
begin
  BackupPair(self.GetCurPair, true);
end;
//------------------------------------------------------------------------------
//  Backup Folder Pair
//------------------------------------------------------------------------------
procedure TTaskViewFrame.OnClearPair(Sender: TObject);
var
  pPair : TBackupPair;
begin
  pPair := self.GetCurPair;
  if (pPair <> nil) and pPair.IsSource and pPair.IsTarget then
    begin
      //MainBackup.AcceptEscape(false);
      MainBackup.pDiffFrame.ClearDiff;
      PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));

      pPair.Clear;

      MainBackup.LogMem;
    end;
end;
//------------------------------------------------------------------------------
//  Hide Help Window if on
//------------------------------------------------------------------------------
procedure TTaskViewFrame.TaskViewEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//  Read Only
//------------------------------------------------------------------------------
procedure TTaskViewFrame.TaskViewEdit(Node: TGenTreeNode;
  var Value: String; var Allow: Boolean);
begin
  Allow := false;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TTaskViewFrame.TaskViewLog(sLine: String);
begin
  Log(sLine);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TTaskViewFrame);
end.
