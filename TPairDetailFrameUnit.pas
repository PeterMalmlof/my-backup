unit TPairDetailFrameUnit;

interface

uses
  Windows, Classes, Controls, Forms, StdCtrls, StrUtils, SysUtils, DateUtils,

  TBackupPairUnit;

type
  TPairDetailFrame = class(TFrame)
    LabelSource   : TLabel;
    LabelTarget   : TLabel;
    SourceFile    : TEdit;
    TargetFile    : TEdit;
    LabelName     : TLabel;
    PairName      : TEdit;
    LabelTotCount : TLabel;
    LabelTotSize  : TLabel;
    MergeTarget: TCheckBox;
    LabelDate: TLabel;
    procedure FrameResize        (Sender: TObject);
    procedure PairNameChange     (Sender: TObject);
    procedure SourceFileChange   (Sender: TObject);
    procedure TargetFileChange   (Sender: TObject);
    procedure SourceFileDblClick (Sender: TObject);
    procedure TargetFileDblClick (Sender: TObject);
    procedure PairNameEnter(Sender: TObject);
    procedure SourceFileEnter(Sender: TObject);
    procedure TargetFileEnter(Sender: TObject);
    procedure MergeTargetClick(Sender: TObject);
  private
    objCurPair : TBackupPair;

    procedure ClearResults;

  public
    // Set Selected Pair

    procedure StartUp;
    procedure ShutDown;

    procedure SetReadOnly(const Value : boolean);

    procedure SetCurPair(const pPair : TBackupPair);

    procedure RefreshFrame;

    procedure ResizeFrame;
  end;

implementation

{$R *.dfm}

uses
  TPmaLogUnit,        // Log
  TBackupFoleUnit,    // Base Fole Object
  TGenStrUnit,        // Strings
  TGenPickFolderUnit, // Pick Folder
  TWmMsgFactoryUnit,  // Message Factory
  MainBackupUnit,     // Main Backup Form
  TPmaEscapeUnit,
  TPmaClassesUnit;    // Classes

resourcestring
  strCount  = 'Count: ';
  strSize   = 'Size: ';
  strDate   = 'Date: ';
  strNA     = 'N/A';
  strNotRun = 'Not Run Yet';

//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TPairDetailFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TPairDetailFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');
end;
//------------------------------------------------------------------------------
//  Set Current Pair
//------------------------------------------------------------------------------
procedure TPairDetailFrame.SetReadOnly(const Value : boolean);
begin
  PairName.ReadOnly   := Value;
  SourceFile.ReadOnly := Value;
  TargetFile.ReadOnly := Value;
  MergeTarget.Enabled := not Value;
end;
//------------------------------------------------------------------------------
//  Set Current Pair
//------------------------------------------------------------------------------
procedure TPairDetailFrame.SetCurPair(const pPair : TBackupPair);
begin
  objCurPair := pPair;
  RefreshFrame;
end;
//------------------------------------------------------------------------------
//  Refresh all Data in Pair
//------------------------------------------------------------------------------
procedure TPairDetailFrame.RefreshFrame;
begin
  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      // Set Name

      PairName.Text    := objCurPair.pName;
      PairName.Enabled := true;
      PairName.Color   := colorok;

      // Set Source Path Name and Color

      SourceFile.Text    := objCurPair.pSource;
      SourceFile.Enabled := true;

      if objCurPair.IsSource then
        SourceFile.Color := colorok
      else
        SourceFile.Color := colorerr;

      // Set Target Path Name and Color

      TargetFile.Text    := objCurPair.pTarget;
      TargetFile.Enabled := true;

      if objCurPair.IsTarget then
        TargetFile.Color := colorok
      else
        TargetFile.Color := colorerr;

      // Set File Count, Size and Last Run Date

      LabelTotCount.Caption := strCount + IntToStr(objCurPair.pTotCount);
      LabelTotSize.Caption  := strSize  + SizeToStr(objCurPair.pTotSize);

      if objCurPair.pDateTime < 100 then
        LabelDate.Caption := strDate + strNotRun
      else
        begin
          LabelDate.Caption := strDate + DateTimeToStr(objCurPair.pDateTime) +
            ' (' + IntToStr(DaysBetween(Now, objCurPair.pDateTime)) +
              ' Days ago)';
        end;

      // Set Backup Mode

      MergeTarget.Checked := objCurPair.pMode = bmSourceMergeTarget;
    end
  else
    begin
      objCurPair := nil;

      PairName.Text    := '';
      PairName.Enabled := false;
      PairName.Color   := colordis;

      SourceFile.Text    := '';
      SourceFile.Enabled := false;
      SourceFile.Color   := colordis;

      TargetFile.Text    := '';
      TargetFile.Enabled := false;
      TargetFile.Color   := colordis;

      LabelTotCount.Caption := strCount + strNA;
      LabelTotSize.Caption  := strSize  + strNA;
    end
end;
//------------------------------------------------------------------------------
//  Resize Pair Detail Panel
//------------------------------------------------------------------------------
procedure TPairDetailFrame.FrameResize(Sender: TObject);
begin
  ResizeFrame;
end;
//------------------------------------------------------------------------------
//  Resize Pair Detail Panel
//------------------------------------------------------------------------------
procedure TPairDetailFrame.ResizeFrame;
begin
  // Name

  LabelName.Left   := brd;
  LabelName.Top    := brd + (PairName.Height - LabelName.Height) div 2;

  PairName.Left    := brd + labelwdt + brd;
  PairName.Width   := (self.ClientWidth div 2) - brd - PairName.Left;
  PairName.Top     := brd;

  // Backup Mode

  MergeTarget.Left := PairName.Left + PairName.Width + brd *3;
  MergeTarget.Top  := LabelName.Top;

  // Source

  LabelSource.Left := LabelName.Left;
  LabelSource.Top  := LabelName.Top + PairName.Height + brd;

  SourceFile.Left  := PairName.Left;
  SourceFile.Width := PairName.Width;
  SourceFile.Top   := PairName.Top + PairName.Height + brd;

  // Target

  LabelTarget.Left := LabelName.Left;
  LabelTarget.Top  := LabelSource.Top + SourceFile.Height + brd;

  TargetFile.Left  := PairName.Left;
  TargetFile.Width := PairName.Width;
  TargetFile.Top   := SourceFile.Top + SourceFile.Height + brd;;

  // Total Count

  LabelTotCount.Left := MergeTarget.Left;
  LabelTotCount.Top  := LabelSource.Top;

  // Total Size

  LabelTotSize.Left := LabelTotCount.Left + 100;
  LabelTotSize.Top  := LabelSource.Top;

  // Date Last Run

  LabelDate.Left := MergeTarget.Left;
  LabelDate.Top  := LabelTarget.Top;

  // Height

  self.Height := SourceFile.Height * 3 + brd * 4;
end;

//------------------------------------------------------------------------------
//  User has Changed Pair Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.PairNameChange(Sender: TObject);
begin

  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      if (not AnsiSameText(Trim(objCurPair.pName), Trim(PairName.Text))) then
        begin
          objCurPair.pName := PairName.Text;
          MainBackup.RefreshViews;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User has Changed Source Folder Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.SourceFileChange(Sender: TObject);
begin

  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      if (not AnsiSameText(Trim(objCurPair.pSource), Trim(SourceFile.Text))) then
        begin
          objCurPair.pSource := Trim(SourceFile.Text);

          ClearResults;
          MainBackup.RefreshViews;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User has Changed Target Folder Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.TargetFileChange(Sender: TObject);
begin
  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      if (not AnsiSameText(Trim(objCurPair.pTarget), Trim(TargetFile.Text))) then
        begin
          objCurPair.pTarget := Trim(TargetFile.Text);

          ClearResults;
          MainBackup.RefreshViews;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User has Double CLicked on Source Folder Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.SourceFileDblClick(Sender: TObject);
var
  sFolder : string;
begin
  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      sFolder := TGenPickFolder.PickFolder(
                    objCurPair.pSource, 'Pick Source Folder');

      if (length(sFolder) > 0) and
         (not AnsiSameText(sFolder, objCurPair.pSource)) then
        begin
          objCurPair.pSource := sFolder;

          ClearResults;
          MainBackup.RefreshViews;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User has Double CLicked on Target Folder Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.TargetFileDblClick(Sender: TObject);
var
  sFolder : string;
begin
  if (objCurPair <> nil) and TaskList.IsPair(objCurPair) then
    begin
      sFolder := TGenPickFolder.PickFolder(objCurPair.pTarget, 'Pick Target Folder');
      if (length(sFolder) > 0) and
         (not AnsiSameText(sFolder, objCurPair.pTarget)) then
        begin
          objCurPair.pTarget := sFolder;

          ClearResults;
          MainBackup.RefreshViews;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  User Entered the Name
//------------------------------------------------------------------------------
procedure TPairDetailFrame.PairNameEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//  User Entered the Source File
//------------------------------------------------------------------------------
procedure TPairDetailFrame.SourceFileEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//  User Entered the Target File
//------------------------------------------------------------------------------
procedure TPairDetailFrame.TargetFileEnter(Sender: TObject);
begin
  MainBackup.HideHelp;
end;
//------------------------------------------------------------------------------
//  User Changed Backup Mode
//------------------------------------------------------------------------------
procedure TPairDetailFrame.MergeTargetClick(Sender: TObject);
begin
  if Assigned(objCurPair) then
    begin
      if MergeTarget.Checked then
        objCurPair.pMode := bmSourceMergeTarget
      else
        objCurPair.pMode := bmSourceToTarget;

      ClearResults;
    end;
end;
//------------------------------------------------------------------------------
//  Clear all Results
//------------------------------------------------------------------------------
procedure TPairDetailFrame.ClearResults;
begin
  if Assigned(objCurPair) then
    begin
      MainBackup.pDiffFrame.ClearDiff;
      PostMsg(MSG_ME_PROGRESSBAR,WP_CMD_CHANGED, DWORD(0));
      objCurPair.Clear;
      MainBackup.RefreshViews;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TPairDetailFrame);
end.
